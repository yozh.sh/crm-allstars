function setPhoneNumMask(selector, defaultValue){
    $(selector).focus(function () {
        $(this).val(defaultValue)
    })

}

function unsetPhoneNumMask(selector, defaultValue) {

}


function PhoneMaskObject(selector, defaultValue){
    return {
        setMask: () => {$(selector).focus(function () {
            if ($(this).val().length >= 4){
                return
            }
            $(this).val(defaultValue)
    })},
        unsetMask: () => {
            $(selector).focusout(function () {
                if ($(this).val().length >=4){
                    return
                } else {
                    $(this).val("")
                }
    })
        }
    }
}


$(document).ready(function(){
    let phoneInputs = $("input[id^='mask_phone_number']")
    for (let phoneInput of phoneInputs){
        let inputPhoneObj = PhoneMaskObject(phoneInput, "+38")
        inputPhoneObj.setMask()
        inputPhoneObj.unsetMask()

    }
    // let phoneMask = PhoneMaskObject("#mask_phone_number", "+38")


})