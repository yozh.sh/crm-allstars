(function () {

    const URL = `${window.location.protocol}//${window.location.hostname}:${window.location.port}/ajax/sales/yesterday/`

    async function getJson(url=URL, callback){
        let response = await fetch(url)
        if (response.ok){
            callback();
            return response.json();
        } else {
            return response.status
        }
    }

    function hideElementById(elemId){
        document.getElementById(elemId).style.display = 'none';
    }


    document.addEventListener("DOMContentLoaded", async function() {
        let resp = await getJson(URL, function () {
            hideElementById('loading_yesterday')
        })
        document.getElementById('sale_yesterday_price').innerText = `₴ ${resp.count}`

    });
}())