(function() {
    const URL = `${window.location.protocol}//${window.location.hostname}:${window.location.port}/ajax/sales/today/`

    async function getJson(url = URL, callback) {
        let response = await fetch(url)
        if (response.ok) {
            let json = response.json();
            callback();
            return json;
        } else {
            return response.status
        }
    }

    function hideElementById(elemId) {
        document.getElementById(elemId).style.display = 'none';
    }


    document.addEventListener("DOMContentLoaded", async function () {
        let resp = await getJson(URL, function () {
            hideElementById('loading_today')
        })
        document.getElementById('sale_today_price').innerText = `₴ ${resp.count}`

    });
}());