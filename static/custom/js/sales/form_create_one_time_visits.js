(function() {
  console.log("FORM ONE TIME VISITS")
$(document).ready(function(){
    $('#create-form').submit(e => {
    e.preventDefault();
    let clientId = $('#id_client').val()
    let abonId = $('#id_abonement').val()
    var data = $('#create-form').serialize() + '&client=' + clientId
    data += `&abonement=${abonId}`
    jQuery.noConflict();
     $('#error-alert').hide();
    $.ajax({
        data: data,
        type: $('#create-form').attr('method'), // GET or POST
        url: $('#create-form').attr('action'), // the file to call
    })
        .done((resp) => {
            $('#createModal').modal('toggle');
            showSuccess();
            console.log("DONE RESPONSE!!!")
            let closeElem = document.getElementsByClassName('uk-modal-close-default uk-icon uk-close')
            closeElem[1].click()
            Swal.fire({
                position: 'top',
                icon: 'success',
                title: "Абонемент успешно продан",
                showConfirmButton: false,
                timer: 1000
            })
        })
        .fail(resp => {

            for (let i in resp.responseJSON){
                let val = resp.responseJSON[i];
                console.log(val, "RESPONE JSON")
                for (let x in val){
                    showFieldError(x, val[x][0])
                    // $(`input[name=${x}]`).addClass('border border-danger')
                    // $(`select[name=${x}]`).next().addClass('border border-danger')
                    // $(`select[name=${x}]`).after(`<div class='invalid-feedback' style='display: block'>${val[x][0]}</div>`)
                    // $(`input[name=${x}]`).after(`<div class='invalid-feedback' style='display: block'>${val[x][0]}</div>`)
                    // showError(val[x][0])
                }
            }
        })
});

})


function showError(errorMessage) {
    $('#error-alert').append(errorMessage)
    $('#error-alert').css('display', 'block')

}

function showFieldError(fieldName, fieldValue){

    if (document.getElementsByClassName('invalid-generated-input').length === 0){
        $(`input[name=${fieldName}]`).addClass('border border-danger')
        $(`input[name=${fieldName}]`).after(`<div class='invalid-feedback invalid-generated-input' style='display: block'>${fieldValue}</div>`)
    }
    if (document.getElementsByClassName('invalid-generated-select').length === 0){
        $(`select[name=${fieldName}]`).addClass('border border-danger')
        $(`select[name=${fieldName}]`).after(`<div class='invalid-feedback invalid-generated-select'  style='display: block'>${fieldValue}</div>`)
    }





}

function showSuccess() {
    $('.invalid-generated-select').remove()
    $('.invalid-generated-input').remove()
    $('#success-alert').append('<p>Добавление произошло успешно!</p>')
    $('#success-alert').css('display', 'flex')
}
})()



