(function() {
$(document).ready(function(){
    console.log("FORM CREATE AJAX")
    $('#create-form').submit(e => {
    e.preventDefault();
    let clientId = $('#id_client').val()
    jQuery.noConflict();
     $('#error-alert').hide();
    $.ajax({
        data: $('#create-form').serialize() + '&client=' + clientId,
        type: $('#create-form').attr('method'), // GET or POST
        url: $('#create-form').attr('action'), // the file to call
    })
        .done((resp) => {
            $('#createModal').modal('toggle');
            showSuccess();
            location.reload();
        })
        .fail(resp => {

            for (let i in resp.responseJSON){
                let val = resp.responseJSON[i];
                console.log(val, "RESPONE JSON")
                for (let x in val){
                    console.log(x, "XXXX")
                    showFieldError(x, val[x][0])
                    // $(`input[name=${x}]`).addClass('border border-danger')
                    // $(`select[name=${x}]`).next().addClass('border border-danger')
                    // $(`select[name=${x}]`).after(`<div class='invalid-feedback' style='display: block'>${val[x][0]}</div>`)
                    // $(`input[name=${x}]`).after(`<div class='invalid-feedback' style='display: block'>${val[x][0]}</div>`)
                    // showError(val[x][0])
                }
            }
        })
});

})


function showError(errorMessage) {
    $('#error-alert').append(errorMessage)
    $('#error-alert').css('display', 'block')

}

function showFieldError(fieldName, fieldValue){

    if (document.getElementsByClassName('invalid-generated-input').length === 0){
        $(`input[name=${fieldName}]`).addClass('border border-danger')
        $(`input[name=${fieldName}]`).after(`<div class='invalid-feedback invalid-generated-input' style='display: block'>${fieldValue}</div>`)
    }
    if (document.getElementsByClassName('invalid-generated-select').length === 0){
        $(`select[name=${fieldName}]`).addClass('border border-danger')
        $(`select[name=${fieldName}]`).after(`<div class='invalid-feedback invalid-generated-select'  style='display: block'>${fieldValue}</div>`)
    }





}

function showSuccess() {
    $('.invalid-generated-select').remove()
    $('.invalid-generated-input').remove()
    $('#success-alert').append('<p>Добавление произошло успешно!</p>')
    $('#success-alert').css('display', 'flex')
}
})()



