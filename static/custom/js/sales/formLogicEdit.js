function todayDate() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    let nowDate = dd + '.' + mm + '.' + yyyy;
    return nowDate
}

function ajaxGetPromotionDiscount(promotionId, money) {
    $.ajax({
        url: `${window.location.protocol}//${window.location.hostname}:${window.location.port}/ajax/sales/get_discount/`,
        type: 'get',
        data: {
            promo_id: promotionId,
            money: money,
        }
    }).done(function (data) {
        if (Math.sign(data.money) === -1){
            if (document.getElementById('invalid_promo_msg') === null){
                $('#create-submit').prop('disabled', true)
                $('select[name=promotion]').addClass('border border-danger')
                $('select[name=promotion]').after("<div id='invalid_promo_msg' class='invalid-feedback' style='display: block'>Сумма скидки превышает итоговую сумму стоимости, пожалуйста выберите другую Акцию</div>")
                showError('Сумма скидки превышает итоговую сумму стоимости, пожалуйста выберите другую Акцию')
            } else {
                $('#invalid_promo_msg').show()
                $('#create-submit').prop('disabled', true)
                $('select[name=promotion]').addClass('border border-danger')

            }

        } else {
            $('#error-alert').hide();
            $('#create-submit').prop('disabled', false)
            $('select[name=promotion]').removeClass('border border-danger')
            $('#invalid_promo_msg').hide()
            localStorage.setItem('price', data.money)



        }
        if ($('#id_installment_plan').prop('checked')) {
            //
        } else {
            $('#id_price').val(data.money)
        }

    }).fail(function () {
        $('#id_price').val(localStorage.getItem('price'))
        $('#error-alert').hide();
            $('#create-submit').prop('disabled', false)
            $('select[name=promotion]').removeClass('border border-danger')
            $('#invalid_promo_msg').hide()
    })
}


function ajaxGetLastAbon(clientId){
    $.ajax({
        url: `${window.location.protocol}//${window.location.hostname}:${window.location.port}/ajax/sales/last_abon/${clientId}`

    }).done(function(data){
        if(data.name === null || Object.keys(data).length === 0){
            data.name = 'Отсутсвует'
        }

        $('#id_last_sale_abon').val(`${data.name}`)
    })
}

function ajaxGetLastPromotion(clientId){
    $.ajax({
        url: `${window.location.protocol}//${window.location.hostname}:${window.location.port}/ajax/sales/last_promotion/${clientId}`

    }).done(function(data){
        if(data.name === null || Object.keys(data).length === 0){
            data.name = 'Отсутсвует'
        }

        $('#id_last_sale_promo').val(`${data.name}`)
    })
}

function abonementPriceInstallmentError(elemId, errorText, propId){
    $(`#${elemId}`).addClass('border-danger')
    $(`#${propId}`).remove()
    $(`#${elemId}`).after(`<div id='${propId}' class='invalid-feedback' style='display: block'>${errorText}</div>`)
}


function ajaxGetAbonementPrice(abonId){
    $.ajax({
        url: `${window.location.protocol}//${window.location.hostname}:${window.location.port}/ajax/sales/get_price/${abonId}`,
        type: 'get',
    }).done(function(data){
        if ($('#id_installment_plan').prop('checked')){
            localStorage.setItem('abonementPrice', data.full_price)
            localStorage.setItem('price', data.full_price)
            if (Number(data.full_price) < Number($('#id_price').val())){
                $('#update-btn').prop('disabled', true)
                abonementPriceInstallmentError('id_abonement', 'Стоимость абонемента ниже чем сумма к оплате', 'abon_error')
            // Если сумма абонемента ниже чем итогая стоимость
            } else {
                $('#update-btn').prop('disabled', false)
                $('#abon_error').remove()
                $('#id_abonement').removeClass('border-danger')
            }
            return
        }
        localStorage.setItem('abonementPrice', data.full_price)
        localStorage.setItem('price', data.full_price)
        $('#id_price').val(data.full_price)

        if ($('#id_promotion').val() !== ''){
            ajaxGetPromotionDiscount($('#id_promotion').val(), data.full_price)

        }

    })
}

function setAbonementValue(saleId) {
    $.ajax({
        url: `${window.location.protocol}//${window.location.hostname}:${window.location.port}/ajax/sales/get_abonement_id_by_sale_id`,
        type: 'get',
        data: {
            sale_id: saleId,
        }
    }).done(function (data) {
        $("#id_abonement select").val(data.abonement)
        $(`#id_abonement option[value=${data.abonement}]`).prop('selected', true)
        $('#id_abonement').trigger('change')
    })
}

function ajaxGetAbonement(groupCount){
    $.ajax({
        url: `${window.location.protocol}//${window.location.hostname}:${window.location.port}/ajax/sales/get_abonement`,
        type: 'get',
        data: {
            filter: groupCount,
        },

    }).done(function(data){
        console.log("AJAXGET ABONEMENT CALLL!!!!")
        if(data.length === 0){
            $('#id_abonement').find('option').remove()
        } else {
            $('#id_abonement option').remove()
            data.forEach((elem) => {
                $('#id_abonement')
                    .append(
                        $("<option></option>")
                            .attr("value", elem.id)
                            .text(elem.name)
                    )
            })

        }
        setAbonementValue($('#id_sale_id').val())
        ajaxGetAbonementPrice($('#id_abonement option:selected').val())
    })
}



function ajaxGetGroupsByTutor(tutorId, departamentId, selectNumber){
    $(`#id_group${selectNumber}`).empty()
    // <int:tutor_id>
    $.ajax({
        url: `${window.location.protocol}//${window.location.hostname}:${window.location.port}/ajax/sales/groups_by_tutor/`,
        type: 'get',
        data: {
            departament: departamentId,
            tutor_id: tutorId,
        }

    }).done(function(data){
        data.forEach((elem) => {
            $(`#id_group${selectNumber}`)
                .append(
                    $("<option></option>")
                        .attr("value", elem.id)
                        .text(elem.name)
                )
        })
    })
}


function formGroupVisibilityControl (groupId, visibility='hidden'){
    if (visibility === 'show'){
        // INPUTS
        $(`#id_tutor${groupId}`).show()
        $(`#id_group${groupId}`).show()
        $(`#id_departament${groupId}`).show()

        // LABELS
        $(`label[for=id_tutor${groupId}]`).show()
        $(`label[for=id_group${groupId}]`).show()
        $(`label[for=id_departament${groupId}]`).show()

        // VALIDATION ON
        $(`#id_group${groupId}`).prop('required', true)
        // SPAN
        $(`span[aria-labelledby=select2-id_tutor${groupId}-container]`).show()
    } else if (visibility === 'hidden') {
        // INPUTS
        $(`#id_tutor${groupId}`).hide()
        $(`#id_group${groupId}`).hide()
        $(`#id_departament${groupId}`).hide()

        // LABELS
        $(`label[for=id_tutor${groupId}]`).hide()
        $(`label[for=id_group${groupId}]`).hide()
        $(`label[for=id_departament${groupId}]`).hide()

        // SPAN
        $(`span[aria-labelledby=select2-id_tutor${groupId}-container]`).hide()

        // VALIDATION OFF
        $(`#id_group${groupId}`).prop('required', false)
    }


}

function ajaxGetAndSetGroup(saleId){
    $.ajax({
        url: `${window.location.protocol}//${window.location.hostname}:${window.location.port}/ajax/sales/get_groups/`,
        type: 'get',
        data: {
            sale_id: saleId
        }
    }).done(function (data) {
        for (let group in data){
            if (data[group] === null){
                delete data[group]
            }
        }
        for (let group in data){
            $(`#id_${group}`).val(String(data[group]))
        }
    })
}

$(document).ready(function () {


     $('#id_first_sum').after("<div id='first_sum_error' class='invalid-feedback' style='display: none'>Внесенная сумма больше необходимой</div>")
    // INSTALLMENT
    $('#id_first_sum').hide()
    $('label[for=id_first_sum]').hide()
    // ABONEMENT UI AND LOAD DATA LOGIC
    // $("#id_abonement").empty()
    // ajaxGetAbonement($('#id_abonement_group_count').val())

    // ACTIVATION FIRST TIME LOAD
    $("#id_activation_date").hide()
    $('label[for=id_activation_date]').hide()


    // GROUP COUNT LOGIC
    if ($('#id_abonement_group_count').val() === 'ONE'){
        formGroupVisibilityControl(2)

    } else if ($('#id_abonement_group_count').val() == 'TWO'){
        formGroupVisibilityControl(1, 'show')
        formGroupVisibilityControl(2, 'show')

        $("#id_abonement").empty()
        ajaxGetAbonement($('#id_abonement_group_count').val())

        let tutorId = $('#id_tutor2 option:selected').val()
        ajaxGetGroupsByTutor(tutorId, $("#id_departament2").val(), 2)

    } else if ($('#id_abonement_group_count').val() === 'ONE') {
        formGroupVisibilityControl(2)
            formGroupVisibilityControl(1, 'show')

            // ABONEMENT UI AND LOAD DATA LOGIC
            $("#id_abonement").empty()
            ajaxGetAbonement($('#id_abonement_group_count').val())

    } else if ($('#id_abonement_group_count').val() === 'ALL'){
        formGroupVisibilityControl(1)
            formGroupVisibilityControl(2)

            // ABONEMENT UI AND LOAD DATA LOGIC
            $("#id_abonement").empty()
            ajaxGetAbonement($('#id_abonement_group_count').val())
    }


    $('#id_abonement_group_count').change(function () {
        if ($(this).val() == 'TWO'){
            $('#id_departament2').val(localStorage.getItem('departament'))
            formGroupVisibilityControl(1, 'show')
            formGroupVisibilityControl(2, 'show')
            // ABONEMENT UI AND LOAD DATA LOGIC
            $("#id_abonement").empty()
            ajaxGetAbonement($('#id_abonement_group_count').val())

            let tutorId = $('#id_tutor2 option:selected').val()
            ajaxGetGroupsByTutor(tutorId, $("#id_departament2").val(), 2)



        } else if ($(this).val() == 'ONE'){
            $('#id_departament1').val(localStorage.getItem('departament'))

            formGroupVisibilityControl(2)
            formGroupVisibilityControl(1, 'show')

            // ABONEMENT UI AND LOAD DATA LOGIC
            $("#id_abonement").empty()
            ajaxGetAbonement($('#id_abonement_group_count').val())

        } else if ($(this).val() == 'ALL'){
            formGroupVisibilityControl(1)
            formGroupVisibilityControl(2)

            // ABONEMENT UI AND LOAD DATA LOGIC
            $("#id_abonement").empty()
            ajaxGetAbonement($('#id_abonement_group_count').val())

        }
    })

    // GET ABONEMENT BY GROUP COUNT
    // WHERE PAGE LOADED FIRST TIME
    ajaxGetAbonement($('#id_abonement_group_count').val())

    $('#id_activation').change(function () {
        if ($(this).prop("checked")){
            $("#id_activation_date").show()
            $("#id_activation_date").prop('required', true)
            $('label[for=id_activation_date]').show()
        } else {
            $("#id_activation_date").hide()
            $("#id_activation_date").prop('required', false)
            $('label[for=id_activation_date]').hide()
        }
    })

    if ($('#id_activation').prop('checked')){
            $("#id_activation_date").show()

            $("#id_activation_date").prop('required', true)
            $('label[for=id_activation_date]').show()
    }

    if ($('#id_departament1').val() !== ''){
        let tutorId = $('#id_tutor1 option:selected').val()
        ajaxGetGroupsByTutor(tutorId, $("#id_departament1").val(), 1)
    }


    if ($('#id_departament2').val() !== ''){
        let tutorId = $('#id_tutor1 option:selected').val()
        ajaxGetGroupsByTutor(tutorId, $("#id_departament2").val(), 1)
    }




    // NOT CHANGE BUT INSTALLED ABONEMENT DATA
    if ($('#id_abonement').val() !== null){
        ajaxGetAbonementPrice($('#id_abonement').val())
    }
    $('#id_abonement').change(function () {
        console.log("CHANGE ABONEMENT")
        ajaxGetAbonementPrice($(this).val())
    })

    // IF DEPARTAMENT CHANGE UPDATE GROUP LIST
    $('#id_departament1').change(function () {
        let tutorId = $('#id_tutor1 option:selected').val()
        ajaxGetGroupsByTutor(tutorId, $("#id_departament1").val(), 1)
    })

    $('#id_departament2').change(function () {
        let tutorId = $('#id_tutor2 option:selected').val()
        ajaxGetGroupsByTutor(tutorId, $("#id_departament2").val(), 2)
    })

    $("#id_promotion").change(function () {
        let price = localStorage.getItem('abonementPrice')
        if ($('#id_installment_plan').prop('checked')){
            if ($('#id_promotion').val() !== ''){
                ajaxGetPromotionDiscount($(this).val(), price)
            } else {
                ajaxGetAbonementPrice($('#id_abonement').val())
            }

        } else {
            ajaxGetPromotionDiscount($(this).val(), price)
        }
        if ($(this).val() == ''){
           ajaxGetAbonementPrice($('#id_abonement').val())
        }

    })

    $('#id_installment_plan').change(function () {
        if ($(this).prop('checked')){
            $('#id_first_sum').show()
            $('#id_first_sum').prop('required', true)
            $('label[for=id_first_sum]').show()
            $('#id_price').val('')
            $('#id_promotion').val()
        } else {
            $('#id_first_sum').hide()
            $('#id_first_sum').prop('required', false)
            $('label[for=id_first_sum]').hide()
            let price = localStorage.getItem('price')
            ajaxGetPromotionDiscount($("#id_promotion").val(), price)
            $('#id_price').val(localStorage.getItem('price'))
        }
    })

    $('#id_first_sum').on('input', function () {
        let currentPrice = $('#id_price').val()
        if (Number($(this).val()) > Number(localStorage.getItem('price'))){
            $('#id_first_sum').addClass('border border-danger')
            $('#first_sum_error').show()

            $('#create-submit').prop('disabled', true)
        } else {
            $('#id_first_sum').removeClass('border border-danger')
            $('#first_sum_error').hide()
            $('#create-submit').prop('disabled', false)
        }

        if (Number($(this).val()) < Number(localStorage.getItem('price')) ){
            $('#update-btn').prop('disabled', false)
            $('#abon_error').remove()
            $('#id_abonement').removeClass('border-danger')
        }
        $('#id_price').val($(this).val())
    })

    $('#id_installment_plan').change(function () {
        if ($('#id_installment_plan').prop('checked') === false ){
            let abonId = $('#id_abonement').val();
            ajaxGetAbonementPrice(abonId)

        }
    })


    if ($('#id_installment_plan').prop('checked')){
        let initialMoney = $('#id_price').val()
        $('#id_first_sum').val(initialMoney)
        $('#id_installment_plan').trigger('change')
        $('#id_price').val(initialMoney)

    }


ajaxGetAndSetGroup($('#id_sale_id').val())

})