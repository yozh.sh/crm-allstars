$(document).ready(function() {

          $('#id_training_days').multiselect({
              dropRight: true,
              templates: {
                button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown" title="None"><span class="multiselect-selected-text"></span></button>',
                ul: '<ul class="multiselect-container dropdown-menu pull-right" style="width: max-content"></ul>',
                filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
                filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default multiselect-clear-filter" type="button"><i class="glyphicon glyphicon-remove-circle"></i></button></span>',
                li: '<div style="display: flex; align-items: center; justify-content: space-between;" <li><a href="javascript:void(0);"><label></label></a></li> <input id="schedule_input" style=" font-size: 80%; width: 20%; height: 25px;" class="form-control" type="text"></div>',
                divider: '<li class="multiselect-item divider"></li>',
                liGroup: '<li class="multiselect-item group"><label class="multiselect-group"></label></li>'
            }
          });
        });