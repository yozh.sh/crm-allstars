function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});


function getChecked(){
    let checkboxes = $('td input:checkbox')
    let checked = []
    for (inp of checkboxes){
        if ($(inp).is(":checked")){
            checked.push(inp)
        }
    }
    return checked
}

function getInputId(inputList){
    let ids = []
    for (inp of inputList){
        ids.push($(inp).prop('id'))
    }
    return ids
}

function deleteChecked(){
    let elemIds = getInputId(getChecked())
    $.ajax({
        url: `${window.location.protocol}//${window.location.hostname}:${window.location.port}/sale/massdelete/`,
        type: 'post',
        data: {
            elemIds
        },

    }).done(function(data){
        location.reload()
    })
}


$(document).ready(function () {
    $("#mass-delete-btn").click(function () {
        let ids = getInputId(getChecked())
        $('#mass-delete-body').html(`<p>Вы точно хотите удалить ${ids.length} элементов???</p>`)

    })

    $('#mass-delete-btn-action').click(function () {
        deleteChecked()
    })

})