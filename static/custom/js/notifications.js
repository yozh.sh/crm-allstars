axios.defaults.xsrfCookieName = 'csrftoken'
axios.defaults.xsrfHeaderName = "X-CSRFTOKEN"

$(document).ready(function () {
    getNotifications()
    $('#alertsDropdown').click(function () {
        setReadNotifications()
    })

})

function unsetUnreadStatus(){
    let storeItems = JSON.parse(window.localStorage.getItem("REPORT"))
    for (item of storeItems){
        $(`#unread-${item.id}`).hide()
    }
    addBadge("")
}

function setReadNotifications(){
axios.post(`${window.location.protocol}//${window.location.hostname}:${window.location.port}/notificationapi/report/set_read/`,
    {
        data: JSON.parse(window.localStorage.getItem("REPORT"))
})
            .then(response => {
                setTimeout(unsetUnreadStatus, 1000)
            })
}

function saveNotificationDataToStore(type, data){
    window.localStorage.setItem(type, data)
}

function addBadge(value){
    $('#notification-badge').html(value)
}

function setReportNotifications(dateTime, message, notifyId){
    let template = `
            <a class="dropdown-item d-flex align-items-center" value="${notifyId}" href="#">
            <div class="mr-3">
                <div class="icon-circle bg-primary">
                    <i class="fas fa-file-alt text-white"></i>
                </div>
            </div>
            <div>
                <div class="small text-gray-500">${dateTime}</div>
                <span class="font-weight-bold">${message}</span>
            </div>
            <i id="unread-${notifyId}" class="fa fa-exclamation-circle"></i>
`
    
    $('#notify-list').after(template)
}

function setNotification(response){
    let reportData = []
    for (notify of response.data){
        if (notify.type === "REPORT"){
            setReportNotifications(notify.created, notify.message, notify.id)
            reportData.push(notify)
        }
    }
    let newCount = response.data.filter(obj => obj.new === true)
    if (newCount.length !== 0){
        addBadge(newCount.length)
        saveNotificationDataToStore('REPORT', JSON.stringify(reportData))
    }

}

function getNotifications() {
    axios.get(`${window.location.protocol}//${window.location.hostname}:${window.location.port}/notificationapi/report`)
            .then(response => {
                setNotification(response)
            })
}