$(function () {
    const dateTo = {
        show: function () {
            $('#id_date_to').prop('required', true)
            $('#id_date_to').show()
            $('label[for=id_date_to]').show()
        },
        hide: function () {
            $('#id_date_to').prop('required', false)
            $('#id_date_to').hide()
            $('label[for=id_date_to]').hide()
        }
    }

    const currentDayChoice = {
        show: function () {
            $('#id_current_day').show()
            $('label[for=id_current_day]').show()
        },
        hide: function () {
            $('#id_current_day').hide()
            $('label[for=id_current_day]').hide()
        }
    }

    const monthParameters = {
        show: function () {
            $('#id_month_parameters').show()
            $('label[for=id_month_parameters]').show()
        },
        hide: function () {
            $('#id_month_parameters').hide()
            $('label[for=id_month_parameters]').hide()
        }
    }

    const yearParameters = {
        show: function () {
            $('#id_year_parameters').show()
            $('label[for=id_year_parameters]').show()
        },
        hide: function () {
            $('#id_year_parameters').hide()
            $('label[for=id_year_parameters]').hide()
        }
    }

    const dateFrom = {
        show: function () {
            $('#id_date_from').prop('required', true)
            $('#id_date_from').show()
            $('label[for=id_date_from]').show()
        },
        hide: function () {
            $('#id_date_from').prop('required', false)
            $('#id_date_from').hide()
            $('label[for=id_date_from]').hide()
        }
    }


    $(document).ready(function () {
        dateTo.hide()
        dateFrom.hide()
        monthParameters.hide()
        yearParameters.hide()
        $('#id_choice_period').change(function () {
            if ($('#id_choice_period option:selected').val() === 'CUSTOM'){
                dateFrom.show()
                dateTo.show()
            } else {
                dateFrom.hide()
                dateTo.hide()
            }
        })


         $('#id_choice_period').change(function () {
             if ($('#id_choice_period option:selected').val() === 'DAY') {
                 currentDayChoice.show()
             } else {
                 currentDayChoice.hide()
             }
         })

        $('#id_choice_period').change(function () {
            if ($('#id_choice_period option:selected').val() === 'MONTH') {
                monthParameters.show()
            } else {
                monthParameters.hide()
            }
        })

        $('#id_choice_period').change(function () {
            if ($('#id_choice_period option:selected').val() === 'YEAR') {
                yearParameters.show()
            } else {
                yearParameters.hide()
            }
        })





    })
})