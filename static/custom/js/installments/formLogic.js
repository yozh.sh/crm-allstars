var store = {
    sale: []
}

function ajaxGetSaleByClient(clientId){
    $.ajax({
        url: `${window.location.protocol}//${window.location.hostname}:${window.location.port}/ajax/installments/get_sales_by_client/${clientId}`

    }).done(function(data){
        store.sale = data
        data.forEach((elem) => {
             $('#id_sale')
                .append(
                    $("<option></option>")
                        .attr("value", elem.id)
                        .text(elem.abonement[0].name)
                )
            
            let result = store.sale.filter(obj => {
                return obj.id === Number($('#id_sale').val())
            })
            $('#id_full_price').val(result[0].final_price)
        })

    })
    .fail(
        $('#id_full_price').val('')
    )
}




$(document).ready(function(){
    
    $('#id_client').change(function(){
        $('#id_sale').find('option').remove()
        ajaxGetSaleByClient($(this).val())
    })

    $('#id_sale').change(function(){
        let result = store.sale.filter(obj => {
            return obj.id === Number($(this).val())
        })
        $('#id_full_price').val(result[0].final_price)
    })

    // запретить отправку формы и отослать аяксом
    $('form').submit(e => {
        e.preventDefault()
    })
})