function forceCloseInstallment(installmentId) {
    return new Promise((resolve, reject) => {

    
    $.ajax({
        url: `${window.location.protocol}//${window.location.hostname}:${window.location.port}/ajax/client/information/installment/${installmentId}/force_close_installment`,
        type: 'get',
        success: function(data) {
            resolve(data)
        },
        error: function(error){
            reject(error)
        }

    })
})
}

function getInstallmentId() {
    let installementId = $('#forceCloseInstallment').attr('installment')
    return Number(installementId)
}


$(document).ready(function(){
    $('#forceCloseInstallment').click(function(){
        forceCloseInstallment(getInstallmentId()).then(data => {
            window.location.reload()
        })
    })
})