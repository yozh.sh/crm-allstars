function validatePaymentMoney(installmentId, money) {
    return new Promise((resolve, reject) => {
    $.ajax({
        url: `${window.location.protocol}//${window.location.hostname}:${window.location.port}/ajax/client/information/installment/${installmentId}/validate_payment_money/`,
        type: 'get',
        data: {
            money: money
        },
        success: function(data) {
            resolve(data)
        },
        error: function(error){
            reject(error)
        },
    })
})    
}

function getMoney() {
    return $('#id_payment').val()
}

function getInstallmentId() {
    return $('#id_installment_id').val()
}

function invalid() {
    $("#invalid_payment").css('display', 'block')
    $("#create-submit").prop('disabled', true)
}

function valid() {
    $("#invalid_payment").css('display', 'none')
    $("#create-submit").prop('disabled', false)
}

$(document).ready(function(){
    $('#id_payment').keyup(function(){
        validatePaymentMoney(getInstallmentId(), $(this).val()).then(data => {
            valid()
        })
        .catch(error => {
            invalid()
        })
    })
    $("#create-submit").click(function(event){
        event.preventDefault()
        validatePaymentMoney(getInstallmentId(), getMoney()).then(data => {
            createForm()
        })
        .catch(error => {
            if (error.status === 406) {
                invalid()
            }
        })
    })
})



function createForm(){
    $('#error-alert').hide();
    $.ajax({
        data: $('#create-form').serialize(),
        type: $('#create-form').attr('method'), // GET or POST
        url: $('#create-form').attr('action'), // the file to call
    })
        .done((resp) => {
            $('#createModal').modal('toggle');
            showSuccess();
            location.reload();
        })
        .fail(resp => {

            for (let i in resp.responseJSON){
                let val = resp.responseJSON[i];
                console.log(val, "RESPONE JSON")
                for (let x in val){
                    console.log(x, "XXXX")
                    showFieldError(x, val[x][0])
                }
            }
        })
}


function showError(errorMessage) {
    $('#error-alert').append(errorMessage)
    $('#error-alert').css('display', 'block')

}

function showFieldError(fieldName, fieldValue){

    if (document.getElementsByClassName('invalid-generated-input').length === 0){
        $(`input[name=${fieldName}]`).addClass('border border-danger')
        $(`input[name=${fieldName}]`).after(`<div class='invalid-feedback invalid-generated-input' style='display: block'>${fieldValue}</div>`)
    }
    if (document.getElementsByClassName('invalid-generated-select').length === 0){
        $(`select[name=${fieldName}]`).addClass('border border-danger')
        $(`select[name=${fieldName}]`).after(`<div class='invalid-feedback invalid-generated-select'  style='display: block'>${fieldValue}</div>`)
    }





}

function showSuccess() {
    $('.invalid-generated-select').remove()
    $('.invalid-generated-input').remove()
    $('#success-alert').append('<p>Добавление произошло успешно!</p>')
    $('#success-alert').css('display', 'flex')
}