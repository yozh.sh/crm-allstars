// Disable Mouse scrolling
    $('input[type=number]').on('mousewheel',function(e){ $(this).blur(); });
    // Disable keyboard scrolling
    $('input[type=number]').on('keydown',function(e) {
        var key = e.charCode || e.keyCode;
        // Disable Up and Down Arrows on Keyboard
        if(key == 38 || key == 40 ) {
        e.preventDefault();
        } else {
        return;
        }
    });
