function revokeSale(abonId){
    $.ajax({
        url: `${window.location.protocol}//${window.location.hostname}:${window.location.port}/sale/${abonId}/revoke`,
        type: 'post',
        success: function (data) {
        //
        },
        error: function (error) {
        //
        }
    })
}

function getAbonId(){
    return $('.revoke-sale').attr('sale_id')
}

$(document).ready(function () {
    $('.revoke-sale').click(function () {
        console.log(getAbonId())
    })

})