$(document).on('click', '.clickRemoveVisit', function () {
    console.log($(this).data('id'));
    var id = '#' + $(this).data('id');
    $(id).remove();
    alert('Remove was complete')
});

function allUsers() {
    let date = localStorage.getItem('date')
    $('#group-date').attr('data-date', date);
    $('#group-date').append(date);

    $.ajax({
        type: "GET",
        cache: false,
        url: '/calendar/api/clients/',
        dataType: "json",
        success: function (data) {
            // console.log('Api all users', data);
            localStorage.setItem('userId', data[0].id);
            $('#user-id').attr('data-user-id', data[0].id);

            var users = data.map(
                item => '<option value="' + item.id + '">' + item.surname + ' ' + item.name + ' ' + item.middle_name + '</option>'
            );

            $(".js-user-search").append(users);

        }
    });
}

function allPreEntry() {
    let date = moment($('#group-date').data('date'), 'DD.MM.YYYY').format('YYYY-MM-DD');
    let groupId = $('#group-id').data('group-id');
    let tableDate = $('#group-date').data('date');
    let api = '/calendar/api/preentry/group/' + groupId + '/?date=' + date;

    //console.log('Api', api);
    $.ajax({
        type: "GET",
        cache: false,
        url: api,
        dataType: "json",
        success: function (data) {
            // console.log('Api', data);

            let content = data.map(function(item, i) {
                return '<tr>' +
                '<td>' + item.client.surname + ' ' + item.client.name + ' ' + item.client.lastname + '</td>' +
                '<td>' + tableDate + '</td>' +
                '<td class="uk-flex uk-flex-around">' +
                    '<button class="uk-button uk-button-primary uk-button-small btn-pre-entry green" type="button" data-user-id="' + item.client.id + '">Посещение</button>' +
                    '<button class="uk-button uk-button-primary uk-button-small btn-pre-entry light-green" type="button" data-user-id="' + item.client.id + '">абонемент</button>' +
                    '<button class="uk-button uk-button-secondary uk-button-small btn-pre-entry dark-blue" type="button" data-user-id="' + item.client.id + '">одно занятие</button>' +
                    '<button class="uk-button uk-button-danger uk-button-small btn-pre-entry-remove" type="button" data-user-id="' + item.client.id + '" data-visit-id="' + item.id + '">Удалить</button>' +
                '</td>' +
                '</tr>'
            })
            // console.log('Content', content);
            $("#pre-entry-table").html(content);

        }
    });


}

function allVisit() {
    let date = moment($('#group-date').data('date'), 'DD.MM.YYYY').format('YYYY-MM-DD');
    let groupId = $('#group-id').data('group-id');
    let tableDate = $('#group-date').data('date');
    let api = '/calendar/api/visits/group/' + groupId + '/?date=' + date;
    let dateEnd;
    $.ajax({
        type: "GET",
        cache: false,
        url: api,
        dataType: "json",
        success: function (data) {
            console.log('Api all users', data);
            let content = data.map(function(item, i) {
                dateEnd = moment($('#group-date').data('date') , 'DD.MM.YYYY').add(item.abonement.abonement_duration, 'days').format('DD-MM-YYYY');
                return '<tr>' +
                    '<td>' + item.client.surname + ' ' + item.client.name + ' ' + item.client.lastname + '</td>' +
                    '<td>' + tableDate + '</td>' +
                    '<td>' + item.abonement.abonement_available_visits_number + '</td>' +
                    '<td>' + dateEnd + '</td>' +
                    '<td class="uk-flex uk-flex-around">' +
                    '<button class="uk-button uk-button-danger uk-button-small btn-entry-remove" type="button" data-user-id="' + item.client.id + '" data-visit-id="' + item.id + '">Удалить</button>' +
                    '</td>' +
                    '</tr>'
            })
            console.log('Content', content);


            $("#visit-table").html(content);
        }
    });


}


$(window).bind('beforeunload', function () {
    var dateDay = $('#group-date').data('date');

    localStorage.setItem('date', dateDay);
    return "Do you really want to refresh?";
});

$('#search-select').on('change', function () {
    let val = $('#search-select').val();
    $('#user-id').attr('data-user-id', val);
    localStorage.setItem('userId', val);

    console.log('Val: ', val);
});

/*Add preentry from all user list*/
$('#search-pre').on('click', function () {
    let clientId = $('#search-select').val();
    let date = $('#group-date').data('date');
    let groupId = $('#group-id').data('group-id');
    // console.log('Val: ', clientId);
    // console.log('Date: ', date);

    let data = {
        "date": date,
        "client": clientId,
        "group": groupId,
    };

    // console.log('Date: ', data);

    $.ajax({
        type: "POST",
        cache: false,
        data: data,
        url: '/calendar/api/preentry/',
        dataType: "json",
        success: function (data) {
            console.log('Output: ', data);
            allPreEntry();
        },
        statusCode: {
            204: function(){ // выполнить функцию если код ответа HTTP 404
                alert( "Пользователь уже был добавлен." );
            },
        }
    });
});

/*Add visit from all user list*/
$('#search-visit').on('click', function () {
    let clientId = $('#search-select').val();
    let date = $('#group-date').data('date');
    let groupId = $('#group-id').data('group-id');
    // console.log('Val: ', clientId);
    // console.log('Date: ', date);

    let data = {
        "visit_date": date,
        "client": clientId,
        "pre_entry_client": false,
        "group": groupId,
    };


    console.log('Date: ', data);

    $.ajax({
        type: "POST",
        cache: false,
        data: data,
        url: '/calendar/api/client/visits/',
        dataType: "json",
        success: function (data) {
            console.log('Output: ', data);
            allVisit();
        },
        error: function (error) {
            console.log('Eror: ', error);

        },
        statusCode: {
            204: function(){ // выполнить функцию если код ответа HTTP 404
                alert( "Пользователь уже был добавлен." );
            },
        },statusCode: {
            400: function(){ // выполнить функцию если код ответа HTTP 404
                alert( "Отсутствует абонимент!" );
            },
        }
    });
});

/*Remove preentry from preentry list*/
$('.btn-pre-entry-remove').on('click', function () {
    let preentryId = this.data('visit-id');
    let api = "/calendar/api/preentry/" + preentryId
    console.log('preentryId: ', api);

    $.ajax({
        type: "GET",
        cache: false,
        url: api,
        dataType: "json",
        success: function (data) {
            // console.log('Output: ', data);
            allPreEntry();
        }
    });
});

$(document).on('click', '.btn-pre-entry-remove', function (e) {
    let preentryId = $(e.currentTarget).data('visit-id');
    let api = "/calendar/api/preentry/" + preentryId
    console.log('preentryId: ', api);

    $.ajax({
        type: "DELETE",
        cache: false,
        url: api,
        dataType: "json",
        success: function (data) {
            // console.log('Output: ', data);
            allPreEntry();
        }
    });
});


/*Remove entry*/
$('.btn-entry-remove').on('click', function () {
    let entryId = this.data('visit-id');
    let api = "/calendar/api/client/visit/" + entryId
    console.log('entryId: ', api);

    $.ajax({
        type: "DELETE",
        cache: false,
        url: api,
        dataType: "json",
        success: function () {
            allVisit();
        }
    });
});

$(document).on('click', '.btn-entry-remove', function (e) {
    let entryId = $(e.currentTarget).data('visit-id');
    let api = "/calendar/api/client/visit/" + entryId
    console.log('entryId: ', api);

    $.ajax({
        type: "DELETE",
        cache: false,
        url: api,
        dataType: "json",
        success: function () {
            allVisit();
        }
    });
});

/*Output all users for search list*/
allUsers();

/*Output all users in visits list*/
allVisit();
/*Output all users in preentry list*/
allPreEntry();