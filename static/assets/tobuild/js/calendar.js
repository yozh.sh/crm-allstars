// Calendar navigation
function Calendar(id, year, month) {
    var Dlast = new Date(year, month + 1, 0).getDate(),
        D = new Date(year, month, Dlast),
        DNlast = D.getDay(),
        DNfirst = new Date(D.getFullYear(), D.getMonth(), 1).getDay(),
        calendar = '<tr>',
        m = document.querySelector('#' + id + ' option[value="' + D.getMonth() + '"]'),
        g = document.querySelector('#' + id + ' input');
    if (DNfirst != 0) {
        for (var i = 1; i < DNfirst; i++) calendar += '<td>';
    } else {
        for (var i = 0; i < 6; i++) calendar += '<td>';
    }
    for (var i = 1; i <= Dlast; i++) {
        if (i == new Date().getDate() && D.getFullYear() == new Date().getFullYear() && D.getMonth() == new Date().getMonth()) {
            day = i;
            if (month < 10) {
                month = '0' + (D.getMonth() + 1);
            }
            if (i < 10) {
                day = '0' + i;
            }
            calendar += '<td class="clickDate today"  data-date="' + day + '.' + month + '.' + D.getFullYear() + '">' + i;
        } else {
            var month = D.getMonth() + 1,
                day = i;
            if (month < 10) {
                month = '0' + (D.getMonth() + 1);
            }
            if (i < 10) {
                day = '0' + i;
            }
            if (  // список официальных праздников
            (i == 1 && D.getMonth() == 0 && ((D.getFullYear() > 1897 && D.getFullYear() < 1930) || D.getFullYear() > 1947)) || // Новый год
            (i == 2 && D.getMonth() == 0 && D.getFullYear() > 1992) || // Новый год
            ((i == 3 || i == 4 || i == 5 || i == 6 || i == 8) && D.getMonth() == 0 && D.getFullYear() > 2004) || // Новый год
            (i == 7 && D.getMonth() == 0 && D.getFullYear() > 1990) || // Рождество Христово
            (i == 23 && D.getMonth() == 1 && D.getFullYear() > 2001) || // День защитника Отечества
            (i == 8 && D.getMonth() == 2 && D.getFullYear() > 1965) || // Международный женский день
            (i == 1 && D.getMonth() == 4 && D.getFullYear() > 1917) || // Праздник Весны и Труда
            (i == 9 && D.getMonth() == 4 && D.getFullYear() > 1964)  // День Победы

            ) {
                calendar += '<td class="clickDate holiday" data-date="' + day + '.' + month + '.' + D.getFullYear() + '"  >' + i;
            } else {
                calendar += '<td class="clickDate" data-date="' + day + '.' + month + '.' + D.getFullYear() + '"  >' + i;
            }
        }
        if (new Date(D.getFullYear(), D.getMonth(), i).getDay() == 0) {
            calendar += '<tr>';
        }
    }
    for (var i = DNlast; i < 7; i++) calendar += '<td>&nbsp;';
    document.querySelector('#' + id + ' tbody').innerHTML = calendar;
    g.value = D.getFullYear();
    m.selected = true;

    document.querySelector('#' + id + ' option[value="' + new Date().getMonth() + '"]').style.color = 'rgb(220, 0, 0)'; // в выпадающем списке выделен текущий месяц


}

function Calendar_prev(id, year, month) {
    month -= 1;
    if (month < 0) {
        month = 11;
        year -= 1;
    }
    var Dlast = new Date(year, month + 1, 0).getDate(),
        D = new Date(year, month, Dlast),
        DNlast = D.getDay(),
        DNfirst = new Date(D.getFullYear(), D.getMonth(), 1).getDay(),
        calendar = '<tr>',
        m = document.querySelector('#' + id + ' option[value="' + D.getMonth() + '"]'),
        g = document.querySelector('#' + id + ' input');
    if (DNfirst != 0) {
        for (var i = 1; i < DNfirst; i++) calendar += '<td>';
    } else {
        for (var i = 0; i < 6; i++) calendar += '<td>';
    }
    for (var i = 1; i <= Dlast; i++) {
        if (i == new Date().getDate() && D.getFullYear() == new Date().getFullYear() && D.getMonth() == new Date().getMonth()) {
            day = i;
            if (month < 10) {
                month = '0' + (D.getMonth() + 1);
            }
            if (i < 10) {
                day = '0' + i;
            }
            calendar += '<td class="clickDate today"  data-date="' + day + '.' + month + '.' + D.getFullYear() + '">' + i;
        } else {
            var month = D.getMonth() + 1,
                day = i;
            if (month < 10) {
                month = '0' + (D.getMonth() + 1);
            }
            if (i < 10) {
                day = '0' + i;
            }
            if (  // список официальных праздников
            (i == 1 && D.getMonth() == 0 && ((D.getFullYear() > 1897 && D.getFullYear() < 1930) || D.getFullYear() > 1947)) || // Новый год
            (i == 2 && D.getMonth() == 0 && D.getFullYear() > 1992) || // Новый год
            ((i == 3 || i == 4 || i == 5 || i == 6 || i == 8) && D.getMonth() == 0 && D.getFullYear() > 2004) || // Новый год
            (i == 7 && D.getMonth() == 0 && D.getFullYear() > 1990) || // Рождество Христово
            (i == 23 && D.getMonth() == 1 && D.getFullYear() > 2001) || // День защитника Отечества
            (i == 8 && D.getMonth() == 2 && D.getFullYear() > 1965) || // Международный женский день
            (i == 1 && D.getMonth() == 4 && D.getFullYear() > 1917) || // Праздник Весны и Труда
            (i == 9 && D.getMonth() == 4 && D.getFullYear() > 1964)  // День Победы

            ) {
                calendar += '<td class="clickDate holiday" data-date="' + day + '.' + month + '.' + D.getFullYear() + '"  >' + i;
            } else {
                calendar += '<td class="clickDate" data-date="' + day + '.' + month + '.' + D.getFullYear() + '"  >' + i;
            }
        }
        if (new Date(D.getFullYear(), D.getMonth(), i).getDay() == 0) {
            calendar += '<tr>';
        }
    }
    for (var i = DNlast; i < 7; i++) calendar += '<td>&nbsp;';
    document.querySelector('#' + id + ' tbody').innerHTML = calendar;
    g.value = D.getFullYear();
    m.selected = true;

    document.querySelector('#' + id + ' option[value="' + new Date().getMonth() + '"]').style.color = 'rgb(220, 0, 0)'; // в выпадающем списке выделен текущий месяц
}

function Calendar_next(id, year, month) {
    month += 1;
    if (month > 11) {
        month = 0;
        year += 1;
    }
    var Dlast = new Date(year, month + 1, 0).getDate(),
        D = new Date(year, month, Dlast),
        DNlast = D.getDay(),
        DNfirst = new Date(D.getFullYear(), D.getMonth(), 1).getDay(),
        calendar = '<tr>',
        m = document.querySelector('#' + id + ' option[value="' + D.getMonth() + '"]'),
        g = document.querySelector('#' + id + ' input');
    if (DNfirst != 0) {
        for (var i = 1; i < DNfirst; i++) calendar += '<td>';
    } else {
        for (var i = 0; i < 6; i++) calendar += '<td>';
    }
    for (var i = 1; i <= Dlast; i++) {
        if (i == new Date().getDate() && D.getFullYear() == new Date().getFullYear() && D.getMonth() == new Date().getMonth()) {
            day = i;
            if (month < 10) {
                month = '0' + (D.getMonth() + 1);
            }
            if (i < 10) {
                day = '0' + i;
            }
            calendar += '<td class="clickDate today"  data-date="' + day + '.' + month + '.' + D.getFullYear() + '">' + i;
        } else {
            var month = D.getMonth() + 1,
                day = i;
            if (month < 10) {
                month = '0' + (D.getMonth() + 1);
            }
            if (i < 10) {
                day = '0' + i;
            }
            if (  // список официальных праздников
            (i == 1 && D.getMonth() == 0 && ((D.getFullYear() > 1897 && D.getFullYear() < 1930) || D.getFullYear() > 1947)) || // Новый год
            (i == 2 && D.getMonth() == 0 && D.getFullYear() > 1992) || // Новый год
            ((i == 3 || i == 4 || i == 5 || i == 6 || i == 8) && D.getMonth() == 0 && D.getFullYear() > 2004) || // Новый год
            (i == 7 && D.getMonth() == 0 && D.getFullYear() > 1990) || // Рождество Христово
            (i == 23 && D.getMonth() == 1 && D.getFullYear() > 2001) || // День защитника Отечества
            (i == 8 && D.getMonth() == 2 && D.getFullYear() > 1965) || // Международный женский день
            (i == 1 && D.getMonth() == 4 && D.getFullYear() > 1917) || // Праздник Весны и Труда
            (i == 9 && D.getMonth() == 4 && D.getFullYear() > 1964)  // День Победы

            ) {
                calendar += '<td class="clickDate holiday" data-date="' + day + '.' + month + '.' + D.getFullYear() + '"  >' + i;
            } else {
                calendar += '<td class="clickDate" data-date="' + day + '.' + month + '.' + D.getFullYear() + '"  >' + i;
            }
        }
        if (new Date(D.getFullYear(), D.getMonth(), i).getDay() == 0) {
            calendar += '<tr>';
        }
    }
    for (var i = DNlast; i < 7; i++) calendar += '<td>&nbsp;';
    document.querySelector('#' + id + ' tbody').innerHTML = calendar;
    g.value = D.getFullYear();
    m.selected = true;

    document.querySelector('#' + id + ' option[value="' + new Date().getMonth() + '"]').style.color = 'rgb(220, 0, 0)'; // в выпадающем списке выделен текущий месяц
}


document.querySelector('#calendar2').onchange = function Kalendar() {
    Calendar("calendar2", document.querySelector('#calendar2 input').value, parseFloat(document.querySelector('#calendar2 select').options[document.querySelector('#calendar2 select').selectedIndex].value));

    Calendar_prev("calendar1", document.querySelector('#calendar2 input').value, parseFloat(document.querySelector('#calendar2 select').options[document.querySelector('#calendar2 select').selectedIndex].value));
    Calendar_next("calendar3", document.querySelector('#calendar2 input').value, parseFloat(document.querySelector('#calendar2 select').options[document.querySelector('#calendar2 select').selectedIndex].value));
}

document.querySelector('#calendar1').onchange = function Kalendar1() {
    Calendar_prev("calendar1", document.querySelector('#calendar2 input').value, parseFloat(document.querySelector('#calendar2 select').options[document.querySelector('#calendar2 select').selectedIndex].value));

    Calendar("calendar2", document.querySelector('#calendar2 input').value, parseFloat(document.querySelector('#calendar2 select').options[document.querySelector('#calendar2 select').selectedIndex].value));

    Calendar_next("calendar3", document.querySelector('#calendar2 input').value, parseFloat(document.querySelector('#calendar2 select').options[document.querySelector('#calendar2 select').selectedIndex].value));
}


//Calendar content

function Calendar_time() {

    var time = 0,
        d_hour = 0,
        d_min = 0,
        content = '<div class="time-calendar"><div id="icon-row" class="icon-row"> <span> <i uk-icon="icon: clock; ratio: 1.5"></i></span></div>';
    while (time != 1440) {
        d_hour = time / 60;
        d_min = time % 60;
        content += '<div id="time' + time + '" class="min-row" data-hour="' + parseInt(d_hour) + '"  data-min="' + d_min + '">';
        if (parseInt(d_hour) < 10) {
            content += '<span>0' + parseInt(d_hour) + ':';
            if (d_min < 10) {
                content += '0' + d_min + '</span>';
            } else {
                content += d_min + '</span>';
            }
        } else {
            content += '<span>' + parseInt(d_hour) + ':';
            if (d_min < 10) {
                content += '0' + d_min + '</span>';
            } else {
                content += d_min + '</span>';
            }
        }
        content += '</div>';
        time += 15;
    }
    content += '</div>';

    $("#content-calendar").append(content);

}

function Calendar_content() {
    var d = new Date();
    var curr_date = d.getDate();
    var curr_month = d.getMonth() + 1; //Months are zero based
    var curr_year = d.getFullYear();
    curr_date < 10 ? curr_date = '0' + curr_date : curr_date;
    let date = curr_date + '.' + curr_month + '.' + curr_year;
    $.ajax({
        type: "GET",
        cache: false,
        url: '/calendar/api/current_day/',
        dataType: "json",
        success: function (data) {
            console.log(data);
            var teachers = '';
            var css_teachers = [];
            var check = true;
            var temp;
            css_teachers[0] = [];
            css_teachers[0][0] = data[0].tutor_fullname;
            css_teachers[0][1] = 45;
            for (var i = 1; i < data.length; i++) {
                $.each(css_teachers, function (index, value) {
                    //console.log(value[0]);
                    if (value[0] == data[i].tutor_fullname) {
                        check = false;
                    }

                });

                if (check) {
                    temp = css_teachers.length;
                    //console.log(data[k].tutor_fullname);

                    css_teachers[temp] = [];
                    css_teachers[temp][0] = data[i].tutor_fullname;
                    css_teachers[temp][1] = 45 + (270 * (css_teachers.length - 1));
                }
                check = true;
            }

            //console.log(css_teachers);

            for (var i = 0; i < css_teachers.length; i++) {
                teachers += '<div class="tutor_column"><p class="uk-text-bold uk-text-emphasis uk-margin-remove">' + css_teachers[i][0] + '</p></div>';
            }

            for (var i = 0; i < data.length; i++) {

                var card = '';
                var start_time = data[i].begin_time;

                var start_parts = start_time.split(/[,\s\.\:]+/).map(Number);
                var start_hour = start_parts.shift();
                var start_min = start_parts.shift();
                var start_mins = (60 * start_hour) + start_min;

                var end_time = data[i].end_time;

                var end_parts = end_time.split(/[,\s\.\:]+/).map(Number);
                var end_hour = end_parts.shift();
                var end_min = end_parts.shift();
                var end_mins = (60 * end_hour) + end_min;

                var dif = (end_mins - start_mins) / 15;

                var height = (dif * 35) + 'px';
                var left = 45;
                var id = '#time' + start_mins;

                var css_mins = start_mins;

                $('#time' + (css_mins - 15)).css("display", "block");

                $.each(css_teachers, function (index, value) {
                    //console.log(value[0]);
                    if (value[0] == data[i].tutor_fullname) {
                        left = value[1];
                    }

                });

                for (var j = 0; j <= dif; j++) {
                    $('#time' + css_mins).css("display", "block");
                    css_mins += 15;
                }

                card = '<a href="'+ data[i].add_visits_url +'" target="_blank" class="card color' + left + '" style="height: ' + height + '; left:' + left + 'px;" data-date="' + date + '"><div class="uk-card-title uk-text-bold">' + data[i].name + '</div><div><b>Время:</b> ' + data[i].begin_time + ' - ' + data[i].end_time + '<div class="uk-text-bold">' + data[i].tutor_fullname + '</div></div></a>';

                $(id).append(card);

            }
            $(".icon-row").append(teachers);

        }
    });


    //console.log(obj[2]);
}


Calendar("calendar2", new Date().getFullYear(), new Date().getMonth());

Calendar_prev("calendar1", new Date().getFullYear(), new Date().getMonth());

Calendar_next("calendar3", new Date().getFullYear(), new Date().getMonth());

Calendar_time();

Calendar_content();

//Render calendar content by click
$(document).on('click', '.clickDate', function() {
    console.log($(this).data());
    $('.clickDate').removeClass("active");
    $(this).addClass("active");

    $(".content-calendar").empty();
    var dateDay = $(this).data();

    var formDate = JSON.stringify(dateDay);
    Calendar_time();
    console.log('Stringify: ', formDate);
    $.ajax({
        type: "POST",
        cache: false,
        data:  dateDay,
        url: '/calendar/api/schedule/',
        dataType: "json",
        success: function (data) {
            console.log('Output: ',data);
            var teachers = '';
            var css_teachers = [];
            var check = true;
            var temp;
            css_teachers[0] = [];
            css_teachers[0][0] = data[0].tutor_fullname;
            css_teachers[0][1] = 45;
            for (var i = 1; i < data.length; i++) {
                $.each(css_teachers, function (index, value) {
                    //console.log(value[0]);
                    if (value[0] == data[i].tutor_fullname) {
                        check = false;
                    }

                });

                if (check) {
                    temp = css_teachers.length;
                    //console.log(data[k].tutor_fullname);

                    css_teachers[temp] = [];
                    css_teachers[temp][0] = data[i].tutor_fullname;
                    css_teachers[temp][1] = 45 + (270 * (css_teachers.length - 1));
                }
                check = true;
            }

            //console.log(css_teachers);

            for (var i = 0; i < css_teachers.length; i++) {
                teachers += '<div class="tutor_column"><p class="uk-text-bold uk-text-emphasis uk-margin-remove">' + css_teachers[i][0] + '</p></div>';
            }

            for (var i = 0; i < data.length; i++) {

                var card = '';
                var start_time = data[i].begin_time;

                var start_parts = start_time.split(/[,\s\.\:]+/).map(Number);
                var start_hour = start_parts.shift();
                var start_min = start_parts.shift();
                var start_mins = (60 * start_hour) + start_min;

                var end_time = data[i].end_time;

                var end_parts = end_time.split(/[,\s\.\:]+/).map(Number);
                var end_hour = end_parts.shift();
                var end_min = end_parts.shift();
                var end_mins = (60 * end_hour) + end_min;

                var dif = (end_mins - start_mins) / 15;

                var height = (dif * 35) + 'px';
                var left = 45;
                var id = '#time' + start_mins;

                var css_mins = start_mins;

                $('#time' + (css_mins - 15)).css("display", "block");

                $.each(css_teachers, function (index, value) {
                    //console.log(value[0]);
                    if (value[0] == data[i].tutor_fullname) {
                        left = value[1];
                    }

                });

                for (var j = 0; j <= dif; j++) {
                    $('#time' + css_mins).css("display", "block");
                    css_mins += 15;
                }

                card = '<a href="'+ data[i].add_visits_url +'" target="_blank"  class="card color' + left + '" style="height: ' + height + '; left:' + left + 'px;" data-date="' + dateDay.date + '"><div class="uk-card-title uk-text-bold">' + data[i].name + '</div><div><b>Время:</b> ' + data[i].begin_time + ' - ' + data[i].end_time + '<div><b>Посещаемост:</b> 2/12 </div><div class="uk-text-bold">' + data[i].tutor_fullname + '</div></div></a>';

                $(id).append(card);

            }
            $(".icon-row").append(teachers);

        }
    });

});

$(document).on('click', '#current-day', function() {
    console.log($(this).data());
    $('.clickDate').removeClass("active");
    $(".content-calendar").empty();
    var date = $(this).data();

    Calendar_time();

    $.ajax({
        type: "GET",
        cache: false,
        data: date,
        url: '/calendar/api/current_day/',
        dataType: "json",
        success: function (data) {
            console.log(data);
            var teachers = '';
            var css_teachers = [];
            var check = true;
            var temp;
            css_teachers[0] = [];
            css_teachers[0][0] = data[0].tutor_fullname;
            css_teachers[0][1] = 45;
            for (var i = 1; i < data.length; i++) {
                $.each(css_teachers, function (index, value) {
                    //console.log(value[0]);
                    if (value[0] == data[i].tutor_fullname) {
                        check = false;
                    }

                });

                if (check) {
                    temp = css_teachers.length;
                    //console.log(data[k].tutor_fullname);

                    css_teachers[temp] = [];
                    css_teachers[temp][0] = data[i].tutor_fullname;
                    css_teachers[temp][1] = 45 + (270 * (css_teachers.length - 1));
                }
                check = true;
            }

            //console.log(css_teachers);

            for (var i = 0; i < css_teachers.length; i++) {
                teachers += '<div class="tutor_column"><p class="uk-text-bold uk-text-emphasis uk-margin-remove">' + css_teachers[i][0] + '</p></div>';
            }

            for (var i = 0; i < data.length; i++) {

                var card = '';
                var start_time = data[i].begin_time;

                var start_parts = start_time.split(/[,\s\.\:]+/).map(Number);
                var start_hour = start_parts.shift();
                var start_min = start_parts.shift();
                var start_mins = (60 * start_hour) + start_min;

                var end_time = data[i].end_time;

                var end_parts = end_time.split(/[,\s\.\:]+/).map(Number);
                var end_hour = end_parts.shift();
                var end_min = end_parts.shift();
                var end_mins = (60 * end_hour) + end_min;

                var dif = (end_mins - start_mins) / 15;

                var height = (dif * 35) + 'px';
                var left = 45;
                var id = '#time' + start_mins;

                var css_mins = start_mins;

                $('#time' + (css_mins - 15)).css("display", "block");

                $.each(css_teachers, function (index, value) {
                    //console.log(value[0]);
                    if (value[0] == data[i].tutor_fullname) {
                        left = value[1];
                    }

                });

                for (var j = 0; j <= dif; j++) {
                    $('#time' + css_mins).css("display", "block");
                    css_mins += 15;
                }

                card = '<a href="'+ data[i].add_visits_url +'" target="_blank"  class="card color' + left + '" style="height: ' + height + '; left:' + left + 'px;" data-date="' + date.date + '"><div class="uk-card-title uk-text-bold">' + data[i].name + '</div><div><b>Время:</b> ' + data[i].begin_time + ' - ' + data[i].end_time + '<div><b>Посещаемост:</b> 2/12 </div><div class="uk-text-bold">' + data[i].tutor_fullname + '</div></div></a>';

                $(id).append(card);

            }
            $(".icon-row").append(teachers);

        }
    });

});


//Save date in local storage
$(document).on('click', '.card', function() {
    var dateDay = $(this).data();
    console.log('Date', dateDay);
    localStorage.setItem('date', dateDay.date);
});




