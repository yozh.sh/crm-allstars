var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();



gulp.task('scripts', require('./gulp/scripts')(gulp, plugins));
gulp.task('sass', require('./gulp/sass')(gulp, plugins));

gulp.task('default', ['sass', 'scripts'], function () {
    gulp.watch('tobuild/js/**/*.js', ['scripts']);
    gulp.watch('tobuild/scss/**/*.{sass,scss}', ['sass']);
});