
module.exports = function (gulp, plugins) {
    return function () {
        gulp.src('tobuild/scss/*.scss')
            .pipe(plugins.sass())
            .pipe(plugins.cleanCss())
            .pipe(gulp.dest('todist/css'));
    };
};