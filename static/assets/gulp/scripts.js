module.exports = function (gulp, plugins) {
    return function () {
        gulp.src('tobuild/js/*.js')
            .pipe(plugins.terser())
            .pipe(gulp.dest('todist/js'));
    };
};