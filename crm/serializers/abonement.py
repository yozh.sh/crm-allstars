from crm.models import Abonement
from rest_framework import serializers


class AbonementSerializer(serializers.ModelSerializer):
    class Meta:
        model = Abonement
        fields = ('id', 'name')

