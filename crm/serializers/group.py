from rest_framework import serializers
from crm.models import CrmGroup


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = CrmGroup
        fields = "__all__"