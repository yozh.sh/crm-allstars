from rest_framework import serializers
from crm.models import Client, ScheduleCanceledGroups, VisitsToGroup, PreEntryClients
from crm.serializers.group import GroupSerializer
from crm.serializers.sale import SaleSerializer

class TutorSerializer(serializers.Serializer):
    full_name = serializers.CharField()


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = "__all__"


class CalendarScheduleCancelGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = ScheduleCanceledGroups
        fields = ('group', 'date')


class CalendarScheduleByDate(serializers.Serializer):
    date = serializers.DateField(format='%d.%m.%Y', input_formats=['%d.%m.%Y'])


class VisitSerializer(serializers.ModelSerializer):
    visit_date = serializers.DateField(format='%d.%m.%Y', input_formats=['%d.%m.%Y'])

    class Meta:
        model = VisitsToGroup
        exclude = ['visit_time', 'abonement']


class PreEntryVisitSerializer(serializers.ModelSerializer):
    date = serializers.DateField(format='%d.%m.%Y', input_formats=['%d.%m.%Y'])

    class Meta:
        model = PreEntryClients
        exclude = ('created_date', 'modified_date')


class PreEntryVisitsGroupSerializer(PreEntryVisitSerializer):
    group = GroupSerializer()
    client = ClientSerializer()


class VisitDetailSerializer(serializers.ModelSerializer):
    visit_date = serializers.DateField(format='%d.%m.%Y', input_formats=['%d.%m.%Y'])
    group = GroupSerializer()
    client = ClientSerializer()
    abonement = SaleSerializer()

    class Meta:
        model = VisitsToGroup
        exclude = ['visit_time']