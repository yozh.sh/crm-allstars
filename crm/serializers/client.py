from rest_framework import serializers


class NewClientTodaySerializer(serializers.Serializer):
    count = serializers.IntegerField()


class ClientCheckSubscriptionInGroup(serializers.Serializer):
    subscription = serializers.BooleanField()


