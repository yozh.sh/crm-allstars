from crm.models import CrmGroup, Abonement, Promotion, Sale
from rest_framework import serializers
from crm.serializers.group import GroupSerializer

class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = CrmGroup
        fields = ('id', 'name')


class AbonementSerializer(serializers.ModelSerializer):
    class Meta:
        model = Abonement
        fields = ('id', 'name')


class PromotionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Promotion
        fields = ('id', 'name')


class SaleSerializer(serializers.ModelSerializer):
    group = GroupSerializer(many=True, read_only=True)
    class Meta:
        model = Sale
        fields = [
            'abonement_name',
            'abonement_available_visits_number',
            'abonement_duration',
            'group',
            'tutor'
        ]
