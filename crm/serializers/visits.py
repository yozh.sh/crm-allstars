from rest_framework import serializers
from crm.models import VisitsToGroup


class TodayVisitsSerializer(serializers.ModelSerializer):
    class Meta:
        model = VisitsToGroup
        fields = ['client', 'visit_date', 'group', 'abonement']
