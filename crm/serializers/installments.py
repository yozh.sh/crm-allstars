from crm.models import Sale
from rest_framework import serializers
from crm.serializers.abonement import AbonementSerializer

class SaleSerializer(serializers.ModelSerializer):
    abonement = AbonementSerializer(many=True, read_only=True)
    class Meta:
        model = Sale
        fields = ('id', 'abonement', 'final_price')