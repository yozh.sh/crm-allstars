import unittest
from django.test import TestCase
from crm.models import Promotion


class PromotionTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.percentage_data = [
            {'money': 100, 'percent': 10, 'excepted_money': 90},
            {'money': 120, 'percent': 10, 'excepted_money': 108},
            {'money': 224, 'percent': 25, 'excepted_money': 168},
            {'money': 250, 'percent': 50, 'excepted_money': 125}
        ]

        cls.money_data = [
            {'money': 200, 'money_discount': 20, 'excepted_money': 180},
            {'money': 100, 'money_discount': 50, 'excepted_money': 50},
            {'money': 125, 'money_discount': 25, 'excepted_money': 100},
            {'money': 300, 'money_discount': 300, 'excepted_money': 0}
        ]

    def setUp(self) -> None:
        self.percent_instance_data = []
        self.money_instance_data = []

        # PERCENTAGE
        for counter, data in enumerate(self.percentage_data):
            cls = Promotion(
                name="PromotionTestInstance{}".format(counter),
                type_of_discount='PROCENT',
                discount=data['percent'],
            )
            self.percent_instance_data.append(cls)

        # MONEY
        for counter, data in enumerate(self.money_data):
            cls = Promotion(
                name="PromotionTestInstance{}".format(counter),
                type_of_discount='MONEY',
                discount=data['money_discount'],
            )
            self.money_instance_data.append(cls)

    def test_percent_data(self):
        for instance, test_data in zip(self.percent_instance_data, self.percentage_data):
            self.assertEqual(instance.get_discounted(test_data['money']), test_data['excepted_money'])

    def test_money_data(self):
        for instance, test_data in zip(self.money_instance_data, self.money_data):
            self.assertEqual(instance.get_discounted(test_data['money']), test_data['excepted_money'])




if __name__ == '__main__':
    unittest.main()
