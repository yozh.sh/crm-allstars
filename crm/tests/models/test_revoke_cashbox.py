from _fixtures.sale import SaleFixtureCase
from crm.models import CashBox


class TestRevokeCashbox(SaleFixtureCase):
    def setUp(self) -> None:
        self.sale = self.setUpSale(discount_type='MONEY', installment_plan=False)
        cashbox_id = self.sale.save_cashbox()
        self.cashbox = CashBox.objects.get(pk=cashbox_id)

    def test_cashbox_operation_type(self):
        self.assertEqual(self.cashbox.operation_type, "SALE")

    def test_cashbox_revoke_operation_type(self):
        cashbox_id = self.sale.revoke_sale()
        cashbox = CashBox.objects.get(pk=cashbox_id)
        self.assertEqual(cashbox.operation_type, 'REVOKE')