from _fixtures.sale import SaleFixtureCaseWithRandomDepartament
from crm.models import CashBox
from django.test import Client
import datetime


class TestSaleCashbox(SaleFixtureCaseWithRandomDepartament):
    def setUp(self) -> None:
        self.sale = self.setUpSale(activation=True, promotion=True, discount_type='MONEY')
        self.sale.save_cashbox()
        self.cashbox = CashBox.objects.first()
        self.http = Client()

    def test_create_date(self):
        self.assertIsNotNone(self.cashbox.create_date)

    def test_client(self):
        self.assertEqual(self.cashbox.client, self.client)

    def test_client_link(self):
        response = self.http.get(self.cashbox.client_link, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_FIO(self):
        self.assertEqual(self.cashbox.get_client(), "Tester Test Testerovich")

    def test_abonement(self):
        self.assertEqual(self.cashbox.get_abonement(), "TEST_ABON")

    def test_activation_false(self):
        new_sale = self.setUpSaleAnother(activation=False, group_name='Another test group', discount_type='MONEY')
        cashbox_id = new_sale.save_cashbox()
        cashbox = CashBox.objects.get(pk=cashbox_id)
        self.assertFalse(cashbox.get_activation())

    def test_activation_true(self):
        self.assertIsInstance(self.cashbox.get_activation(), datetime.date)
        activation_date = datetime.date.today()
        self.assertEqual(self.cashbox.get_activation(), activation_date)

    def test_promo_true(self):
        self.assertEqual(self.cashbox.get_promotion(), 'TEST PROMOTION')

    def test_promo_false(self):
        new_sale = self.setUpSaleAnother(activation=False, promotion=False, group_name='Another test group1', discount_type='MONEY')
        cashbox_id = new_sale.save_cashbox()
        cashbox = CashBox.objects.get(pk=cashbox_id)
        self.assertFalse(cashbox.get_promotion())
