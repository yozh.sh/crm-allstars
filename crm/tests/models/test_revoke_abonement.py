import unittest
from django.test import TestCase
from crm.models import Client, Tutor, CrmGroup, Abonement, PromotionToSale, \
    Sale, Installment
import datetime
from _fixtures.departament import DepartamentFixture
from _fixtures.user_profile import ProfileFixture

"""
Шаги для тестирования
1. Создать клиента
2. Создать педагога
3. Создать группу
4. Создать абонемент
5. Создать продажу
6. Установить все переменные в продажу
7. Сохранить продажу.
8. Проверить тест кейсы
9. Очистить базу от тестовых записей
"""


class RevokeWithoutInstallmentAbonementTestCase(DepartamentFixture, ProfileFixture, TestCase):
    def setUpClient(self):
        client = Client(
            name='Тест',
            surname='Тестович',
            middle_name='Джангович',
            sex=1,
            birthday=datetime.datetime.now(),
            phone_number='+380988962515'
        )
        client.save()
        return client

    def setUpTutor(self):
        return Tutor.objects.create(
            first_name='Тестовый',
            last_name='Преподователь'
        )

    def setUpGroup(self):
        # MONDAY
        begin_time_m = datetime.time(
            hour=17,
            minute=0
        ).isoformat()
        end_time_m = datetime.time(
            hour=17,
            minute=30
        ).isoformat()

        # FRIDAY
        begin_time_f = datetime.time(
            hour=16,
            minute=0
        ).isoformat()
        end_time_f = datetime.time(
            hour=16,
            minute=30
        ).isoformat()
        data = [
            {"day": 1, "begin_time": begin_time_m, "end_time": end_time_m},
            {"day": 2, "begin_time": None, "end_time": None},
            {"day": 3, "begin_time": None, "end_time": None},
            {"day": 4, "begin_time": None, "end_time": None},
            {"day": 5, "begin_time": begin_time_f, "end_time": end_time_f},
            {"day": 6, "begin_time": None, "end_time": None},
            {"day": 7, "begin_time": None, "end_time": None},
        ]
        return CrmGroup.objects.create(
            name='Test Group',
            rate=10,
            is_podushka=False,
            training_days=data,
            training_duration=30,
            one_visit_price=100,
        )

    def setUpAbonement(self):
        return Abonement.objects.create(
            name='Тестовый Абонемент',
            available_visits_number=48,
            duration=168,
            group_count='ONE',
            number_of_freezing=14,
            cost=3100
        )

    def setUpPromotion(self):
        return PromotionToSale.objects.create(
            name='2% Тестовая',
            type_of_discount='PROCENT',
            discount=2
        )

    def setUpSale(self, client: Client, tutor: Tutor,
                  group: CrmGroup, abonement: Abonement,
                  promo: PromotionToSale, user, departament):
        sale = Sale(
            saled_by=user.profile,
            saled_by_departament=departament,
            client=client,
            abonement_name='Test Abonement',
            abonement_available_visits_number=40,
            start_visits_number=40,
            current_visits_number=40,
            abonement_duration=168,
            abonement_group_count='ONE',
            abonement=abonement,
            activation=True,
            activation_date=datetime.datetime(year=2020, month=1, day=3),
            deactivation_date=datetime.datetime(year=2020, month=6, day=19),
            promotion=promo,
            installment_plan=False,
            saled_by_installment=False,
            abonement_price=3100,
            revoke=False,
            payment_type='CASH',
            end_payment=3038

        )

        sale.save()
        sale.tutor.add(tutor)
        sale.group.add(group)
        sale.save()
        return sale

    def setUp(self) -> None:
        self.departament = self.setUpDepartament()
        self.user = self.setUpUser(self.departament)
        self.client = self.setUpClient()
        self.tutor = self.setUpTutor()
        self.group = self.setUpGroup()
        self.abonement = self.setUpAbonement()
        self.promo = self.setUpPromotion()
        self.sale = self.setUpSale(
            user=self.user,
            departament=self.departament,
            client=self.client,
            tutor=self.tutor,
            group=self.group,
            abonement=self.abonement,
            promo=self.promo
        )

    def _get_excepted_value(self):
        return 1838

    def test_revoke_formula(self):
        revoke_result = self.sale.calculate_revoke_money()
        self.assertEqual(revoke_result, self._get_excepted_value())


class RevokeWithInstallmentAbonementTestCase(
    RevokeWithoutInstallmentAbonementTestCase):

    def setUp(self) -> None:
        super().setUp()
        self.installment = self.setUpInstallment(self.client, self.sale,
                                                 self.promo)

    def setUpInstallment(self, client: Client, sale: Sale,
                         promo: PromotionToSale):
        installment = Installment.objects.create(
            client=client,
            sale=sale,
            debt=promo.get_discounted(sale.abonement_price)
        )
        installment.save()
        installment.new_payment(sale.end_payment)
        installment.new_payment(500)
        installment.new_payment(850)
        return installment

    def setUpSale(self, client: Client, tutor: Tutor,
                  group: CrmGroup, abonement: Abonement,
                  promo: PromotionToSale, user, departament):
        sale = Sale(
            saled_by=user.profile,
            saled_by_departament=departament,
            client=client,
            abonement_name='Test Abonement',
            abonement_available_visits_number=40,
            start_visits_number=40,
            current_visits_number=40,
            abonement_duration=168,
            abonement_group_count='ONE',
            abonement=abonement,
            activation=True,
            activation_date=datetime.datetime(year=2020, month=1, day=3),
            deactivation_date=datetime.datetime(year=2020, month=6, day=19),
            promotion=promo,
            installment_plan=True,
            saled_by_installment=True,
            abonement_price=3100,
            revoke=False,
            payment_type='CASH',
            end_payment=1000

        )
        sale.save()
        sale.tutor.add(tutor)
        sale.group.add(group)
        return sale

    def test_installment_status(self):
        self.assertFalse(self.installment.paid_out)

    def _get_excepted_value(self):
        return 1150
