from django.test import TestCase
from _fixtures.installment import InstallmentFixture
from _fixtures.sale import SaleFixture
from _fixtures.tutor import TutorFixture
from _fixtures.group import GroupFixture
from _fixtures.abonement import AbonementFixture
from _fixtures.client import ClientFixture
from _fixtures.departament import DepartamentFixture
from _fixtures.user_profile import ProfileFixture


class TestInstallment(SaleFixture, InstallmentFixture, TutorFixture, GroupFixture, AbonementFixture, ClientFixture, ProfileFixture, DepartamentFixture, TestCase):
    def setUp(self) -> None:
        self.tutor = self.setUpTutor()
        self.group = self.setUpGroup()
        self.abonement = self.setUpAbonement()
        self.client = self.setUpClient()
        self.departament = self.setUpDepartament()
        self.user = self.setUpUser(self.departament)
        self.sale = self.setUpSale(
            client=self.client,
            tutor=self.tutor,
            group=self.group,
            user=self.user,
            departament=self.departament,
            abonement=self.abonement,
            promo=None,
            abonement_price=5000,
            payment=1000
        )
        self.installment = self.setUpInstallment(
            client=self.client,
            sale=self.sale,
            promo=None
        )

    def test_first_payment(self):
        self.installment.new_payment(self.sale.end_payment)
        self.assertEqual(self.installment.debt, 4000)

    def test_new_payment(self):
        self.installment_create_payment(1000)
        self.installment_create_payment(1000)
        self.assertEqual(self.installment.debt, 3000)

    def test_payment_sum(self):
        self.installment.new_payment(self.sale.end_payment)
        self.installment_create_payment(1000)
        self.installment_create_payment(1000)
        self.assertEqual(self.installment.get_sum_all_payments(), 3000)

    def test_payment_sum_2(self):
        self.installment_create_payment(1000)
        self.installment_create_payment(1000)
        self.installment_create_payment(1000)
        self.installment_create_payment(1000)
        self.assertEqual(self.installment.get_sum_all_payments(), 4000)