from _fixtures.sale import SaleFixtureCase
from crm.models import CashBox, Installment


class TestSaleInstallmentCashbox(SaleFixtureCase):
    def setUp(self) -> None:
        self.sale = self.setUpSale(activation=True, promotion=True, installment_plan=True, discount_type='MONEY')
        self.sale.client.block_by_installment()
        self.installment = Installment(
            client=self.sale.client,
            sale=self.sale,
            debt=5000
        )
        self.installment.save()

    def test_first_payment(self):
        self.installment.new_payment(money=self.sale.end_payment, first_payment=True)
        cashbox_record = CashBox.objects.filter(operation_type='INSTALLMENT').all()
        self.assertEqual(len(cashbox_record), len(list()))

    def test_another_payment(self):
        self.installment.new_payment(money=300)
        cashbox_record = CashBox.objects.filter(operation_type='INSTALLMENT').all()
        self.assertEqual(len(cashbox_record), len([0]))


