from django.test import TestCase, Client
from django.shortcuts import reverse

"""
Все django apps должны иметь этот тест
В тесте перечисляются все общественные url для этого приложения
Тестируется доступность страниц, то есть все страницы должны возвращать 200 код на GET запрос.
POST запросы тестируются отдельно
"""


class PagesTests(TestCase):
    def setUp(self) -> None:
        self.http = Client()
        self.pages = {
            'index': reverse('index'),
            'login': reverse('login'),
            'tutor_list': reverse('tutor_list'),
            'client_list': reverse('client_list'),
            'promo_list': reverse('promo_list'),
            'sofa_list': reverse('sofa_list'),
            'abon_list': reverse('abon_list'),
            'sale_list': reverse('sale_list'),
            'calendar_view': reverse('calendar_view')
        }

    def test_pages(self):
        for url_name in self.pages:
            resp = self.http.get(self.pages[url_name], follow=True)
            self.assertEqual(resp.status_code, 200, msg=f"FAILED {url_name}")

