from _fixtures.sale import SaleWithDepartamentCase
from django.db import IntegrityError


class DeacreseSaleTest(SaleWithDepartamentCase):
    CURRENT_VISITS = 40  # Please look in SaleCase in field current_visits_number, because value set in SaleCase class code

    def setUp(self) -> None:
        self.sale = self.setUpSale(departament_name='TESTDepartament')

    def test_deacrease(self):
        self.sale.decrease_visit()
        self.assertEqual(39, self.sale.current_visits_number)
        self.sale.decrease_visit()
        self.assertEqual(38, self.sale.current_visits_number)

    def test_disabling_sale(self):
        self.sale.current_visits_number = 1
        self.sale.save()
        self.sale.decrease_visit()
        self.assertFalse(self.sale.activation)

    def test_non_positive_number_of_visits(self):
        self.sale.current_visits_number = 0
        self.sale.save()
        self.assertRaises(IntegrityError, self.sale.decrease_visit)
