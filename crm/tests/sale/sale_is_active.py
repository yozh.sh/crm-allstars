from _fixtures.sale import SaleWithDepartamentCase
import datetime
from crm.models import Sale
from crm.utils.dt import datetime_now
import datetime


class SaleIsActive(SaleWithDepartamentCase):

    def test_is_active_true(self):
        sale = self.setUpSale(departament_name='Test',
                              activation_date=datetime.date(year=2020, month=1, day=18),
                              deactivation_date=datetime.date(year=2020, month=4, day=18))

        self.assertTrue(sale.is_active())

    def test_is_active_false(self):
        sale = self.setUpSale(departament_name='Test',
                              activation_date=datetime.date(year=2020, month=2, day=18),
                              deactivation_date=datetime.date(year=2020, month=3, day=18))

        self.assertFalse(sale.is_active())

    def test_is_active_false_activation_date_more(self):
        sale = self.setUpSale(departament_name='Test',
                              activation_date=datetime.date(year=2020, month=4, day=18),
                              deactivation_date=datetime.date(year=2020, month=8, day=18))

        self.assertFalse(sale.is_active())




    def setUpSale(self, activation=True, promotion=True, group_name='Test Group',
                  installment_plan=False, departament_name=None, abon_cost: int = 0,
                  activation_date=datetime_now(), deactivation_date=datetime_now()
                  ):
        client = self.setUpClient()
        self.setUpUserDepartament(departament_name)
        self.setUpUserProfile(self.departament)

        self.client = client
        abonement = self.setUpAbonement(abon_cost)
        if promotion:
            promo = self.setUpPromo(discount_type='money')
        else:
            promo = None

        group = self.setUpGroup(group_name)
        tutor = self.setUpTutor()
        sale = Sale(
            saled_by=self.user.profile,
            saled_by_departament=self.user.profile.departament,
            client=client,
            abonement_name=abonement.name,
            abonement_available_visits_number=40,
            start_visits_number=40,
            current_visits_number=40,
            abonement_duration=168,
            abonement_group_count='ONE',
            abonement=abonement,
            activation=activation,
            activation_date=activation_date,
            deactivation_date=deactivation_date,
            promotion=promo if promo else None,
            installment_plan=installment_plan,
            saled_by_installment=installment_plan,
            abonement_price=abonement.cost,
            revoke=False,
            payment_type='CASH',
            end_payment=self.PAYMENT

        )

        sale.save()
        sale.tutor.add(tutor)
        sale.group.add(group)
        sale.save()
        sale.save_cashbox()
        return sale