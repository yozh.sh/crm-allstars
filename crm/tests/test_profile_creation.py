from django.test import TestCase
from django.contrib.auth.models import User
from crm.models import Departament

class ProfileTest(TestCase):

    def setUp(self) -> None:
        self.user = User.objects.create(username='test', password='1234')
        self.departament = Departament.objects.create(name='Test Departament')

    def test_creation_profile(self):
        self.assertIsNotNone(self.user.profile)

    def test_exist_profile_departament(self):
        self.user.profile.departament = self.departament
        self.user.save()
        self.assertIsNotNone(self.user.profile.departament)