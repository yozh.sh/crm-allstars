from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages


class LoginMixin(LoginRequiredMixin):
    login_url = '/login/'
    redirect_field_name = 'login'
    # def dispatch(self, request, *args, **kwargs):
    #     print(request.user, "USER")


class MessageMixin(object):
    success_msg = None
    success_view_name = None

    def success_message(self, message: str):
        messages.add_message(self.request, messages.SUCCESS, message)

    def error_message(self, message: str):
        messages.add_message(self.request, messages.ERROR, message)
