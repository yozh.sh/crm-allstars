from celery.result import AsyncResult
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.postgres.fields import JSONField
from django.db.models import Sum
import datetime
import pytz
from crm.utils.dt import datetime_now
from crm.tasks import deactivate_abonement, activation_abonement
from crm.tasks import freeze_abonement, unfreeze_abonement
from django.shortcuts import reverse
import copy
from django.forms import ValidationError
from simple_history.models import HistoricalRecords
from typing import List
from crm.utils.models.model_managers import DepartamentModelManager
from typing import Union

TZKIEV = pytz.timezone('Europe/Kiev')

class BaseModel(models.Model):
    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Departament(BaseModel):
    """
        Поля service, type - для внутреннего использования
        service - указывает на то что значение служебное или нет
        type - здесь будет хранится запись ALL для филиалов

    """
    name = models.CharField(max_length=255, unique=True)
    service = models.BooleanField(default=False)    # Необходимо для служебного использования
    type = models.CharField(max_length=3, null=True, blank=True, unique=True)  # для значения ALL

    objects = DepartamentModelManager()
    objects_default = models.Manager()

    def __str__(self):
        return self.name


class Profile(BaseModel):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, blank=True)
    departament = models.ForeignKey(Departament, on_delete=models.SET_NULL, null=True, blank=True)

    @receiver(post_save, sender=User)
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance)

    @receiver(post_save, sender=User)
    def save_user_profile(sender, instance, **kwargs):
        instance.profile.save()

    def __str__(self):
        if self.user.email:
            return self.user.email
        return self.user.username


class Tutor(BaseModel):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    history = HistoricalRecords()

    def get_rus_name(self):
        return 'Преподователи'

    def get_mapped_labels(self):
        return {
            'first_name': 'Имя',
            'last_name': 'Фамилия'
        }

    def __str__(self):
        return self.first_name + ' ' + self.last_name

    @property
    def full_name(self):
        return "{} {}".format(self.first_name, self.last_name)


class CrmGroup(BaseModel):
    # in minutes
    HALF_HOUR = 30              # minute
    HOUR = 60                   # minute
    HALF_AN_HOUR = 90           # minute
    TWO_HOURS = 120             # minute
    THREE_HOUR = 180            # minute

    TRAINING_DURATION = (
        (HALF_HOUR, 'Пол часа'),
        (HOUR, 'Один час'),
        (HALF_AN_HOUR, 'Полтора часа'),
        (TWO_HOURS, 'Два часа'),
        (THREE_HOUR, 'Три часа'),
    )

    name = models.CharField(max_length=255, unique=True, db_index=True)
    rate = models.IntegerField()
    is_podushka = models.BooleanField(default=False)
    podushka_cost = models.IntegerField(default=0)
    training_days = JSONField(default=dict)
    training_duration = models.IntegerField(
        choices=TRAINING_DURATION,
        default=HOUR
    )
    one_visit_price = models.IntegerField(default=150)
    departament = models.ForeignKey(Departament, on_delete=models.SET_NULL, null=True, blank=True)
    tutor = models.ForeignKey("Tutor", null=True, blank=True, on_delete=models.SET_NULL)
    history = HistoricalRecords()
    # training_hall

    def get_rus_name(self):
        return 'Группы'

    def get_mapped_labels(self):
        return {
            'name': 'Имя',
            'rate': 'Ставка',
            'is_podushka': 'Подушка',
            'one_visit_price': 'Цена за одно посещение',
            'training_duration': 'Длительность занятия',
            'tutor': 'Педагог'
        }

    def __str__(self):
        return self.name

    # day in ISO week
    def get_day(self, day: int):
        data = dict()
        for days in self.training_days:
            if days['day'] == day and days['end_time'] and days['begin_time'] != None:
                data.update(days)
        return data

    def get_training_days(self):
        today = datetime.date.today()
        data = dict()
        copy_days = copy.deepcopy(self.training_days)
        for day in copy_days:
            day.update(dict(date=datetime_now() - datetime.timedelta(days=day.get('day'))))
        return data


class Client(models.Model):
    MALE = 1
    FEMALE = 0

    SEX = (
        (MALE, 'Мужской'),
        (FEMALE, 'Женский'),
    )

    surname = models.CharField(max_length=255, db_index=True)  # Фамилия
    name = models.CharField(max_length=255, db_index=True)
    middle_name = models.CharField(max_length=255, null=True, blank=True, db_index=True)  # Отчество
    sex = models.IntegerField(
        choices=SEX,
        default=None,
        null=False
    )
    birthday = models.DateField()
    phone_number = models.CharField(max_length=255, null=True, blank=True, db_index=True)
    # card number TODO: спросить у Коли что такое Номер карты
    source_of_attraction = models.ForeignKey('SourceOfAttraction', on_delete=models.PROTECT, null=True)
    parent_FIO = models.CharField(max_length=255, null=True, blank=True)
    parent_phone_number = models.CharField(max_length=255, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    notes = models.TextField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated = models.DateTimeField(auto_now=True, null=True, blank=True)
    blocked_by_installment = models.BooleanField(default=False)
    history = HistoricalRecords(excluded_fields=['blocked_by_installment', 'created', 'updated'])

    def get_mapped_labels(self):
        return {
            "surname": "Фамилия",
            "name": "Имя",
            "middle_name": "Отчество",
            "sex": "Пол",
            "birthday": "Дата рождения",
            "phone_number": "Телефонный номер",
            "source_of_attraction": "Источник привлечения",
            "parent_FIO": "ФИО родителя",
            "parent_phone_number": "Телефонный номер родителя",
            "email": "Электронная почта",
            "notes": "Заметки"
        }

    def get_rus_name(self):
        return "Клиенты"

    def __str__(self):
        return self.surname + ' ' + self.name

    def fullname(self):
        return self.__str__()

    def get_fio(self):
        return "{surname} {first_name} {middle_name}".format(
            surname=self.surname,
            first_name=self.name,
            middle_name=self.middle_name if self.middle_name else ''
        )

    def get_last_active_abon(self):
        try:
            sale = Sale.objects.filter(client_id=self.id, activation=True).order_by('-created')[0]
            return sale
        except IndexError:
            return None
    
    def block_by_installment(self):
        self.blocked_by_installment = True
        self.save()
    
    def unblock_by_installment(self):
        self.blocked_by_installment = False
        self.save(update_fields=["blocked_by_installment"])

    def check_abonement_visit(self, group) -> Union[bool, models.Model]:
        # Проверяет может ли клиент записаться в эту группу
        sale = self.sale_set.filter(group=group, activation=True, abonement_available_visits_number__gte=1).first()
        if not sale:
            return False
        return sale


class Abonement(models.Model):
    # GROUP COUNT
    ONE = 'ONE'
    TWO = 'TWO'
    ALL = 'ALL'

    GROUP_COUNT = (
        (ONE, 'Одна'),
        (TWO, 'Две'),
        (ALL, 'ВСЕ')
    )

    name = models.CharField(max_length=255, db_index=True)
    available_visits_number = models.IntegerField()  # количество доступных посещений в штуках(днях)
    duration = models.IntegerField()  # длительность в днях
    group_count = models.CharField(
        max_length=255,
        choices=GROUP_COUNT,
        default=ONE
    )
    number_of_freezing = models.IntegerField(default=0)  # количество дней заморозки
    cost = models.IntegerField()
    history = HistoricalRecords()

    def get_rus_name(self):
        return 'Абонементы'

    def get_mapped_labels(self):
        return {
            'name': 'Имя',
            'available_visits_number': 'Количество посещений',
            'duration': 'Длительность(в днях)',
            'group_count': 'Количество групп',
            'number_of_freezing': 'Количество дней заморозки',
            'cost': 'Цена',
        }

    def __str__(self):
        return self.name


class Promotion(models.Model):
    PROCENT = 'PROCENT'
    MONEY = 'MONEY'

    TYPE = (
        (PROCENT, 'Проценты'),
        (MONEY, 'Гривны')
    )

    name = models.CharField(max_length=255)
    type_of_discount = models.CharField(choices=TYPE, max_length=120, default=MONEY)
    discount = models.IntegerField() # или в процентах или в гривнах
    temporary = models.BooleanField(default=False)
    start_date = models.DateField(null=True, blank=True)
    end_date = models.DateField(null=True, blank=True)
    active = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True)
    history = HistoricalRecords()

    def get_rus_name(self):
        return "Акции"

    def get_mapped_labels(self):
        return {
            'name': 'Имя',
            'type_of_discount': 'Тип скидки',
            'discount': 'Скидка',
            'temporary': 'Временная',
        }

    def __str__(self):
        return self.name

    def get_discounted(self, money: int) -> int:
        if self.type_of_discount == self.PROCENT:
            return round(money * (100 - self.discount) / 100)
        return money - self.discount


class SourceOfAttraction(models.Model):
    name = models.CharField(max_length=255)
    history = HistoricalRecords()

    def get_rus_name(self):
        return "Источники привлечения"

    def get_mapped_labels(self):
        return {
            'name': 'Название'
        }

    def __str__(self):
        return self.name


class SaleFormData(BaseModel):

    order_departament1 = models.PositiveIntegerField(null=True, blank=True, db_index=True)
    order_departament2 = models.PositiveIntegerField(null=True, blank=True, db_index=True)
    order_tutor1 = models.PositiveIntegerField(null=True, blank=True, db_index=True)
    order_tutor2 = models.PositiveIntegerField(null=True, blank=True, db_index=True)
    order_group1 = models.PositiveIntegerField(null=True, blank=True, db_index=True)
    order_group2 = models.PositiveIntegerField(null=True, blank=True, db_index=True)
    all_groups = models.BooleanField(default=False)

    sale = models.ForeignKey("Sale", on_delete=models.CASCADE, null=True)

    def get_one_group_instance(self) -> List[CrmGroup]:
        return [CrmGroup.objects.get(pk=self.order_group1)]

    def get_two_group_instance(self) -> List[CrmGroup]:
        return list(CrmGroup.objects.filter(id__in=[self.order_group1, self.order_group2]).all())

    def get_all_group_instance(self) -> List[CrmGroup]:
        return list(CrmGroup.objects.all())

    def get_one_tutor_instance(self) -> List[Tutor]:
        return [Tutor.objects.get(pk=self.order_tutor1)]

    def get_two_tutor_instance(self) -> List[Tutor]:
        return list(Tutor.objects.filter(id__in=[self.order_tutor1, self.order_group2]).all())


class Sale(models.Model):
    # PAYMENT TYPE
    CASH = 'CASH'
    CARD = 'CARD'
    RETURN = 'RETURN'

    PAYMENT_TYPE = (
        (CASH, 'Наличные'),
        (CARD, 'Карта'),

    )

    # GROUP COUNT
    ONE = 'ONE'
    TWO = 'TWO'
    ALL = 'ALL'

    GROUP_COUNT = (
        (ONE, 'Одна'),
        (TWO, 'Две'),
        (ALL, 'ВСЕ')
    )

    saled_by = models.ForeignKey('Profile', on_delete=models.PROTECT)
    saled_by_departament = models.ForeignKey('Departament', on_delete=models.PROTECT)
    abonement_name = models.CharField(max_length=255, db_index=True)
    abonement_available_visits_number = models.IntegerField()  # количество доступных посещений в штуках(днях)
    start_visits_number = models.IntegerField()  # сколько было по старту
    current_visits_number = models.PositiveIntegerField()  # Сколько осталось на текущий момент
    abonement_duration = models.IntegerField()  # длительность в днях
    client = models.ForeignKey('Client', on_delete=models.PROTECT, null=True, blank=True)
    abonement_group_count = models.CharField(
        max_length=255,
        choices=GROUP_COUNT,
        default=ONE
    )
    abonement_number_of_freezing = models.IntegerField(default=0)   # Количество доступных заморозок
    abonement = models.ForeignKey(Abonement, on_delete=models.SET_NULL, null=True, blank=True)  # Использованый шаблон для абонемента
    tutor = models.ManyToManyField('Tutor')
    group = models.ManyToManyField('CrmGroup')
    activation = models.BooleanField(default=False, db_index=True)
    activation_date = models.DateField(null=True, blank=True, default=datetime.datetime.now)
    deactivation_date = models.DateField(null=True, blank=True)
    promotion = models.ForeignKey('PromotionToSale', on_delete=models.PROTECT, blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated = models.DateTimeField(auto_now=True, null=True, blank=True)
    installment_plan = models.BooleanField(default=False)
    saled_by_installment = models.BooleanField(default=False, db_index=True)
    payment_type = models.CharField(
        max_length=120,
        choices=PAYMENT_TYPE,
        default=CASH
    )
    end_payment = models.PositiveIntegerField(default=0)
    abonement_price = models.PositiveIntegerField(default=0)
    revoke = models.BooleanField(default=False, db_index=True)
    revoked_money = models.IntegerField(blank=True, null=True)
    archived = models.BooleanField(default=False)
    celery_task_id = models.CharField(max_length=36, blank=True, null=True)
    freezed = models.BooleanField(default=False)
    history = HistoricalRecords()
    # freeze_date = models.DateField(null=True, blank=True)
    # unfreeze_date = models.DateField(null=True, blank=True)


    MAPPED_LABELS = {
        'abonement_name': 'Имя абонемента',
        'abonement_available_visits_number': 'Доступные посещения',
        'start_visits_number': 'Начальное количество посещений',
        'current_visits_number': 'Текущее количество посещений',
        'abonement_duration': 'Длительность абонемента',
        'abonement_number_of_freezing': 'Количество заморозок',
        'end_payment': 'Сумма платежа',
        'abonement_price': 'Стоимость абонемента'
    }

    def save(self, *args, **kwargs):

        if not self.id:
            pass
        else:
            # Если при рассрочке менялся end_payment то надо обновить первую оплату по рассрочке
            old_value = Sale.objects.filter(pk=getattr(self, "pk", None)).first()
            if old_value:
                if old_value.end_payment != self.end_payment and (old_value.installment_plan and self.installment_plan):
                    installment = Installment.objects.filter(sale_id=self.id).first()
                    payment = installment.installmentpayments_set.filter(first_payment=True).first()
                    payment.payment = self.end_payment
                    payment.save()
                    installment.recalc_debt()
                    installment.save()
        super(Sale, self).save(*args, **kwargs)


    def get_income_money(self, date_to, date_from):
        money = 0
        if self.saled_by_installment:
            installments = self.installment_set.filter(created__range=(date_to, date_from))
            for installment in installments:
                sum_by_installment = installment.installmentpayments_set.filter(created__range=(date_to, date_from)).\
                    aggregate(sum=Sum('payment'))
                money += sum_by_installment['sum']
        else:
            money = self.end_payment

        return money


    def get_count_payments_by_installment(self, date_to, date_from):
        count = 0
        installments = self.installment_set.filter(created__range=(date_to, date_from)).all()
        for installment in installments:
            count += installment.installmentpayments_set.\
                filter(created__range=(date_to, date_from)).count()
        return count

    def is_active(self):
        now = datetime_now()
        try:
            activation_date = datetime.datetime(
                year=self.activation_date.year,
                month=self.activation_date.month,
                day=self.activation_date.day,
                tzinfo=TZKIEV
            )
            deactivation_date = datetime.datetime(
                year=self.deactivation_date.year,
                month=self.deactivation_date.month,
                day=self.deactivation_date.day,
                tzinfo=TZKIEV
            )
        except AttributeError:
            return False
        if (activation_date is not None and now >= activation_date) and (deactivation_date is not None and now < deactivation_date):
            return True
        return False

    def decrease_visit(self):
        if self.current_visits_number == 1:
            self.current_visits_number = 0
            self.deactivate_now()
        else:
            self.current_visits_number -= 1

        self.save()

    def freeze(self, days: int):
        self.freezed = True
        self.deactivation_date = self.deactivation_date + datetime.timedelta(days=days)
        self.save(update_fields=['deactivation_date', 'freezed'])
        self.run_deactivate_task(update=True)

    def run_freeze(self, days: int, begin_date: datetime.date) -> None:
        freeze_abonement.apply_async((self.id, days), eta=begin_date)

    def run_unfreeze(self, days: int, freeze_date: datetime.date):
        unfreeze_date = freeze_date + datetime.timedelta(days=days)
        unfreeze_abonement.apply_async((self.id, ), eta=unfreeze_date)

    def unfreeze(self):
        self.freezed = False
        self.save(update_fields=['freezed'])

    def run_activation_task(self, update=False):
        if update and self.celery_task_id:
            task = AsyncResult(self.celery_task_id)
            task.revoke(terminate=True)
        task = activation_abonement.apply_async(self.id, eta=self.activation_date)
        self.celery_task_id = task.id
        self.save()

    def cancel_activation_task(self):
        task = AsyncResult(self.celery_task_id)
        task.revoke(terminate=True)
        self.celery_task_id = None
        self.save()

    def run_deactivate_task(self, update=False):
        if update and self.celery_task_id:
            task = AsyncResult(self.celery_task_id)
            task.revoke(terminate=True)
        task = deactivate_abonement.apply_async((self.id, ), eta=self.deactivation_date)
        self.celery_task_id = task.id
        self.save()

    def cancel_deactivate_task(self):
        task = AsyncResult(self.celery_task_id)
        task.revoke()
        self.celery_task_id = None
        self.save()

    def set_zero_freeze(self):
        self.abonement_number_of_freezing = 0

    def set_zero_abonement_available_visits_number(self):
        self.abonement_available_visits_number = 0

    def set_zero_current_visits_number(self):
        self.current_visits_number = 0

    def calculate_revoke_money(self):
        calc = self.abonement.available_visits_number - self.current_visits_number
        calc = calc * 150
        return self._revoke_sale_get_abon_cost() - calc

    def revoke_sale(self):
    #     возвращаем деньги
        self.revoked_money = self.calculate_revoke_money()
        self.save()
    #   Отменяем оставшиеся посещения
        self.cancellation_visits()
    #   Анулируем количество доступных посещений и количество текущих посещений и заморозок у продажи
        self.set_zero_freeze()
        self.set_zero_abonement_available_visits_number()
        self.set_zero_current_visits_number()
    #   Деактивируем сразу
        self.deactivate_now()
        self.revoke = True
        self.save()
        return self.save_cashbox(revoke=True)

    def _revoke_sale_get_abon_cost(self):
        if self.saled_by_installment:
            installments = Installment.objects.filter(sale_id=self.id).first()
            return installments.get_sum_all_payments()
        if self.promotion:
            return self.promotion.get_discounted(self.abonement_price)
        return self.abonement_price

    def cancellation_visits(self):
        self.current_visit_number = 0
        self.save()

    def deactivate_now(self):
        self.deactivation_date = datetime.datetime.now()
        self.activation = False
        self.save()

    def get_abonement(self):
        return self.abonement
    
    def full_price(self):
        if self.promotion:
            return self.promotion.get_discounted(self.abonement_price)
        return self.abonement_price

    def get_status(self):

        # GREEN
        if self.activation and self.installment_plan == False and not self.freezed:
            return "bg-success"
        # RED
        elif self.deactivation_date is not None and datetime.date.today() > self.deactivation_date or self.abonement_available_visits_number == 0:
            return "bg-danger"
        # YELLOW
        elif self.activation and self.installment_plan:  # or self.abonement.available_visits_number == 0
            return "bg-warning"
        # BLUE
        # elif абонемент заморожен
        elif self.freezed:
            return "bg-info"

        elif self.activation is False:
            return "bg-dark"

    def set_abonement_values(self, name: str, available_visits_number: int, duration: int, number_of_freezing: int, price: int):
        self.abonement_name = name
        self.abonement_available_visits_number = available_visits_number
        self.abonement_duration = duration
        self.abonement_number_of_freezing = number_of_freezing
        self.abonement_price = price
        self.start_visits_number = available_visits_number
        self.current_visits_number = available_visits_number

    @staticmethod
    def get_sales_prefetch(client_id):
        return Sale.objects.prefetch_related('group', 'abonement', 'client').filter(client_id=client_id)

    def get_deactivation_date(self, date: datetime.datetime):
        return date + datetime.timedelta(days=self.abonement_duration)

    def update_abonement_from_client_info(self, available_visits_number: int, number_of_freezing: int, activation: bool, **kwargs):
        self.abonement_available_visits_number = available_visits_number
        self.abonement_number_of_freezing = number_of_freezing
        if activation:
            self.activation = activation
            self.activation_date = kwargs.get('activation_date')
            self.deactivation_date = kwargs.get('deactivation_date')

    def close_installment(self):
        self.installment_plan = False
        self.save()

    def delete_sale(self):
        cashbox = CashBox.objects.filter(abonement_id=self.id, operation_type='SALE').first()
        # print(cashbox, "SALE DATA FROM CASHBOX")
        cashbox.delete()
        self.archived = True
        self.save()

    def save_cashbox(self, revoke=False):
        installment = None
        if self.installment_plan:
            installment = Installment.objects.filter(sale_id=self.id).first()

        cashbox = CashBox(
            create_date=self.created,
            client=self.client,
            abonement=self,
            activation=self.activation,
            activation_date=self.activation_date,
            promotion=self.promotion,
            is_promotion=True if self.promotion else False,
            installment=installment,
            is_installment=self.installment_plan,
            payment_type='Возврат' if revoke else self.get_payment_type_display(),
            price=self.revoked_money if revoke else self.end_payment,
            operation_type='REVOKE' if revoke else 'SALE'
        )
        cashbox.save()
        cashbox.group.set(self.group.all())
        cashbox.save()
        return cashbox.id

    def update_cashbox(self, revoke=False):
        cashbox = self.cashbox_set.last()
        installment = None
        if self.installment_plan:
            installment = Installment.objects.filter(sale_id=self.id).first()

        cashbox.client = self.client
        cashbox.abonement = self
        cashbox.activation = self.activation
        cashbox.activation_date = self.activation_date
        cashbox.promotion = self.promotion
        cashbox.is_promotion = True if self.promotion else False
        cashbox.installment = installment
        cashbox.is_installment = self.installment_plan
        cashbox.payment_type = 'Возврат' if revoke else self.get_payment_type_display()
        cashbox.price = self.revoked_money if revoke else self.end_payment
        cashbox.operation_type = 'REVOKE' if revoke else 'SALE'
        cashbox.group.clear()
        cashbox.group.set(self.group.all())
        cashbox.save()

        return cashbox.id

    def get_abonement_name(self):
        return self.abonement_name



class PromotionToSale(models.Model):
    # Неизменяемые данные в таблице.
    # Данные копируются с Promotion а потом Продажа привязывается к записи в этой таблице
    PROCENT = 'PROCENT'
    MONEY = 'MONEY'

    TYPE = (
        (PROCENT, 'Проценты'),
        (MONEY, 'Гривны')
    )

    name = models.CharField(max_length=255)
    type_of_discount = models.CharField(choices=TYPE, max_length=120,
                                        default=MONEY)
    discount = models.IntegerField()  # или в процентах или в гривнах
    temporary = models.BooleanField(default=False)
    start_date = models.DateField(null=True, blank=True)
    end_date = models.DateField(null=True, blank=True)
    active = models.BooleanField(default=True)
    template_promotion = models.ForeignKey(Promotion, on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return self.name

    def get_discounted(self, money: int) -> int:
        if self.type_of_discount == self.PROCENT:
            return round(money * (100 - self.discount) / 100)
        return money - self.discount


class Installment(models.Model):
    client = models.ForeignKey(Client, on_delete=models.PROTECT)
    sale = models.ForeignKey(Sale, on_delete=models.PROTECT)
    created = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    debt = models.PositiveIntegerField(default=0)  # общая сумма долга
    active = models.BooleanField(default=True)
    paid_out = models.BooleanField(default=False)  # выплачена ли рассрочка

    def get_abonement(self):
        return self.sale.get_abonement()

    def new_payment(self, money: int, first_payment=False):
        debt = self.debt - money
        if debt == 0:
            self.debt = debt
            self.save()
            self.close_installment()
        else:
            InstallmentPayments.objects.create(
                client=self.client,
                installment=self,
                payment=money,
                first_payment=first_payment
            )
            self.debt -= money
            self.save()

    
    def get_price(self):
        return self.sale.full_price()
    
    def validate_payment_money(self, money: int) -> bool:
        if money > self.debt:
            return False
        return True

    def payment(self, payment: int):
        debt = self.debt - payment
        if debt == 0:
            self.debt = debt
            self.save()
            self.close_installment()
        elif debt < 0:
            raise ValidationError("Внесенная сумма не может быть больше суммы долга")
        else:
            self.debt -= payment
            self.save()
        
    def close_installment(self):
        self.active = False
        self.paid_out = True
        self.client.unblock_by_installment()
        self.sale.close_installment()
        self.save()
    
    def force_close_installment(self):
        if self.paid_out:
            return
        self.debt = 0
        self.active = False
        self.paid_out = True
        self.client.unblock_by_installment()
        self.sale.close_installment()
        self.save()

    def get_all_payments(self):
        return self.installmentpayments_set.all()

    def get_sum_all_payments(self) -> int:
        return self.installmentpayments_set.aggregate(Sum('payment')).get('payment__sum')

    def recalc_debt(self, commit=False):
        price = self.get_price()
        payments = self.installmentpayments_set.aggregate(total=Sum('payment'))
        self.debt = price - payments['total']
        if commit:
            self.save()


class InstallmentPayments(models.Model):
    client = models.ForeignKey(Client, on_delete=models.PROTECT)
    installment = models.ForeignKey(Installment, on_delete=models.CASCADE)
    payment = models.PositiveIntegerField()
    created = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    create_date = models.DateField(blank=True, null=True, default=datetime_now)
    first_payment = models.BooleanField(default=False)

    def create_payment(self, client, installment):
        self.client = client
        self.installment = installment
        self.installment.payment(self.payment)
        self.save()

    def save_cashbox(self):
        cashbox = CashBox(
            create_date=self.created,
            client=self.client,
            abonement=self.installment.sale,
            activation=self.installment.sale.activation,
            activation_date=self.installment.sale.activation_date,
            promotion=self.installment.sale.promotion,
            is_promotion=True if self.installment.sale.promotion else False,
            installment=self.installment,
            is_installment=True,
            payment_type='Взнос',
            price=self.payment,
            operation_type='INSTALLMENT',
            payment_installment=self
        )
        cashbox.save()
        cashbox.group.set(self.installment.sale.group.all())
        cashbox.save()

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        if not self.first_payment:
            print("SAVE CASHBOX")
            self.save_cashbox()



    

# class CrmHistory(models.Model):
#     PAYMENT = 'PAYMENT'
#     VISIT = 'GROUP_VISIT'
#     RETURN = 'ABON_RETURN'
#     OPERATION_TYPES = (
#         (PAYMENT, 'Оплата'),
#         (VISIT, 'Посещение'),
#         (RETURN, 'Возврат')
#     )
#     type = models.CharField(
#         max_length=50,
#         choices=OPERATION_TYPES,
#         default=PAYMENT
#     )
#     sale = models.ForeignKey(Sale, on_delete=models.PROTECT)
#     created = models.DateTimeField(auto_now_add=True)


class CashBox(models.Model):
    SALE = 'SALE'
    INSTALLMENT = 'INSTALLMENT'
    REVOKE = 'REVOKE'

    OPERATION_TYPES = (
        (SALE, 'Продажа'),
        (INSTALLMENT, 'Рассрочка'),
        (REVOKE, 'Возврат')
    )

    create_date = models.DateTimeField()
    updated_at = models.DateTimeField(auto_now=True)
    client = models.ForeignKey(Client, on_delete=models.PROTECT)
    client_link = models.CharField(max_length=255)
    group = models.ManyToManyField(CrmGroup)
    abonement = models.ForeignKey(Sale, on_delete=models.PROTECT)
    activation = models.BooleanField(default=True)
    activation_date = models.DateField(blank=True, null=True)
    promotion = models.ForeignKey(PromotionToSale, on_delete=models.PROTECT, null=True, blank=True)
    is_promotion = models.BooleanField(default=False)
    installment = models.ForeignKey(Installment, on_delete=models.PROTECT, null=True, blank=True)
    is_installment = models.BooleanField(default=False)
    payment_type = models.CharField(max_length=255, blank=True, null=True)
    price = models.PositiveIntegerField()
    operation_type = models.CharField(max_length=100,
                                      choices=OPERATION_TYPES,
                                      default=SALE
                                      )
    payment_installment = models.ForeignKey(InstallmentPayments, on_delete=models.SET_NULL, blank=True, null=True)
    archived = models.BooleanField(default=False)

    def get_groups(self):
        return self.abonement.group.all()

    def set_client_link(self):
        self.client_link = reverse('client_info_information', args=[self.client.id])

    def save(self, *args, **kwargs):
        self.set_client_link()
        super().save(*args, **kwargs)

    def get_activation(self):
        if self.operation_type == self.SALE or self.REVOKE:
            if self.activation:
                return self.activation_date
            return self.activation
        elif self.operation_type == self.INSTALLMENT:
            if self.activation:
                return self.activation_date
            return self.activation

    def get_client(self):
        if self.operation_type == self.SALE or self.REVOKE:
            return self.client.get_fio()
        elif self.operation_type == self.INSTALLMENT:
            return self.client.get_fio()

    def get_abonement(self):
        if self.operation_type == self.SALE or self.REVOKE:
            return self.abonement.get_abonement_name()
        if self.operation_type == self.INSTALLMENT:
            return self.abonement.get_abonement_name()

    def get_promotion(self):
        if self.operation_type == self.SALE or self.REVOKE:
            if self.is_promotion:
                return self.promotion.name
            return self.is_promotion
        if self.operation_type == self.INSTALLMENT:
            if self.is_promotion:
                return self.promotion.name
            return self.is_promotion

    def get_installment(self):
            return self.is_installment

    def get_payment_type(self):
        if self.operation_type == self.SALE or self.REVOKE:
            return self.payment_type
        elif self.operation_type == self.INSTALLMENT:
            return self.payment_type

    def hide_revoke(self):
        self.archived = True
        self.save()


class VisitsToGroup(models.Model):
    client = models.ForeignKey('Client', on_delete=models.PROTECT)
    group = models.ForeignKey('CrmGroup', on_delete=models.PROTECT)
    abonement = models.ForeignKey('Sale', on_delete=models.PROTECT)
    visit_date = models.DateField()
    visit_time = models.TimeField(auto_now=True)
    pre_entry_client = models.BooleanField(default=False)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.abonement.decrease_visit()
        super().save()


class ScheduleCanceledGroups(models.Model):
    user = models.ForeignKey('Profile', on_delete=models.PROTECT)
    group = models.ForeignKey('CrmGroup', on_delete=models.CASCADE)
    date = models.DateField()

    def __str__(self):
        return self.group


class PreEntryClients(BaseModel):
    # Предварительная запись
    client = models.ForeignKey('Client', on_delete=models.CASCADE)
    group = models.ForeignKey('CrmGroup', on_delete=models.PROTECT)
    date = models.DateField()   # На какую конкретно дату клиент записывается

    def sale_one_time(self):
        pass

    def sale_abonement(self):
        pass


class SearchCache(BaseModel):
    # кеш запросов поиска.
    user_id = models.PositiveIntegerField(db_index=True)
    surname = models.CharField(max_length=255)
    phone_number = models.CharField(max_length=255)

    def __str__(self):
        return f"{self.user_id} search cache"

    @staticmethod
    def set(user_id, surname, phone_number):
            SearchCache.objects.create(
                user_id=user_id,
                surname=surname,
                phone_number=phone_number
            )

    @staticmethod
    def get(user_id):
        data = SearchCache.objects.filter(user_id=user_id).last()
        if data is None:
            return None
        return dict(surname=data.surname, phone_number=data.phone_number)

    @staticmethod
    def cache_clean(user_id):
        results = SearchCache.objects.filter(user_id=user_id).all()
        for data in results:
            data.delete()