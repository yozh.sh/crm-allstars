class SaleClearRealtionMixin(object):
    """
    Подмешивает метод clear_relation который вызывается в set_relation
    Необходим для того что бы не копипастить один и тот же метод в имплементаторе интерфейса ISaleRelationAdder\
    т.к функционал один и тот же
    """
    def clear_relations(self):
        self.sale.tutor.clear()
        self.sale.group.clear()