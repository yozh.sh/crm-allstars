from crm.strategies.sale.interfaces import ISaleFormDataStrategy, ISaleRelationAdder
from crm.models import Sale, CrmGroup, Tutor, Departament, SaleFormData
from crm.strategies.sale.mixins import SaleClearRealtionMixin


class AllGroupRelationAdder(ISaleRelationAdder, SaleClearRealtionMixin):

    def __init__(self, sale_form_data: SaleFormData, sale: Sale):
        self.form_data = sale_form_data
        self.sale = sale

    def get_data_instance(self) -> SaleFormData:
        return self.form_data

    def set_relation(self):
        self.clear_relations()
        self.sale.saleformdata_set.add(self.form_data)
        self.sale.group.add(*self.form_data.get_all_group_instance())


class AllGroup(ISaleFormDataStrategy):

    def set_data(self, sale: Sale,
                 group1: CrmGroup = None, group2: CrmGroup = None,
                 tutor1: Tutor = None, tutor2: Tutor = None,
                 departament1: Departament = None, departament2: Departament = None) -> ISaleRelationAdder:
        sale_form_data = SaleFormData.objects.create(
            all_groups=True
        )
        return AllGroupRelationAdder(
            sale_form_data=sale_form_data,
            sale=sale
        )
