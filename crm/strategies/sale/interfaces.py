import abc
from crm.models import Sale, CrmGroup, Tutor, Departament, SaleFormData


class ISaleRelationAdder(metaclass=abc.ABCMeta):
    """
    Интерфейс для добавления многие ко многим
    Невозможно создать многие ко многим пока модель на сохранится
    по этому этот интерфейс в зависимости от стратегии добавляет отношения в модель save
    Метод set_relation работает только после sale.save()
    Метод _clear_relation приватный метод который нужен что бы очишать связи перед добавлением связей
    необходим для корректного сохранения связей m2m
    """

    def get_data_instance(self) -> SaleFormData:
        pass

    def set_relation(self):
        pass



class ISaleFormDataStrategy(metaclass=abc.ABCMeta):

    """
        Стратегия сохранения данных формы в зависимости от количества групп(Одна, Две, Все).
        Метод set_data основной метод который вызывается стратегией.
        sale: сюда подается несохраненный обьект sale
    """

    def set_data(self, sale: Sale,
                 group1: CrmGroup = None, group2: CrmGroup = None,
                 tutor1: Tutor = None, tutor2: Tutor = None,
                 departament1: Departament = None, departament2: Departament = None) -> ISaleRelationAdder:
        pass
