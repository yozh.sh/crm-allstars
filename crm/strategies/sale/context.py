from .interfaces import ISaleFormDataStrategy
from crm.models import Sale, CrmGroup, Tutor, Departament, SaleFormData


class SaleContextStrategy(object):
    _strategy = None

    def set_strategy(self, strategy: ISaleFormDataStrategy):
        self._strategy = strategy

    def exec_strategy(self, sale: Sale,
                 group1: CrmGroup = None, group2: CrmGroup = None,
                 tutor1: Tutor = None, tutor2: Tutor = None,
                 departament1: Departament = None, departament2: Departament = None):

        return self._strategy.set_data(
            sale=sale,
            group1=group1,
            group2=group2,
            tutor1=tutor1,
            tutor2=tutor2,
            departament1=departament1,
            departament2=departament2

        )
