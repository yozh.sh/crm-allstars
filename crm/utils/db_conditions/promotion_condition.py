from crm.models import Promotion
from django.db.models import Q
import datetime

def condition():
    q = Promotion.objects.filter(active=True)
    q1 = q.filter(temporary=True)
    to_exclude = q1.filter(Q(end_date__lte=datetime.date.today())).values_list('id', flat=True)
    q = q.exclude(pk__in=to_exclude)
    return q