SYSTEMADMIN_ROLE = 'SYSTEM_ADMIN',
ADMIN_ROLE = 'ADMIN'
USER_ROLE = 'USER'

ROLES = (
    (ADMIN_ROLE, 'Администратор'),
    (USER_ROLE, 'Пользователь')
)