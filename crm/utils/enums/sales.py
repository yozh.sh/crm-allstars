# PAYMENT TYPE
CASH = 'CASH'
CARD = 'CARD'

PAYMENT_TYPE = (
    (CASH, 'Наличные'),
    (CARD, 'Карта')
)

# GROUP COUNT
ONE = 'ONE'
TWO = 'TWO'
ALL = 'ALL'

GROUP_COUNT = (
    (None, '---------'),
    (ONE, 'Одна'),
    (TWO, 'Две'),
    (ALL, 'ВСЕ')
)