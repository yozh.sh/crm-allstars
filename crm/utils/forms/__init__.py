from django import forms

class ChoiceFieldNoValidation(forms.ChoiceField):
    def validate(self, value):
        pass


def make_dynamic_choice_field(queryset, default_field: tuple):
    choice = [(obj.id, str(obj)) for obj in queryset]
    if default_field:
        choice.append(default_field)
    return choice
