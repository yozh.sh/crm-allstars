from datetime import date, datetime, time, timedelta
from django.forms import ValidationError
from django.utils import timezone

# передаешь минуты и передаешь обьект времени datetime.time и получаешь между ним разницу
def time_delta(_time: datetime.time, minutes: int):
    dt = datetime.combine(date.today(), time(_time.hour, _time.minute)) + timedelta(minutes=minutes)
    return dt.time()


# парсинг даты из календаря (bootstrap-datetimepicker)
def parse_date(date_string: str):
    _format = '%Y-%m-%d'
    return datetime.strptime(date_string, _format)


def parse_date_from_input(date_string):
    _format = '%d.%m.%Y'
    try:
        return datetime.strptime(date_string, _format)
    except ValueError:
        raise ValidationError('Неверно введеная дата, проверьте правильность ввода')


def datetime_to_string(dt: datetime):
    _format = '%d.%m.%Y'
    if dt:
        return '{:02d}.{:02d}.{}'.format(dt.day, dt.month, dt.year)
    return None


def datetime_now():
    return timezone.localtime(timezone.now())
