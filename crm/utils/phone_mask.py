def to_format_string(phone_number):
    phone_num_list = list(phone_number).copy()
    try:
        l_bracket = phone_num_list.index('(')
    except ValueError:
        return phone_number
    del phone_num_list[l_bracket]
    r_bracket = phone_num_list.index(')')
    del phone_num_list[r_bracket]

    for x in phone_num_list:
        if x == '-':
            index = phone_num_list.index(x)
            del phone_num_list[index]
    result = "".join(phone_num_list)
    return result


