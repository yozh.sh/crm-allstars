# from django.db.models import SmallIntegerField
# from django.db.models import CharField

DAY_OF_THE_WEEK = (
    (1, u'Понедельник'),
    (2, u'Вторник'),
    (3, u'Среда'),
    (4, u'Четверг'),
    (5, u'Пятница'),
    (6, u'Суббота'),
    (7, u'Воскресенье')
)

# class DayOfTheWeekField(CharField):
#     def __init__(self, *args, **kwargs):
#         kwargs['choices'] = tuple(sorted(DAY_OF_THE_WEEK.items()))
#         kwargs['max_length'] = 1
#         super(DayOfTheWeekField,self).__init__(*args, **kwargs)