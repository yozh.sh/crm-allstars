from django.apps import apps


def get_sale():
    return apps.get_model('crm', 'Sale')
