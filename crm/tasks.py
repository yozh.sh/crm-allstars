# Create your tasks here
from __future__ import absolute_import, unicode_literals
from celery import shared_task
from crm.utils.models.get_crm_models import get_sale




@shared_task
def deactivate_abonement(sale_id: int):
    Sale = get_sale()
    Sale.objects.filter(id=sale_id).update(activation=False)


@shared_task
def activation_abonement(sale_id: int):
    Sale = get_sale()
    Sale.objects.filter(id=sale_id).update(activation=True)


@shared_task
def freeze_abonement(sale_id: int, days: int):
    Sale = get_sale()
    sale = Sale.objects.get(pk=sale_id)
    sale.freeze(days=days)


@shared_task
def unfreeze_abonement(sale_id: int):
    Sale = get_sale()
    sale = Sale.objects.get(pk=sale_id)
    sale.unfreeze()