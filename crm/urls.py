from django.urls import path
from django.conf import settings
from django.conf.urls import include, url

from .views import (abonement, client, group, index, login, logout, promotion,
                    source_of_attraction, tutor, sale)
from .views.calendar.api.preentry_visits import CreatePreEntryVisitView, DeletePreEntryVisitView, GetPreEntryGroupByUser
from .views.client_information import abonement as info_abonement
from .views.client_information import history as info_history
from .views.client_information import installment
from .views.client_information.information import InformationView
from .views.settings import departaments
from crm.views.settings.rbac import users, groups
from crm.views.client_information.installment import (InstallmentPaymentsView,
                                                    InstallmentPaymentsCreate,
                                                    AjaxValidatePaymentMoney,
                                                    AjaxForceClosePayment,
                                                      DeletePaymentAjax)
from crm.views import revoke_abonement
from crm.views.calendar.calendar import CalendarView, CalendarAddVisitsView
from crm.views.calendar.api.tutor import CalendarDayView
from crm.views.calendar.api.client import ClientApiView, GetAbonementsByClient, CheckClientSubscriptionInGroup
from crm.views.calendar.api.schedule import ScheduleByDayView
from crm.views.calendar.api.visits import CreateVisitsView, DestroyVisitsView, GetVisitsByGroupAndDate
from crm.views.calendar.api.preentry_visits import GetPreEntryByGroupAndDate
from crm.views import abonement_freeze
from crm.views import visits
from allstars.swagger import schema_view
# from .views.reports.daily_revenue import DailyRevenueView
from django.contrib.auth.decorators import login_required

##### DELETE ON DEBUG ENVIRONMENT
from  django.views.defaults import page_not_found, permission_denied, server_error
######


urlpatterns = [
    path('', index.IndexView.as_view(), name='index'),
    path('login/', login.LoginView.as_view(), name='login'),
    path('logout/', logout.logout_view, name='logout'),

    path('tutors/list/', tutor.TutorViewList.as_view(), name='tutor_list'),
    path('tutors/create/', tutor.TutorCreateView.as_view(), name='tutor_create'),
    path('tutors/edit/<int:id>', tutor.TutorEditView.as_view(), name='tutor_edit'),
    path('tutors/delete/<int:id>', tutor.delete_tutor, name='tutor_delete'),
    #path('tutors/mass_delete/<int:id>',  name='tutor_mass_delete'),

    # client
    path('clients/list/', client.ClientViewList.as_view(), name='client_list'),
    path('clients/create/', client.ClientCreateView.as_view(), name='client_create'),
    path('clients/edit/<int:id>', client.ClientEditView.as_view(), name='client_edit'),
    path('clients/delete/<int:id>', client.delete_client, name='client_delete'),
    path('clients/search/', client.ClientSearchView.as_view(), name='search_clients'),

    # client information
    path('client/<int:client_id>/information/abonement', info_abonement.AbonementView.as_view(), name='client_info_detail'),
    path('client/<int:client_id>/information/abonement/<int:abon_id>/edit', info_abonement.AbonementEditView.as_view(), name='client_abonement_edit'),
    path('client/<int:client_id>/information/history/', info_history.HistoryView.as_view(), name='client_info_history'),
    path('client/<int:client_id>/information/info', InformationView.as_view(), name="client_info_information"),
    path('client/<int:client_id>/information/installments', installment.InstallmentView.as_view(), name="client_info_installment"),
    path('client/<int:client_id>/information/installment/<int:installment_id>/payments/list', InstallmentPaymentsView.as_view(), name="client_info_installment_payments"),
    path('client/<int:client_id>/information/installment/<int:installment_id>/payments/create', InstallmentPaymentsCreate.as_view(), name="client_info_create_payment_by_installment"),


    # promotion
    path('promo/list/', promotion.PromotionViewList.as_view(), name='promo_list'),
    path('promo/create/', promotion.PromotionCreateView.as_view(), name='promo_create'),
    path('promo/edit/<int:id>', promotion.PromotionEditView.as_view(), name='promo_edit'),
    path('promo/delete/<int:id>', promotion.promotion_delete, name='promo_delete'),

    # source of attraction
    path('sofa/list/', source_of_attraction.SofaViewList.as_view(), name='sofa_list'),
    path('sofa/create/', source_of_attraction.SofaCreateView.as_view(), name='sofa_create'),
    path('sofa/edit/<int:id>', source_of_attraction.SofaEditView.as_view(), name='sofa_edit'),
    path('sofa/delete/<int:id>', source_of_attraction.sofa_delete, name='sofa_delete'),

    # abonements
    path('abon/list/', abonement.AbonViewList.as_view(), name='abon_list'),
    path('abon/create/', abonement.AbonCreateView.as_view(), name='abon_create'),
    path('abon/edit/<int:id>', abonement.AbonEditView.as_view(), name='abon_edit'),
    path('abon/delete/<int:id>', abonement.abon_delete, name='abon_delete'),

    #sales
    path('sale/list/', sale.SaleViewList.as_view(), name='sale_list'),
    path('sale/create/', sale.SaleCreateView.as_view(), name='sale_create'),
    path('sale/edit/<int:id>', sale.SaleEditView.as_view(), name='sale_edit'),
    path('sale/massdelete/', sale.SaleDelete.as_view(), name='sale_mass_delete'),
    path('sale/delete/<int:sale_id>', sale.SaleDelete.as_view(), name='sale_delete'),
    path('sale/one_delete/<int:sale_id>', sale.sale_delete, name='one_sale_delete'),
    path('sale/search/', sale.CashboxSearchView.as_view(), name='search_cashbox'),

    # REVOKE SALE
    path('sale/<int:abon_id>/revoke', revoke_abonement.RevokeAbonementAjaxView.as_view(), name='revoke_sale_abon'),

    # FREEZE SALE
    path('sale/freeze', abonement_freeze.AbonementFreeze.as_view(), name='freeze_sale'),

    # groups
    path('groups/list/', group.GroupViewList.as_view(), name='group_list'),
    path('groups/create/', group.GroupCreateView.as_view(), name='group_create'),
    path('groups/edit/<int:id>', group.GroupEditView.as_view(), name='group_edit'),
    path('groups/delete/<int:id>', group.delete_group, name='group_delete'),
    #path('groups/mass_delete/<int:id>',  name='tutor_mass_delete'),


    # SETTINGS
    ## RBAC
    ### USER
    path('settings/rbac/users/list/', users.UserViewList.as_view(), name='user_setting_list'),
    path('settings/rbac/users/create/', users.UserCreateView.as_view(), name='user_setting_create'),
    path('settings/rbac/users/edit/<int:id>', users.UserEditView.as_view(), name='user_setting_edit'),
    path('settings/rbac/users/delete/<int:id>', users.delete_user, name='user_setting_delete'),
    path('settings/rbac/users/mass_delete/', users.mass_user_delete, name='mass_delete_user'),

    ### GROUPS
    path('settings/rbac/groups/list/', groups.GroupView.as_view(), name='group_settings_list'),
    path('settings/rbac/groups/create/', groups.GroupCreateView.as_view(), name='group_settings_create'),




    path('settings/departaments/list/', departaments.DepartamentViewList.as_view(), name='departament_setting_list'),
    path('settings/departaments/create/', departaments.DepartamentCreateView.as_view(), name='departament_setting_create'),
    path('settings/departaments/edit/<int:id>', departaments.DepartamentEditView.as_view(), name='departament_setting_edit'),
    path('settings/departaments/delete/<int:id>', departaments.delete_departament, name='departament_setting_delete'),
    #path('settings/departaments/mass_delete/<int:id>')

    # Installments

    # path('installments/list',  installments.InstallmentViewList.as_view(), name='installment_list'),
    # path('installments/create',  installments.InstallmentCreateView.as_view(), name='installment_create'),
    #
    # path('installments/payments/list', payments.PaymentViewList.as_view(), name='payments_by_installment_list'),
    # path('installments/<int:id>/payments/create', payments.PaymentCreateView.as_view(), name='payments_by_installment_create'),
    


    # AJAX DATA


    # SALES
    path('ajax/sales/last_abon/<int:client_id>', sale.AjaxGetLastSoldAbon.as_view(), name='ajax_last_abon'),
    path('ajax/sales/last_promotion/<int:client_id>', sale.AjaxGetLastSoldPromotion.as_view(), name='ajax_last_promo'),
    path('ajax/sales/groups_by_tutor/', sale.AjaxGroupsByTutorView.as_view(), name='ajax_groups_by_tutor'),
    path('ajax/sales/get_abonement', abonement.AjaxAbonementByGroupView.as_view(), name='ajax_get_abonement'),
    path('ajax/sales/get_one_time_abonement', abonement.AjaxOneTimeVisitAbonementByGroupView.as_view(), name='ajax_get_one_time_abonement'),
    path('ajax/sales/get_price/<int:abon_id>', abonement.AjaxGetAbonementPrice.as_view(), name='ajax_get_full_price'),
    path('ajax/sales/get_discount/', sale.AjaxGetDiscount.as_view(), name='ajax_get_discount'),
    path('ajax/sales/today/', index.AjaxSalesToday.as_view(), name='ajax_get_sale_today'),
    path('ajax/sales/yesterday/', index.AjaxSaleYersterday.as_view(), name='ajax_get_sale_yesterday'),
    path('ajax/sales/month/', index.AjaxSaleMonth.as_view(), name='ajax_get_sale_month'),
    path('ajax/client/new/today/', client.AjaxGetNewClients.as_view(), name='ajax_get_new_client_today'),
    path('ajax/sales/get_abonement_id_by_sale_id/', sale.AjaxGetAbonementIdBySale.as_view(), name='ajax_get_abon_id_by_sale_id'),
    path('ajax/sales/delete_revoke/', sale.AjaxDeleteRevoke.as_view(), name='ajax_delete_revoke'),
    path('ajax/sales/get_groups/', sale.AjaxGetGroupsBySale.as_view(), name='ajax_groups_by_sale'),

    # USER CHANGE PASSWORD
    path('ajax/user/change_password/', users.AjaxUpdateUserPassword.as_view(), name='ajax_update_user_password'),

    # CLIENT INFORAMTION
    path('ajax/client/information/abonement/get_deactivation_date/', info_abonement.AjaxGetDeactivationDate.as_view(), name='ajax_get_deactivation_date'),  # ?sale_id ?activation_date
    path('ajax/client/information/installment/<int:installment_id>/validate_payment_money/', AjaxValidatePaymentMoney.as_view(), name='ajax_vaidate_payments'),
    path('ajax/client/information/installment/<int:installment_id>/force_close_installment', AjaxForceClosePayment.as_view(), name="force_close_installment"),
    path('ajax/client/information/installment/payment_delete/', DeletePaymentAjax.as_view(), name="payment_delete"),

    # INSTALLMENTS

    # path('ajax/installments/get_sales_by_client/<int:client_id>', installments.SaleByClient.as_view(), name='ajax_get_sales_by_client'),

    # REPORTS
    # path('daily_revenue_reports/', DailyRevenueView.as_view(), name='dayli_revenue_report'),


    # CALENDAR
    path('calendar/', CalendarView.as_view(), name="calendar_view"),
    path('calendar/add_visits/group/<int:group_id>/day/<int:day_number>', CalendarAddVisitsView.as_view(), name="calendar_addvisits"),
    # #### CALENDAR API
    path('calendar/api/current_day/', CalendarDayView.as_view(), name="c_data_api"),
    path('calendar/api/visits/', visits.GetVisitsToday.as_view(), name="c_visit_cur_day"),
    path('calendar/api/visits/client/<int:client_id>/today/', visits.GetVisitsClientToday.as_view(), name="c_visit_client_today"),
    path('calendar/api/visits/group/<int:group_id>/today/', visits.GetGroupVisitsToday.as_view(), name="c_visit_group_today"),
    path('calendar/api/clients/', ClientApiView.as_view({'get': 'list'}), name="c_clients_all"),
    path('calendar/api/client/<int:pk>/', ClientApiView.as_view({'get': 'retrieve'}), name="c_clients_single"),
    path('calendar/api/schedule/', ScheduleByDayView.as_view(), name="c_get_schedule_by_date"),
    path('calendar/api/client/visits/', CreateVisitsView.as_view(), name="c_client_visits_create"),
    path('calendar/api/client/visit/<int:id>/', DestroyVisitsView.as_view(), name="c_client_visits_delete"),
    path('calendar/api/visits/group/<int:id>/', GetVisitsByGroupAndDate.as_view(), name="c_get_client_visits_by_group_date"),
    path('calendar/api/preentry/group/<int:id>/', GetPreEntryByGroupAndDate.as_view(), name="c_get_pre_entry_client_visits_by_group_date"),
    path('calendar/api/preentry/', CreatePreEntryVisitView.as_view(), name="c_create_preentry_visit"),
    path('calendar/api/preentry/<int:id>/', DeletePreEntryVisitView.as_view(), name="c_delete_preentry_visit"),
    path('calendar/api/client/<int:id>/abonements/', GetAbonementsByClient.as_view(), name="c_active_abones_by_client_id"),
    path('calendar/api/client_check_subscription/<int:client_id>/<int:group_id>/',
         CheckClientSubscriptionInGroup.as_view(), name="c_check_client_in_grp"),
    path('calendar/api/preentry_visits/<int:pk>/', GetPreEntryGroupByUser.as_view()),

    # RBAC

    ##### DEBUG 500, 404, 403 PAGES


    # path('404/', page_not_found, kwargs={'exception': Exception('Not Found')}),
    # path('403/', permission_denied, kwargs={'exception': Exception('Permission Denied')}),
     path('500/', server_error),
    ######

    # DJANGO SELECT2
    url(r'^select2/', include('django_select2.urls')),

    # SWAGGER
    url(r'^swagger/$', login_required(schema_view), name="swagger_ui")

]


if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
