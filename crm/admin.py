from django.contrib import admin
from crm.models import (
    Departament, Profile,
    Installment, InstallmentPayments, PromotionToSale, CashBox, VisitsToGroup, SaleFormData
)
from simple_history.admin import SimpleHistoryAdmin

# LOGS
from crm.models import Tutor, CrmGroup, Client, Abonement, Promotion, SourceOfAttraction, Sale, PreEntryClients

# Register your models here.
# LOGS ADMIN
admin.site.register(Tutor, SimpleHistoryAdmin)
admin.site.register(CrmGroup, SimpleHistoryAdmin)
admin.site.register(Client, SimpleHistoryAdmin)
admin.site.register(Abonement, SimpleHistoryAdmin)
admin.site.register(Promotion, SimpleHistoryAdmin)
admin.site.register(SourceOfAttraction, SimpleHistoryAdmin)
admin.site.register(Sale, SimpleHistoryAdmin)

admin.site.register(Departament)
admin.site.register(Profile)
admin.site.register(Installment)
admin.site.register(InstallmentPayments)
admin.site.register(PromotionToSale)
admin.site.register(CashBox)
admin.site.register(VisitsToGroup)
admin.site.register(SaleFormData)
admin.site.register(PreEntryClients)