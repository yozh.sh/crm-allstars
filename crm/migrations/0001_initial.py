# Generated by Django 2.1 on 2020-03-22 16:12

import datetime
from django.conf import settings
import django.contrib.postgres.fields.jsonb
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import simple_history.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Abonement',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(db_index=True, max_length=255)),
                ('available_visits_number', models.IntegerField()),
                ('duration', models.IntegerField()),
                ('group_count', models.CharField(choices=[('ONE', 'Одна'), ('TWO', 'Две'), ('ALL', 'ВСЕ')], default='ONE', max_length=255)),
                ('number_of_freezing', models.IntegerField(default=0)),
                ('cost', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='CashBox',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('create_date', models.DateTimeField()),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('client_link', models.CharField(max_length=255)),
                ('activation', models.BooleanField(default=True)),
                ('activation_date', models.DateField(blank=True, null=True)),
                ('is_promotion', models.BooleanField(default=False)),
                ('is_installment', models.BooleanField(default=False)),
                ('payment_type', models.CharField(blank=True, max_length=255, null=True)),
                ('price', models.PositiveIntegerField()),
                ('operation_type', models.CharField(choices=[('SALE', 'Продажа'), ('INSTALLMENT', 'Рассрочка'), ('REVOKE', 'Возврат')], default='SALE', max_length=100)),
                ('archived', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Client',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('surname', models.CharField(db_index=True, max_length=255)),
                ('name', models.CharField(db_index=True, max_length=255)),
                ('middle_name', models.CharField(blank=True, db_index=True, max_length=255, null=True)),
                ('sex', models.IntegerField(choices=[(1, 'Мужской'), (0, 'Женский')], default=None)),
                ('birthday', models.DateField()),
                ('phone_number', models.CharField(blank=True, db_index=True, max_length=255, null=True)),
                ('parent_FIO', models.CharField(blank=True, max_length=255, null=True)),
                ('parent_phone_number', models.CharField(blank=True, max_length=255, null=True)),
                ('email', models.EmailField(blank=True, max_length=254, null=True)),
                ('notes', models.TextField(blank=True, null=True)),
                ('created', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated', models.DateTimeField(auto_now=True, null=True)),
                ('blocked_by_installment', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='CrmGroup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(db_index=True, max_length=255, unique=True)),
                ('rate', models.IntegerField()),
                ('is_podushka', models.BooleanField(default=False)),
                ('podushka_cost', models.IntegerField(default=0)),
                ('training_days', django.contrib.postgres.fields.jsonb.JSONField(default=dict)),
                ('training_duration', models.IntegerField(choices=[(30, 'Пол часа'), (60, 'Один час'), (90, 'Полтора часа'), (120, 'Два часа'), (180, 'Три часа')], default=60)),
                ('one_visit_price', models.IntegerField(default=150)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Departament',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=255, unique=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='HistoricalSale',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('abonement_name', models.CharField(db_index=True, max_length=255)),
                ('abonement_available_visits_number', models.IntegerField()),
                ('start_visits_number', models.IntegerField()),
                ('current_visits_number', models.PositiveIntegerField()),
                ('abonement_duration', models.IntegerField()),
                ('abonement_group_count', models.CharField(choices=[('ONE', 'Одна'), ('TWO', 'Две'), ('ALL', 'ВСЕ')], default='ONE', max_length=255)),
                ('abonement_number_of_freezing', models.IntegerField(default=0)),
                ('activation', models.BooleanField(db_index=True, default=False)),
                ('activation_date', models.DateField(blank=True, default=datetime.datetime.now, null=True)),
                ('deactivation_date', models.DateField(blank=True, null=True)),
                ('created', models.DateTimeField(blank=True, editable=False, null=True)),
                ('updated', models.DateTimeField(blank=True, editable=False, null=True)),
                ('installment_plan', models.BooleanField(default=False)),
                ('saled_by_installment', models.BooleanField(db_index=True, default=False)),
                ('payment_type', models.CharField(choices=[('CASH', 'Наличные'), ('CARD', 'Карта')], default='CASH', max_length=120)),
                ('end_payment', models.PositiveIntegerField(default=0)),
                ('abonement_price', models.PositiveIntegerField(default=0)),
                ('revoke', models.BooleanField(db_index=True, default=False)),
                ('revoked_money', models.IntegerField(blank=True, null=True)),
                ('archived', models.BooleanField(default=False)),
                ('celery_task_id', models.CharField(blank=True, max_length=36, null=True)),
                ('freezed', models.BooleanField(default=False)),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
                ('abonement', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='crm.Abonement')),
                ('client', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='crm.Client')),
                ('history_user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'historical sale',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
        migrations.CreateModel(
            name='HistoricalTutor',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('created_date', models.DateTimeField(blank=True, editable=False)),
                ('modified_date', models.DateTimeField(blank=True, editable=False)),
                ('first_name', models.CharField(max_length=255)),
                ('last_name', models.CharField(max_length=255)),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
                ('history_user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'historical tutor',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
        migrations.CreateModel(
            name='Installment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, null=True)),
                ('debt', models.PositiveIntegerField(default=0)),
                ('active', models.BooleanField(default=True)),
                ('paid_out', models.BooleanField(default=False)),
                ('client', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='crm.Client')),
            ],
        ),
        migrations.CreateModel(
            name='InstallmentPayments',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('payment', models.PositiveIntegerField()),
                ('created', models.DateTimeField(auto_now_add=True, null=True)),
                ('create_date', models.DateField(blank=True, default=django.utils.timezone.now, null=True)),
                ('first_payment', models.BooleanField(default=False)),
                ('client', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='crm.Client')),
                ('installment', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='crm.Installment')),
            ],
        ),
        migrations.CreateModel(
            name='PreEntryClients',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('date', models.DateField()),
                ('client', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='crm.Client')),
                ('group', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='crm.CrmGroup')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('departament', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='crm.Departament')),
                ('user', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Promotion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('type_of_discount', models.CharField(choices=[('PROCENT', 'Проценты'), ('MONEY', 'Гривны')], default='MONEY', max_length=120)),
                ('discount', models.IntegerField()),
                ('temporary', models.BooleanField(default=False)),
                ('start_date', models.DateField(blank=True, null=True)),
                ('end_date', models.DateField(blank=True, null=True)),
                ('active', models.BooleanField(default=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='PromotionToSale',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('type_of_discount', models.CharField(choices=[('PROCENT', 'Проценты'), ('MONEY', 'Гривны')], default='MONEY', max_length=120)),
                ('discount', models.IntegerField()),
                ('temporary', models.BooleanField(default=False)),
                ('start_date', models.DateField(blank=True, null=True)),
                ('end_date', models.DateField(blank=True, null=True)),
                ('active', models.BooleanField(default=True)),
                ('template_promotion', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='crm.Promotion')),
            ],
        ),
        migrations.CreateModel(
            name='Sale',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('abonement_name', models.CharField(db_index=True, max_length=255)),
                ('abonement_available_visits_number', models.IntegerField()),
                ('start_visits_number', models.IntegerField()),
                ('current_visits_number', models.PositiveIntegerField()),
                ('abonement_duration', models.IntegerField()),
                ('abonement_group_count', models.CharField(choices=[('ONE', 'Одна'), ('TWO', 'Две'), ('ALL', 'ВСЕ')], default='ONE', max_length=255)),
                ('abonement_number_of_freezing', models.IntegerField(default=0)),
                ('activation', models.BooleanField(db_index=True, default=False)),
                ('activation_date', models.DateField(blank=True, default=datetime.datetime.now, null=True)),
                ('deactivation_date', models.DateField(blank=True, null=True)),
                ('created', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated', models.DateTimeField(auto_now=True, null=True)),
                ('installment_plan', models.BooleanField(default=False)),
                ('saled_by_installment', models.BooleanField(db_index=True, default=False)),
                ('payment_type', models.CharField(choices=[('CASH', 'Наличные'), ('CARD', 'Карта')], default='CASH', max_length=120)),
                ('end_payment', models.PositiveIntegerField(default=0)),
                ('abonement_price', models.PositiveIntegerField(default=0)),
                ('revoke', models.BooleanField(db_index=True, default=False)),
                ('revoked_money', models.IntegerField(blank=True, null=True)),
                ('archived', models.BooleanField(default=False)),
                ('celery_task_id', models.CharField(blank=True, max_length=36, null=True)),
                ('freezed', models.BooleanField(default=False)),
                ('abonement', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='crm.Abonement')),
                ('client', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='crm.Client')),
                ('group', models.ManyToManyField(to='crm.CrmGroup')),
                ('promotion', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='crm.PromotionToSale')),
                ('saled_by', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='crm.Profile')),
                ('saled_by_departament', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='crm.Departament')),
            ],
        ),
        migrations.CreateModel(
            name='SaleFormData',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('order_departament1', models.PositiveIntegerField(blank=True, db_index=True, null=True)),
                ('order_departament2', models.PositiveIntegerField(blank=True, db_index=True, null=True)),
                ('order_tutor1', models.PositiveIntegerField(blank=True, db_index=True, null=True)),
                ('order_tutor2', models.PositiveIntegerField(blank=True, db_index=True, null=True)),
                ('order_group1', models.PositiveIntegerField(blank=True, db_index=True, null=True)),
                ('order_group2', models.PositiveIntegerField(blank=True, db_index=True, null=True)),
                ('all_groups', models.BooleanField(default=False)),
                ('sale', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='crm.Sale')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ScheduleCanceledGroups',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField()),
                ('group', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='crm.CrmGroup')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='crm.Profile')),
            ],
        ),
        migrations.CreateModel(
            name='SourceOfAttraction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Tutor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('first_name', models.CharField(max_length=255)),
                ('last_name', models.CharField(max_length=255)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='VisitsToGroup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('visit_date', models.DateField()),
                ('visit_time', models.TimeField(auto_now=True)),
                ('pre_entry_client', models.BooleanField(default=False)),
                ('abonement', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='crm.Sale')),
                ('client', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='crm.Client')),
                ('group', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='crm.CrmGroup')),
            ],
        ),
        migrations.AddField(
            model_name='sale',
            name='tutor',
            field=models.ManyToManyField(to='crm.Tutor'),
        ),
        migrations.AddField(
            model_name='installment',
            name='sale',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='crm.Sale'),
        ),
        migrations.AddField(
            model_name='historicalsale',
            name='promotion',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='crm.PromotionToSale'),
        ),
        migrations.AddField(
            model_name='historicalsale',
            name='saled_by',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='crm.Profile'),
        ),
        migrations.AddField(
            model_name='historicalsale',
            name='saled_by_departament',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='crm.Departament'),
        ),
        migrations.AddField(
            model_name='crmgroup',
            name='departament',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='crm.Departament'),
        ),
        migrations.AddField(
            model_name='crmgroup',
            name='tutor',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='crm.Tutor'),
        ),
        migrations.AddField(
            model_name='client',
            name='source_of_attraction',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='crm.SourceOfAttraction'),
        ),
        migrations.AddField(
            model_name='cashbox',
            name='abonement',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='crm.Sale'),
        ),
        migrations.AddField(
            model_name='cashbox',
            name='client',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='crm.Client'),
        ),
        migrations.AddField(
            model_name='cashbox',
            name='group',
            field=models.ManyToManyField(to='crm.CrmGroup'),
        ),
        migrations.AddField(
            model_name='cashbox',
            name='installment',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='crm.Installment'),
        ),
        migrations.AddField(
            model_name='cashbox',
            name='payment_installment',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='crm.InstallmentPayments'),
        ),
        migrations.AddField(
            model_name='cashbox',
            name='promotion',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='crm.PromotionToSale'),
        ),
    ]
