from django.db import migrations
from django.contrib.auth.models import User     # where User lives
from django.contrib.auth.hashers import make_password
import uuid
import os                                      # env var access


def create_django_system_user(apps, schema_editor):
    User.objects.create_superuser(
        username=os.getenv('DJANGO_ADMIN_SUPERUSER'),
        password=os.getenv('DJANGO_ADMIN_SUPERUSER_PASSWORD'),
        email='rootuser@admin.com'
    )


def reverse_create_django_system_user(apps, schema_editor):
    db_alias = schema_editor.connection.alias
    User = apps.get_model("auth", "User")
    User.objects.using(db_alias).get(username=os.getenv('DJANGO_ADMIN_SUPERUSER')).delete()


def create_crm_user(apps, schema_editor):
    # this user is have admin permission
    # crm app have 2 permission roles - admin, user
    db_alias = schema_editor.connection.alias
    User = apps.get_model("auth", "User")
    Deparament = apps.get_model("crm", "Departament")
    Profile = apps.get_model("crm", "Profile")
    user = User.objects.create(
        username=uuid.uuid4().hex,
        email=os.getenv('CRM_ADMIN_USER_EMAIL'),
        first_name='Hussam',
        last_name='Bessan',
        password=make_password(os.getenv('CRM_ADMIN_USER_PASSWORD'))
    )
    departament = Deparament.objects.get(type='ALL', service=True)
    # create user profile and add user to deprtament
    Profile.objects.create(
        user=user,
        departament=departament
    )
    # add user to permission group
    Group = apps.get_model("auth", "Group")
    admin_group = Group.objects.using(db_alias).get(name='admin')
    admin_group.user_set.add(user)




def reverse_create_crm_user(apps, schema_editor):
    db_alias = schema_editor.connection.alias
    User = apps.get_model("auth", "User")
    user = User.objects.using(db_alias).filter(email=os.getenv('CRM_ADMIN_USER_EMAIL')).first()
    if user:
        user.delete()


class Migration(migrations.Migration):

    dependencies = [
        ('crm', 'set_initial_departament_data'),
    ]

    operations = [
        migrations.RunPython(create_django_system_user, reverse_create_django_system_user),
        migrations.RunPython(create_crm_user, reverse_create_crm_user)
    ]

