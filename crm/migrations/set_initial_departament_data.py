from django.db import migrations


def create_departament_all(apps, schema_editor):
    Departament = apps.get_model('crm', 'Departament')
    # CHECK IF EXIST
    if Departament.objects.filter(service=True, type='ALL').exists():
        pass
    else:
        Departament.objects.create(
            name="Все",
            service=True,
            type="ALL"
        )

def reverse_create_departament_all(apps, schema_editor):
    Departament = apps.get_model('crm', 'Departament')
    query = Departament.objects.filter(service=True, type='ALL')
    if query.exists():
        query.first().delete()

class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0002_auto_20200322_2032'),
    ]

    operations = [
        migrations.RunPython(create_departament_all, reverse_create_departament_all),
    ]
