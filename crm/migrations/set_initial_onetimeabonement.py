from django.db import migrations

def create_one_time_abonement(apps, schema_editor):
    abon_name = "Разовый Абонемент"
    Abonement = apps.get_model('crm', 'Abonement')

    if not Abonement.objects.filter(
        name=abon_name, available_visits_number=1, 
        duration=1, cost=0):

        Abonement.objects.create(
            name=abon_name,
            available_visits_number=1,
            duration=1,
            group_count='ALL',
            cost=0
        )


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0001_initial')
    ]
    operations = [
        migrations.RunPython(create_one_time_abonement)
    ]
    
