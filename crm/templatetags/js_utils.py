from django import template

register = template.Library()


@register.inclusion_tag('utils/checkbox_to_dropdown.html', takes_context=True)
def dropdown_by_check(context):
    return context

@register.inclusion_tag('utils/input_mask.html', takes_context=True)
def input_mask(context):
    return context

@register.inclusion_tag('utils/input_mask_input.html', takes_context=True)
def input_mask_input(context):
    return context



