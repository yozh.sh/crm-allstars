from django import template

register = template.Library()


@register.inclusion_tag('utils/pagination.html', takes_context=True)
def paginator(context):
    return context