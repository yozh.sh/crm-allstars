from django import template
from rolepermissions.roles import get_user_roles
from django.shortcuts import reverse
import datetime

register = template.Library()


def days_sequence(value):
    days = []
    for day in value:
        if day['day'] == 1 and day['end_time'] != None:
            days.append('Понедельник')
        elif day['day'] == 2 and day['end_time'] != None:
            days.append('Вторник')
        elif day['day'] == 3 and day['end_time'] != None:
            days.append('Среда')
        elif day['day'] == 4 and day['end_time'] != None:
            days.append('Четверг')
        elif day['day'] == 5 and day['end_time'] != None:
            days.append('Пятница')
        elif day['day'] == 6 and day['end_time'] != None:
            days.append('Суббота')
        elif day['day'] == 7 and day['end_time'] != None:
            days.append('Воскресение')
    return ', '.join(days)


def training_duration(value):
    data = []
    for json in value:
        if json['end_time'] != None:
            time = datetime.time.fromisoformat(json['begin_time'])
            data.append(time.strftime("%H:%M"))
    return ', '.join(data)


def sex(value):
    if value == 1:
        return "Мужской"
    else:
        return "Женский"


def get_user_role(user):
    return get_user_roles(user)


def is_has_role(user, roles):
    _roles = roles.split(',')
    available_roles = get_user_roles(user)
    for role in _roles:
        for r in available_roles:
            if role == r.get_name():
                return True
            else:
                return False

def rev(value):
    return reverse(value)


def cashbox_event_display(value):
    if value == 'SALE':
        return 'Продажа'
    elif value == 'INSTALLMENT':
        return 'Рассрочка'
    elif value == 'REVOKE':
        return 'Возврат'
    else:
        return None


register.filter('days_sequence', days_sequence)
register.filter('sex', sex)
register.filter('get_role', get_user_role)
register.filter('is_has_role', is_has_role)
register.filter('training_duration', training_duration)
register.filter('reverso', rev)
register.filter('cashbox_event_display', cashbox_event_display)
