from django import forms
from crm.models import Sale, Client, Tutor, Departament, Abonement, CrmGroup, \
    Promotion, PromotionToSale
from crm.utils.dt import parse_date_from_input
from crm.utils.db_conditions.promotion_condition import condition

tutor_qs = Tutor.objects.all



class SaleModelForm(forms.ModelForm):
    client = forms.ModelChoiceField(queryset=Client.objects.filter(blocked_by_installment=False).all(), label='Клиент', required=True)
    tutor1 = forms.ModelChoiceField(queryset=tutor_qs(), label='Педагог', required=False)
    group1 = forms.CharField(widget=forms.Select(), label='Группа*', required=False)
    departament1 = forms.ModelChoiceField(queryset=Departament.objects.all(), required=False, label='Филиал')

    tutor2 = forms.ModelChoiceField(queryset=tutor_qs(), label='Педагог', required=False)
    group2 = forms.CharField(widget=forms.Select(), label='Группа*', required=False)
    departament2 = forms.ModelChoiceField(queryset=Departament.objects.all(), required=False, label='Филиал')

    last_sale_abon = forms.CharField(widget=forms.TextInput(attrs={'readonly':'readonly'}), label='Последний проданный абонемент', required=False)
    last_sale_promo = forms.CharField(widget=forms.TextInput(attrs={'readonly':'readonly'}), label='Последняя проданная акция', required=False)

    end_payment = forms.IntegerField(widget=forms.TextInput(attrs={'readonly':'readonly', 'id': 'id_price'}), label='Итого к оплате')


    activation_date = forms.CharField(widget=forms.DateInput(format='%d.%m.%Y', attrs={
        "data-mask": "00.00.0000",
        "placeholder": "день.месяц.год"
    }), label="Дата активации", required=False)

    first_sum = forms.CharField(widget=forms.NumberInput(), required=False, label='Сумма первого платежа*')
    sale_id = forms.CharField(widget=forms.HiddenInput(), required=False)  # КОСТЫЛЬ!!!
    saled_by_departament = forms.ModelChoiceField(queryset=Departament.objects.all(), required=False)

    # deactivation_date = forms.CharField(widget=forms.DateInput(format='%d.%m.%Y', attrs={
    #     "data-mask": "00.00.0000",
    #     "placeholder": "день.месяц.год"
    # }), label="Дата деактивации")

    abonement = forms.CharField(widget=forms.Select(), label='Абонемент')

    promotion = forms.ModelChoiceField(queryset=condition(), label='Акция', required=False)

    # def __init__(self, *args, **kwargs):
    #     super(SaleModelForm, self).__init__(*args, **kwargs)
    #
    #     self.fields['promotion'] = forms.ModelChoiceField(queryset=Promotion.objects.filter(id__in=condition()), label='Акция', required=False)

    def clean_saled_by_departament(self):
        if self.cleaned_data['saled_by_departament'] is None:
            raise forms.ValidationError('У вас не установлен филиал, пожалуйста обратитесь к администратору')
        return self.cleaned_data['saled_by_departament']

    def clean_abonement(self):
        abonement_id = int(self.cleaned_data['abonement'])
        return Abonement.objects.get(pk=abonement_id)

    def clean_client(self):
        if self.cleaned_data['client'] == None:
            raise forms.ValidationError('Необходимо добавить клиента')
        else:
            return self.cleaned_data['client']

    def clean_group1(self):
        if self.cleaned_data['group1'] == '':
            return None
        return CrmGroup.objects.get(pk=int(self.cleaned_data['group1']))

    def clean_group2(self):
        if self.cleaned_data['group2'] == '':
            return None
        return CrmGroup.objects.get(pk=int(self.cleaned_data['group2']))

    def clean_activation_date(self):
        date = self.cleaned_data['activation_date']
        if date == '':
            return None
        return parse_date_from_input(date)

    def clean_tutor1(self):
        if self.cleaned_data['abonement_group_count'] == 'ONE' or self.cleaned_data['abonement_group_count'] == 'TWO':
            if self.cleaned_data['tutor1'] == None:
                raise forms.ValidationError('Необходимо добавить преподователя')
            else:
                return self.cleaned_data['tutor1']

    def clean_tutor2(self):
        if self.cleaned_data['abonement_group_count'] == 'TWO':
            if self.cleaned_data['tutor2'] == None:
                raise forms.ValidationError('Необходимо добавить преподователя')
            else:
                return self.cleaned_data['tutor2']

    def clean_promotion(self):
        if self.cleaned_data['promotion'] != None:
            promotion = self.cleaned_data['promotion']
            copied_promo = PromotionToSale(
                name=promotion.name,
                type_of_discount=promotion.type_of_discount,
                discount=promotion.discount,
                temporary=promotion.temporary,
                start_date=promotion.start_date,
                end_date=promotion.end_date,
                active=promotion.active,
                template_promotion=promotion
            )
            self.cleaned_data['promotion'] = copied_promo
            return self.cleaned_data['promotion']
        return self.cleaned_data['promotion']


    class Meta:
        model = Sale
        fields = (
            'client',
            'last_sale_abon',
            'last_sale_promo',
            'abonement_group_count',
            'tutor1',
            'departament1',
            'group1',
            'tutor2',
            'departament2',
            'group2',
            'abonement',
            'activation',
            'activation_date',
            'promotion',
            'installment_plan',
            'first_sum',
            'payment_type',
            'end_payment',

        )
        exclude = (
            'created',
            'updated',
            'tutor',
            'group',
            'deactivation_date',
            'abonement_price',
            'order_departament1',
            'order_departament2',
            'order_group1',
            'order_group2',
            'order_tutor1',
            'order_tutor2',
        )
        labels = {
            'client': 'Клиент',
            'abonement_group_count': 'Количество групп',
            'abonement': 'Абонемент',
            'activation': 'Активация',
            'activation_date': 'Дата активации',
            'deactivation_date': 'Дата деактивации',
            'promotion': 'Акция',
            'installment_plan': 'Рассрочка',
            'payment_type': 'Тип оплаты',


        }


class SaleEditModelForm(SaleModelForm):
    client = forms.ModelChoiceField(
        queryset=Client.objects.all(),
        label='Клиент', required=True)


class SaleOneTimeVisitForm(SaleModelForm):
    client = forms.ModelChoiceField(queryset=Client.objects.filter(blocked_by_installment=False).all(), label='Клиент')
    abonement = forms.CharField(widget=forms.Select(attrs={
        'disabled': 'disabled'
    }), label='Абонемент')