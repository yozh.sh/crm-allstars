from django import forms
from django.forms import fields


class CashboxSearchForm(forms.Form):
    surname = fields.CharField(label="", required=False, widget=fields.TextInput(
        attrs={
            "placeholder": "Фамилия"
        }
    ))
    cost = fields.CharField(label="", required=False, widget=forms.NumberInput(
        attrs={
            "placeholder": "Стоимость продажи"
        }
    ))
