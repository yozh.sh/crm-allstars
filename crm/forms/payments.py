from django import forms
from crm.models import PaymentsByInstallments

class PaymentModelForm(forms.ModelForm):
    class Meta:
        model = PaymentsByInstallments
        fields = ('cash',)
        readonly_fields = ('date_of_entry', )