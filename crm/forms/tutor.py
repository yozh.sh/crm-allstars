from django import forms
from crm.models import Tutor, CrmGroup
from django_select2.forms import Select2MultipleWidget
# Select2MultipleWidget(attrs={"style": "width: 100%"}
#                                      )
class TutorModelForm(forms.ModelForm):
    class Meta:
        model = Tutor
        fields = "__all__"
        labels = {
            'first_name': 'Имя',
            'last_name': 'Фамилия'
        }