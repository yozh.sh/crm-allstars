from django import forms
from crm.models import CrmGroup, Departament, Tutor
from crm.utils.models.dayofweek import DAY_OF_THE_WEEK


class CrmGroupModelForm(forms.ModelForm):
    departament = forms.ModelChoiceField(queryset=Departament.objects.all(), label="Филиал")
    tutor = forms.ModelChoiceField(queryset=Tutor.objects.all(), label="Педагог")
    # training_days = forms.MultipleChoiceField(choices=DAY_OF_THE_WEEK)
    podushka_cost = forms.IntegerField(widget=forms.NumberInput(attrs={
        'id': 'id_podushka_cost',
        # 'class': 'numberinput form-control'
    }), required=False, label="Цена подушки*")

    class Meta:
        model = CrmGroup
        fields = ('name', 'rate', 'is_podushka', 'podushka_cost', 'departament', 'one_visit_price', 'training_duration', 'tutor')
        labels = {
            'name': 'Имя',
            'rate': 'Ставка',
            'is_podushka': 'Подушка',
            'one_visit_price': 'Цена за одно посещение',
            'training_duration': 'Длительность занятия',
            'tutor': 'Педагог'
        }


class MondayDayForm(forms.Form):
    monday_day = forms.BooleanField(label="Понедельник", required=False)
    monday_time = forms.TimeField(widget=forms.TextInput(attrs={'id': 'time_monday'}), required=False, label='')


class TuesdayDayForm(forms.Form):
    tuesday_day = forms.BooleanField(label="Вторник", required=False)
    tuesday_time = forms.TimeField(widget=forms.TextInput(attrs={'id': 'time_tuesday'}), required=False, label='')


class WednesdayDayForm(forms.Form):
    wednesday_day = forms.BooleanField(label="Среда", required=False)
    wednesday_time = forms.TimeField(widget=forms.TextInput(attrs={'id': 'time_wednesday'}), required=False, label='')


class ThursdayDayForm(forms.Form):
    thursday_day = forms.BooleanField(label="Четверг", required=False)
    thursday_time = forms.TimeField(widget=forms.TextInput(attrs={'id': 'time_thursday'}), required=False, label='')


class FridayDayForm(forms.Form):
    friday_day = forms.BooleanField(label="Пятница", required=False)
    friday_time = forms.TimeField(widget=forms.TextInput(attrs={'id': 'time_friday'}), required=False, label='')


class SaturdayDayForm(forms.Form):
    saturday_day = forms.BooleanField(label="Суббота", required=False)
    saturday_time = forms.TimeField(widget=forms.TextInput(attrs={'id': 'time_saturday'}), required=False, label='')


class SundayDayForm(forms.Form):
    sunday_day = forms.BooleanField(label="Воскресение", required=False)
    sunday_time = forms.TimeField(widget=forms.TextInput(attrs={'id': 'time_sunday'}), required=False, label='')