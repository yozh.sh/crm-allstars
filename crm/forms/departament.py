from django import forms
from crm.models import Departament


class DepartamentModelForm(forms.ModelForm):
    class Meta:
        model = Departament
        fields = ['name']
        labels = {
            'name': 'Название'
        }
