from django import forms
from django.forms import fields
from crm.utils import phone_mask

class ClientSearchForm(forms.Form):
    surname = fields.CharField(label="", required=False, widget=fields.TextInput(attrs={
        "placeholder": "Фамилия"
    }))
    phone_number = forms.CharField(label="", required=False, widget=forms.TextInput(attrs={
        "id": "mask_phone_number",
        "autocomplete": "off",
        "data-mask": "+38(000)-000-00-00",
        "placeholder": "+38(_ _ _) - _ _ _ - _ _ - _ _)"

    }))

    def clean_phone_number(self):
        if len(self.cleaned_data['phone_number']) == 0:
            return ''
        elif len(self.cleaned_data['phone_number']) < 8:
            return ''
        return phone_mask.to_format_string(self.cleaned_data['phone_number'])

