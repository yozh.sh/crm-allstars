from django import forms
from crm.models import SourceOfAttraction, Client
from bootstrap_datepicker_plus import DatePickerInput


class SourceOfAttractionModelForm(forms.ModelForm):
    class Meta:
        model = SourceOfAttraction
        fields = ('name',)
        labels = {
            'name': 'Название'
        }