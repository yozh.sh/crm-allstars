from django import forms
from crm.models import Promotion
from crm.utils.dt import parse_date_from_input



class PromotionModelForm(forms.ModelForm):
    start_date = forms.CharField(widget=forms.DateInput(format='%d.%m.%Y', attrs={
        "data-mask": "00.00.0000",
        "placeholder": "день.месяц.год"
    }), label="Дата старта акции*", required=False)


    end_date = forms.CharField(widget=forms.DateInput(format='%d.%m.%Y', attrs={
        "data-mask": "00.00.0000",
        "placeholder": "день.месяц.год"

    }), label='Дата окончания*', required=False)

    def clean_end_date(self):
        if self.cleaned_data['temporary']:
            return parse_date_from_input(self.cleaned_data['end_date'])
        return None

    def clean_start_date(self):
        if self.cleaned_data['temporary']:
            return parse_date_from_input(self.cleaned_data['start_date'])
        return None

    def clean(self):
        start_date = self.cleaned_data['start_date']
        end_date = self.cleaned_data['end_date']
        if not start_date and not end_date:

            return super().clean()

        if start_date > end_date:
            self.add_error("start_date", "Дата начала акции не может быть больше чем дата окончания")
        super().clean()

    class Meta:
        model = Promotion
        fields = "__all__"
        exclude = ('active',)
        labels = {
            'name': 'Имя',
            'type_of_discount': 'Тип скидки',
            'discount': 'Скидка',
            'temporary': 'Временная',
        }