from django import forms
from crm.models import Abonement


class AbonementModelForm(forms.ModelForm):
    class Meta:
        model = Abonement
        fields = "__all__"
        labels = {
            'name': 'Имя',
            'available_visits_number': 'Количество посещений',
            'duration': 'Длительность(в днях)',
            'group_count': 'Количество групп',
            'number_of_freezing': 'Количество дней заморозки',
            'cost': 'Цена',
        }