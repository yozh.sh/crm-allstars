from django import forms


class LoginForm(forms.Form):
    email = forms.EmailField(error_messages={'required': ''})
    password = forms.CharField(widget=forms.PasswordInput())
