from django import forms
from crm.models import Client, SourceOfAttraction
from crm.utils.dt import parse_date_from_input
from crm.utils.phone_mask import to_format_string


class ClientModelForm(forms.ModelForm):
    
    birthday = forms.CharField(widget=forms.DateInput(format='%d.%m.%Y', attrs={
        "data-mask": "00.00.0000",
        "placeholder": "день.месяц.год"
    }), label="Дата рождения")

    phone_number = forms.CharField(required=True, min_length=18, label="Телефонный номер", widget=forms.TextInput(attrs={
        "id": "mask_phone_number",
        "autocomplete": "off",
        "data-mask": "+38(000)-000-00-00",
        "placeholder": "+38(_ _ _) - _ _ _ - _ _ - _ _)"

    }))

    parent_phone_number = forms.CharField(label="Телефонный номер родителя", required=False, widget=forms.TextInput(attrs={
        "id": "mask_phone_number1",
        "autocomplete": "off",
        "data-mask": "+38(000)-000-00-00",
        "placeholder": "+38(_ _ _) - _ _ _ - _ _ - _ _)"

    }))
    source_of_attraction = forms.ModelChoiceField(queryset=SourceOfAttraction.objects.all(), required=False, label="Источник привлечения")


    def clean_phone_number(self):
        if len(self.cleaned_data['phone_number']) == 0:
            return ''
        return to_format_string(self.cleaned_data['phone_number'])

    def clean_parent_phone_number(self):
        if len(self.cleaned_data['parent_phone_number']) == 0:
            return ''
        return to_format_string(self.cleaned_data['parent_phone_number'])

    def clean_birthday(self):
        date = self.cleaned_data['birthday']
        return parse_date_from_input(date)

    class Meta:
        model = Client
        labels = {
            "surname": "Фамилия",
            "name": "Имя",
            "middle_name": "Отчество",
            "sex": "Пол",
            "birthday": "Дата рождения",
            "phone_number": "Телефонный номер",
            "source_of_attraction": "Источник привлечения",
            "parent_FIO": "ФИО родителя",
            "parent_phone_number": "Телефонный номер родителя",
            "email": "Электронная почта",
            "notes": "Заметки"
        }
        fields = '__all__'
        exclude = ('created', 'updated', 'blocked_by_installment')
