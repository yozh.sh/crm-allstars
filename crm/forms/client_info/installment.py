from django import forms
from crm.models import InstallmentPayments
from crm.utils.dt import parse_date_from_input


class InstallmentPaymentsForm(forms.ModelForm):
    create_date = forms.CharField(widget=forms.DateInput(format='%d.%m.%Y', 
    attrs={
        "data-mask": "00.00.0000",
        "placeholder": "день.месяц.год"
    }), label="Дата платежа", required=True)
    installment_id = forms.CharField(widget=forms.HiddenInput())
    payment = forms.CharField(widget=forms.NumberInput(
        attrs={
            'class': 'form-control'
        }
    ), label='Сумма Платежа*', required=True)
    class Meta:
        model = InstallmentPayments
        exclude = ["client", "installment"]
        labels = {
            'payment': "Сумма Платежа"
        }
    
    def clean_create_date(self):
        date = self.cleaned_data['create_date']
        return parse_date_from_input(date)