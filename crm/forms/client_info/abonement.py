from django import forms
from crm.models import Sale
from crm.utils.dt import parse_date_from_input


class AbonementSaleClientInfoEdit(forms.ModelForm):
    activation = forms.BooleanField(widget=forms.CheckboxInput(attrs={
        'v-model': 'activation'
    }), label='Активация', required=False)

    activation_date = forms.CharField(widget=forms.DateInput(format='%d.%m.%Y', attrs={
        "data-mask": "00.00.0000",
        "placeholder": "день.месяц.год",
        'v-model': 'activationDate',
        'v-bind:class': "{ 'is-invalid': isActivationDateError }",
    }), label="Дата активации", required=False)

    deactivation_date = forms.CharField(widget=forms.DateInput(format='%d.%m.%Y', attrs={
        "data-mask": "00.00.0000",
        "placeholder": "день.месяц.год",
        'v-model': 'deactivationDate'
    }), label="Дата деактивации", required=False)

    def clean_activation_date(self):
        date = self.cleaned_data['activation_date']
        if date == '':
            return None
        return parse_date_from_input(date)

    def clean_deactivation_date(self):
        date = self.cleaned_data['deactivation_date']
        if date == '':
            return None
        return parse_date_from_input(date)

    class Meta:
        model = Sale
        fields = [
            'activation',
            'activation_date',
            'deactivation_date',
            'current_visits_number',
            'abonement_number_of_freezing',
        ]
        labels = {
            'activation': 'Активация',
            'activation_date': 'Дата активации',
            'deactivation_date': 'Дата деактивации',
            'current_visits_number': 'Количество посещений',
            'abonement_number_of_freezing': 'Количество дней заморозок',
        }


class AbonementFreezeForm(forms.Form):
    begin_date = forms.DateField(widget=forms.DateInput(
        format='%d.%m.%Y',
        attrs={
            "data-mask": "00.00.0000",
            "placeholder": "день.месяц.год",
        }
    ), label="Дата начала заморозки")

    freeze_day_count = forms.CharField(min_length=1, widget=forms.NumberInput(),
                                       label="Количество дней заморозки")

    reactivate_date = forms.DateField(widget=forms.DateInput(
        attrs={
            'readonly': "readonly"
        }
    ), label="Дата реактивации", required=False)

    abonement_id = forms.CharField(widget=forms.HiddenInput())
