from django import forms
from crm.models import Departament
from django.contrib.auth.models import User
from crm.utils.enums.roles import ROLES



class CreateUserModelForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())
    departament = forms.ModelChoiceField(queryset=Departament.objects.all())
    roles = forms.ChoiceField(choices=ROLES, label="Роль")
    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name', 'password', 'departament')

    def clean_email(self):
        email = self.cleaned_data['email']
        if User.objects.filter(email=email).exists():
            raise forms.ValidationError("Такой Email уже существует")
        return email


class EditUserForm(forms.ModelForm):
    departament = forms.ModelChoiceField(queryset=Departament.objects.all())
    roles = forms.ChoiceField(choices=ROLES, label="Роль")
    class Meta:
        model = User
        exclude = ('password', )
        fields = ('email', 'first_name', 'last_name')

    # def clean_email(self):
    #     email = self.cleaned_data['email']
    #     if User.objects.filter(email=email).exists():
    #         raise forms.ValidationError("Такой Email уже существует")
    #     return email


class UpdateUserPasswordForm(forms.Form):
    password = forms.CharField(min_length=3, label="Новый пароль", widget=forms.PasswordInput())