from crm.mixins import LoginMixin
from rest_framework.views import APIView
from crm.models import VisitsToGroup
from crm.serializers.visits import TodayVisitsSerializer
from rest_framework.response import Response
import datetime


class VisitsBase(APIView):
    serializer = None
    many = False

    def get_today_date_range(self):
        today_min = datetime.datetime.combine(datetime.date.today(),
                                              datetime.time.min)
        today_max = datetime.datetime.combine(datetime.date.today(),
                                              datetime.time.max)
        return today_min, today_max

    def get_data(self):
        if self.serializer:
            if self.many:
                serializer = self.serializer(self.get_queryset(), many=self.many)
            return serializer.data

    def get_queryset(self):
        pass

    def get(self, request, *args, **kwargs):
        return Response(data=self.get_data())


class GetVisitsClientToday(VisitsBase):
    # permission_classes = (IsAuthenticated, )
    serializer = TodayVisitsSerializer
    many = True

    def get_queryset(self):
        client_id = self.kwargs.get('client_id')
        today_min, today_max = self.get_today_date_range()
        return VisitsToGroup.objects.filter(client_id=client_id,
                                            visit_date__range=(today_min, today_max)).all()


class GetVisitsToday(VisitsBase):
    # permission_classes = (IsAuthenticated, )
    serializer = TodayVisitsSerializer
    many = True

    def get_queryset(self):
        today_min, today_max = self.get_today_date_range()
        return VisitsToGroup.objects.filter(visit_date__range=(today_min, today_max)).all()


class GetGroupVisitsToday(VisitsBase):
    # permission_classes = (IsAuthenticated, )
    serializer = TodayVisitsSerializer
    many = True

    def get_queryset(self):
        group_id = self.kwargs.get('group_id')
        return VisitsToGroup.objects.filter(group_id=group_id).all()