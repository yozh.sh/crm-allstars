from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from crm.views import BaseListView
from crm.models import SourceOfAttraction, Client, Sale
from crm.forms.source_of_attraction import SourceOfAttractionModelForm
from django.views.generic.edit import FormView
from django.http import JsonResponse
from django.views.generic import TemplateView
from django.contrib import messages
from django.shortcuts import redirect
from crm.mixins import LoginMixin


class SofaViewList(BaseListView):
    title = 'Source of Attraction'
    create_name = 'Источник'
    heading = 'Источник привлечения'
    template_name = 'sofa.html'
    create_form = SourceOfAttractionModelForm()
    create_url = 'sofa_create'

    def get_queryset(self):
        return SourceOfAttraction.objects.all()


class SofaCreateView(FormView):
    form_class = SourceOfAttractionModelForm

    def form_valid(self, form):
        sofa = form.save(commit=False)
        sofa.save()
        client_id_list = self.request.POST.getlist('client')
        for _id in client_id_list:
            client = Client.objects.get(pk=_id)
            client.source_of_attraction.add(sofa)
            client.save()

        return JsonResponse(data={}, status=200)

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({"error": dict(form.errors.items())}, status=400)



class SofaEditView(LoginMixin, TemplateView):
    template_name = 'crud/edit.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        sofa_id = kwargs['id']
        sofa = SourceOfAttraction.objects.get(pk=sofa_id)
        form = SourceOfAttractionModelForm(instance=sofa)
        context['edit_form'] = form
        return context

    def post(self, request, *args, **kwargs):
        sofa = SourceOfAttraction.objects.filter(id=kwargs.get('id')).first()
        form = SourceOfAttractionModelForm(request.POST, instance=sofa)
        if form.is_valid():
            form.save()
            return redirect('sofa_list')


def sofa_delete(request, id):
    if request.method == "POST":
        SourceOfAttraction.objects.get(pk=id).delete()
        messages.add_message(request, messages.SUCCESS, 'Источник успешно удалена')
        return redirect('sofa_list')


