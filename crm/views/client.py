from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from crm.views import BaseListView
from crm.models import Client
from crm.forms.client import ClientModelForm
from django.views.generic.edit import FormView
from django.http import JsonResponse
from django.views.generic import TemplateView
from django.contrib import messages
from django.shortcuts import redirect, render
from crm.forms.client_search import ClientSearchForm
from crm.utils.constants.pagination import SHOW_COUNT
from crm.mixins import LoginMixin
import datetime
from crm.models import SearchCache


class ClientViewList(BaseListView):
    permission_required = 'crm.can_view_client'
    title = 'Clients'
    create_name = 'Клиента'
    heading = 'Клиенты'
    template_name = 'client/client.html'
    create_form = ClientModelForm()
    create_url = 'client_create'
    search_form = ClientSearchForm()
    paginate_by = SHOW_COUNT
    queryset = Client.objects.order_by('surname')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['search_form'] = self.search_form
        return context


class ClientCreateView(FormView):
    form_class = ClientModelForm

    def form_valid(self, form):
        client = form.save(commit=False)
        client.save()
        form.save_m2m()

        return JsonResponse(data={}, status=200)

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({"error": dict(form.errors.items())}, status=400)


class ClientEditView(LoginMixin, TemplateView):
    template_name = 'crud/edit.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        client_id = kwargs['id']
        client = Client.objects.get(pk=client_id)
        form = ClientModelForm(instance=client)
        context['edit_form'] = form
        return context

    def post(self, request, *args, **kwargs):
        client = Client.objects.get(pk=kwargs.get('id'))
        form = ClientModelForm(request.POST, instance=client)
        if form.is_valid():
            form.save()
            return redirect('client_list')


class ClientSearchView(ClientViewList):
    context = None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['search_done'] = True
        self.context = context

    def render_results(self, form, request, **kwargs):
        qset = Client.objects.filter()
        final_qset = None
        if form.is_valid():
            phone_number = form.clean_phone_number()
            if form.cleaned_data['surname'] and phone_number:
                final_qset = qset.filter(surname__icontains=form.cleaned_data['surname'],
                                         phone_number__icontains=phone_number)
            elif form.cleaned_data['surname'] and len(phone_number) == 0:
                final_qset = qset.filter(surname__icontains=form.cleaned_data['surname'])
            elif phone_number and len(form.cleaned_data.get('surname')) == 0:
                final_qset = qset.filter(phone_number__contains=phone_number)

            self.object_list = final_qset
            self.get_context_data(**kwargs)
            print(request.POST, "REQUEST DATA!!!!!!!!")
            return render(request, self.template_name, context=self.context)

    def get(self, request, *args, **kwargs):
        cache = SearchCache.get(request.user.id)
        if cache is None:
            return redirect('client_list')
        return self.render_results(ClientSearchForm(data=cache), request, **kwargs)


    def post(self, request, *args, **kwargs):
        # костыльная валидация пустой формы
        surname, phone_number = request.POST.get('surname'), request.POST.get('phone_number')
        if not surname and not phone_number:
            return redirect('client_list')

        SearchCache.set(
            user_id=request.user.id,
            surname=surname,
            phone_number=phone_number
        )
        form = ClientSearchForm(request.POST)
        return self.render_results(form, request, **kwargs)

def delete_client(request, id):
    if request.method == "POST":
        Client.objects.get(pk=id).delete()
        messages.add_message(request, messages.SUCCESS, 'Клиент успешно удален')
        return redirect('client_list')



class AjaxGetNewClients(APIView):
    def get(self, request):
        if not request.user.is_authenticated:
            return Response(data=dict(), status=status.HTTP_403_FORBIDDEN)
        today = datetime.datetime.today()
        clients = Client.objects.filter(
            created__year=today.year,
            created__month=today.month,
            created__day=today.day
        ).count()
        return Response(data={'count': clients})


