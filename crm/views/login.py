from crm.forms.login import LoginForm
from django.views.generic.edit import FormView
from django.contrib.auth import authenticate, login
from django.shortcuts import redirect
from django.urls import reverse_lazy


class LoginView(FormView):
    template_name = 'login.html'
    form_class = LoginForm
    success_url = reverse_lazy('index')

    def form_valid(self, form):
        password, email = form.cleaned_data['password'], form.cleaned_data['email']
        user = authenticate(self.request, username=email, password=password)
        if user is not None:
            login(self.request, user)
            return super().form_valid(form)
        else:
            form.add_error(None, 'Пользователь или пароль не верный')
            return self.form_invalid(form)

    def form_invalid(self, form):
        form.clean()
        return super().form_invalid(form)