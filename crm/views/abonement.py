from crm.views import BaseListView
from crm.models import Abonement, CrmGroup
from crm.forms.abonement import AbonementModelForm
from django.views.generic.edit import FormView
from django.http import JsonResponse
from django.views.generic import TemplateView
from django.contrib import messages
from django.shortcuts import redirect
from rest_framework.views import APIView
from rest_framework.response import Response
from crm.serializers.sale import GroupSerializer, AbonementSerializer
from rest_framework import status
from crm.mixins import LoginMixin


class AbonViewList(BaseListView):
    title = 'Abonement'
    create_name = 'Абонемент'
    heading = 'Абонементы'
    template_name = 'abonement.html'
    create_form = AbonementModelForm()
    create_url = 'abon_create'

    def get_queryset(self):
        return Abonement.objects.all


class AbonCreateView(FormView):
    form_class = AbonementModelForm

    def form_valid(self, form):
        form.save()
        return JsonResponse(data={}, status=200)

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({"error": dict(form.errors.items())}, status=400)


class AbonEditView(LoginMixin, TemplateView):
    template_name = 'crud/edit.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        abon = Abonement.objects.get(pk=kwargs['id'])
        form = AbonementModelForm(instance=abon)
        context['edit_form'] = form
        return context

    def post(self, request, *args, **kwargs):
        abonement = Abonement.objects.filter(id=kwargs.get('id')).first()
        form = AbonementModelForm(request.POST, instance=abonement)
        if form.is_valid():
            form.save()
            return redirect('abon_list')



def abon_delete(request, id):
    if request.method == "POST":
        Abonement.objects.get(pk=id).delete()
        messages.add_message(request, messages.SUCCESS, 'Абонемент успешно удалена')
        return redirect('abon_list')


# AJAX DATA
class AjaxAbonementByGroupView(APIView):

    def get(self, request):
        if not request.user.is_authenticated:
            return Response(data=dict(), status=status.HTTP_403_FORBIDDEN)

        group_count = request.query_params.get('filter', None)
        abonement = Abonement.objects.filter(group_count=group_count)
        serialized = AbonementSerializer(abonement, many=True)
        return Response(data=serialized.data, status=status.HTTP_200_OK)


class AjaxOneTimeVisitAbonementByGroupView(APIView):

    def get(self, request):
        if not request.user.is_authenticated:
            return Response(data=dict(), status=status.HTTP_403_FORBIDDEN)

        group_count = request.query_params.get('filter', None)
        abonement = Abonement.objects.filter(available_visits_number=1).first()
        serialized = AbonementSerializer(abonement)
        return Response(data=serialized.data, status=status.HTTP_200_OK)




class AjaxGetAbonementPrice(APIView):
    def get(self, request, abon_id):
        if not request.user.is_authenticated:
            return Response(data=dict(), status=status.HTTP_403_FORBIDDEN)

        abonement = Abonement.objects.get(pk=abon_id)
        return Response(data={'full_price': abonement.cost})
