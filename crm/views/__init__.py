from django.views.generic.list import ListView
from crm.mixins import LoginMixin



class BaseListView(LoginMixin, ListView):
    title = None
    create_name = None
    heading = None
    create_form = None
    create_url = None
    edit_url = None
    edit_form = None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = self.title
        context['create_name'] = self.create_name
        context['heading'] = self.heading
        context['create_form'] = self.create_form or self.get_create_form()
        context['create_url'] = self.create_url
        context['edit_url'] = self.edit_url
        context['edit_form'] = self.edit_form
        return context
    
    def get_create_form(self):
        pass



