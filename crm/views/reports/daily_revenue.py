from django.views.generic import TemplateView
import datetime
from crm.models import Sale
from django.db.models import Sum


class DailyRevenueView(TemplateView):
    template_name = "reports/daily_revenue.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['sales'] = self.get_sales_today()
        context['departament'] = self.request.user.profile.departament
        context['cash'] = self.get_today_cash()
        return context

    def get_day_range(self) -> tuple:
        now = datetime.datetime.now()
        start_date = datetime.datetime(year=now.year, day=now.day, month=now.month, hour=0, minute=0, second=0)
        end_date = datetime.datetime(year=now.year, day=now.day, month=now.month, hour=23, minute=59, second=59)
        return start_date, end_date

    def get_today_cash(self):
        start_date, end_date = self.get_day_range()
        today_cash = Sale.objects.filter(
            sale_date__gte=start_date,
            sale_date__lte=end_date,
            departament_id=self.request.user.profile.departament.id
        ).aggregate(Sum('final_price'))
        return today_cash['final_price__sum']

    def get_sales_today(self):
        start_date, end_date = self.get_day_range()
        return Sale.objects.filter(
            sale_date__gte=start_date,
            sale_date__lte=end_date,
            departament_id=self.request.user.profile.departament.id
        )

    # TODO:
        # Добавить аякс загрузку!!!!!!