from django.views.generic import TemplateView
from crm.views import BaseListView
from django.contrib.auth.models import Group
from django.views.generic.edit import FormView
from crm.forms.rbac import DjangoGroupModelForm
from django.http import JsonResponse


class GroupView(TemplateView):
    template_name = 'rbac/groups/list.html'


class GroupView(BaseListView):
    title = 'Groups'
    create_name = 'Группу'
    heading = 'Группы'
    template_name = 'rbac/groups/list.html'
    create_form = DjangoGroupModelForm()
    create_url = 'group_settings_create'

    def get_queryset(self):
        return Group.objects.all()



class GroupCreateView(FormView):
    form_class = DjangoGroupModelForm

    def form_valid(self, form):


        return JsonResponse(data={}, status=200)

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({"error": dict(form.errors.items())}, status=400)