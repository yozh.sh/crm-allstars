from crm.views import BaseListView
from django.views import View
from django.http import JsonResponse
from crm.forms.user import CreateUserModelForm, UpdateUserPasswordForm
from django.views.generic.edit import FormView
from django.views.generic import TemplateView
from django.shortcuts import render, redirect
from crm.forms.user import EditUserForm
from django.contrib import messages
from django.contrib.auth.models import User
from crm.models import Departament, Profile
from django.contrib.auth.hashers import make_password
import json
import uuid
from rolepermissions.mixins import HasRoleMixin
from rolepermissions.roles import assign_role, remove_role
from rolepermissions.roles import get_user_roles



class UserViewList(HasRoleMixin, BaseListView):
    allowed_roles = ['system_admin', 'admin']
    title = 'Users'
    create_name = 'Пользователя'
    heading = 'Пользователи'
    template_name = 'rbac/users/list.html'
    create_form = CreateUserModelForm()
    create_url = 'user_setting_create'

    def get_queryset(self):
        return User.objects.all().order_by('email')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class UserCreateView(FormView):
    form_class = CreateUserModelForm

    def form_valid(self, form):
        user = User(
            username=str(uuid.uuid4()),
            email=form.clean_email(),
            first_name=form.cleaned_data['first_name'],
            last_name=form.cleaned_data['last_name'],
            password=make_password(form.cleaned_data['password']),
        )
        user.save()
        if form.cleaned_data['roles'] == 'USER':
            assign_role(user, 'user')
        elif form.cleaned_data['roles'] == 'ADMIN':
            assign_role(user, 'admin')
            user.is_superuser = True


        profile = Profile.objects.filter(user_id=user.id).first()




        departament = Departament.objects.filter(id=form.cleaned_data['departament'].id).first()
        profile.departament = departament
        profile.save()

        return JsonResponse(data={}, status=200)

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({"error": dict(form.errors.items())}, status=400)


class UserEditView(TemplateView):
    template_name = 'rbac/users/edit.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user_id = kwargs['id']
        user = User.objects.filter(id=user_id).first()
        form = EditUserForm(instance=user, initial={'departament': user.profile.departament})
        context['user_id'] = user_id
        context['edit_form'] = form
        context['update_password_form'] = UpdateUserPasswordForm()
        return context


    def post(self, *args, **kwargs):
        user = User.objects.filter(id=kwargs['id']).first()
        form = EditUserForm(self.request.POST, instance=user)
        if form.is_valid():
            roles = get_user_roles(user)
            role = None
            if len(roles):
                role = roles[0]

            if role is not None and role.get_role_name() == form.cleaned_data['roles']:
                form.save()
            elif role is not None and role.get_role_name() != form.cleaned_data['roles']:
                remove_role(user, role.get_name())
                if form.cleaned_data['roles'] == 'USER':
                    assign_role(user, 'user')
                    user.is_superuser = False
                elif form.cleaned_data['roles'] == 'ADMIN':
                    assign_role(user, 'admin')
                    user.is_superuser = True
                form.save()

            if role is None:
                if form.cleaned_data['roles'] == 'USER':
                    assign_role(user, 'user')
                elif form.cleaned_data['roles'] == 'ADMIN':
                    assign_role(user, 'admin')
                form.save()

            messages.add_message(self.request, messages.SUCCESS, 'Пользователь успешно обновлен')
            context = self.get_context_data(**kwargs)
            return redirect('user_setting_list')
        context = super().get_context_data(**kwargs)
        context['edit_form'] = form
        return render(self.request, self.template_name, context)


def delete_user(request, id):
    if request.method == "POST":
        User.objects.get(pk=id).delete()
        messages.add_message(request, messages.SUCCESS, 'Пользователь успешно удален')
        return redirect('user_setting_list')


class AjaxUpdateUserPassword(View):
    def post(self, request, *args, **kwargs):
        data = json.loads(request.body.decode('utf-8'))
        if len(data['password']) < 3:
            return JsonResponse({'error': 'min length of password must be more than 3 char'})
        user = User.objects.get(pk=data['user_id'])
        user.set_password(data['password'])
        user.save()
        return JsonResponse(data={'success': 'Обновленно успешно!'})

def mass_user_delete(request):
    return 200