from crm.views import BaseListView
from django.http import JsonResponse
from crm.models import Departament
from crm.forms.departament import DepartamentModelForm
from django.views.generic.edit import FormView
from django.views.generic import TemplateView
from django.contrib import messages
from django.shortcuts import redirect


class DepartamentViewList(BaseListView):
    title = 'Departaments'
    create_name = 'Филиал'
    heading = 'Филиалы'
    template_name = 'departament.html'
    create_form = DepartamentModelForm()
    create_url = 'departament_setting_create'

    def get_queryset(self):
        return Departament.objects.all()


class DepartamentCreateView(FormView):
    form_class = DepartamentModelForm

    def form_valid(self, form):
        departament = Departament(**form.cleaned_data)
        departament.save()
        return JsonResponse(data={}, status=200)

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({"error": dict(form.errors.items())}, status=400)


class DepartamentEditView(TemplateView):
    template_name = 'crud/edit.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        departament_id = kwargs['id']
        departament = Departament.objects.get(pk=departament_id)
        form = DepartamentModelForm(instance=departament)
        context['edit_form'] = form
        return context

def delete_departament(request, id):
    if request.method == "POST":
        Departament.objects.get(pk=id).delete()
        messages.add_message(request, messages.SUCCESS, 'Филиал успешно удален')
        return redirect('departament_setting_list')