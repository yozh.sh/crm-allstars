from crm.views import BaseListView
from crm.models import Promotion
from crm.forms.promotion import PromotionModelForm
from django.views.generic.edit import FormView
from django.http import JsonResponse
from django.views.generic import TemplateView
from django.contrib import messages
from django.shortcuts import redirect
from crm.mixins import LoginMixin


class PromotionViewList(BaseListView):
    title = 'Promotios'
    create_name = 'Акцию'
    heading = 'Акции'
    template_name = 'promotion.html'
    create_form = PromotionModelForm()
    create_url = 'promo_create'

    def get_queryset(self):
        return Promotion.objects.order_by('-created').filter(active=True)


class PromotionCreateView(FormView):
    form_class = PromotionModelForm

    def form_valid(self, form):
        form.save()
        return JsonResponse(data={}, status=200)

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({"error": dict(form.errors.items())}, status=400)


class PromotionEditView(LoginMixin, TemplateView):
    template_name = 'promotions/edit.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        client_id = kwargs['id']
        client = Promotion.objects.get(pk=client_id)
        form = PromotionModelForm(instance=client)
        context['edit_form'] = form
        return context

    def post(self, request, *args, **kwargs):
        promotion = Promotion.objects.filter(id=kwargs.get('id')).first()
        form = PromotionModelForm(request.POST, instance=promotion)
        if form.is_valid():
            form.save()
            return redirect('promo_list')


def promotion_delete(request, id):
    if request.method == "POST":
        promo = Promotion.objects.filter(pk=id).update(active=False)
        messages.add_message(request, messages.SUCCESS, 'Акция успешно удалена')
        return redirect('promo_list')