from django.shortcuts import redirect, render
from crm.views import BaseListView
from crm.forms.sale import SaleModelForm, SaleEditModelForm
from crm.forms.cashbox_search import CashboxSearchForm
from django.views.generic.edit import FormView
from crm.models import Sale, Client, CrmGroup, Tutor, Promotion, Departament, Abonement
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from crm.serializers.sale import GroupSerializer, AbonementSerializer, PromotionSerializer
import datetime
from django.http.response import JsonResponse
from crm.mixins import LoginMixin
from django.views.generic import TemplateView
from crm.utils.constants.pagination import SHOW_COUNT
from crm.models import Installment, CashBox
from django.forms import ValidationError
from django.db.utils import IntegrityError
from django.db import transaction

# SALE STRATEGY
from crm.strategies.sale.context import SaleContextStrategy
from crm.strategies.sale.one_group import OneGroup
from crm.strategies.sale.two_group import TwoGroup
from crm.strategies.sale.all_groups import AllGroup


class SaleViewList(BaseListView):
    # Вьюха для Кассы а не продаж
    title = 'Sale'
    create_name = 'Продажу'
    heading = 'Продажа'
    cashbox_search_form = CashboxSearchForm()
    template_name = 'sales.html'
    create_form = SaleModelForm
    create_url = 'sale_create'
    paginate_by = SHOW_COUNT

    def get_queryset(self):
        return CashBox.objects.filter(archived=False).order_by('-updated_at').all()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['create_form'] = self.create_form(initial={
            'departament1': self.request.user.profile.departament,
            'departament2': self.request.user.profile.departament,
            'activation_date': datetime.datetime.now(),
            'saled_by_departament': self.request.user.profile.departament
        })
        context['search_form'] = self.cashbox_search_form
        return context


class SaleCreateView(FormView):
    form_class = SaleModelForm
    strategy_context = SaleContextStrategy()

    def calculate_deactivation_date(self, days: int, activation_date: datetime.datetime) -> datetime.datetime:
        if not activation_date:
            return None
        return activation_date + datetime.timedelta(days=days)

    @transaction.atomic
    def form_valid(self, form):
        saled_by_departament = form.cleaned_data['saled_by_departament']
        client = form.cleaned_data['client']
        group_count = form.cleaned_data['abonement_group_count']
        abonement: Abonement = form.cleaned_data['abonement']
        activation = form.cleaned_data['activation']
        promotion = form.cleaned_data['promotion']
        installment = form.cleaned_data['installment_plan']
        payment_type = form.cleaned_data['payment_type']
        end_payment = form.cleaned_data['end_payment']
        activation_date = form.cleaned_data['activation_date']

        if not activation:
            activation_date = None

        # Взять акцию с формы, скопировать в таблицу PromotionToSale, привязать к продаже
        if promotion:
            promotion.save()

        sale = Sale(
            saled_by=self.request.user.profile,
            saled_by_departament=saled_by_departament,
            client=client,
            abonement_group_count=group_count,
            abonement=abonement,
            activation=activation,
            activation_date=activation_date,
            deactivation_date=self.calculate_deactivation_date(abonement.duration, activation_date),
            promotion=promotion,
            installment_plan=installment,
            payment_type=payment_type,
            end_payment=end_payment,
            saled_by_installment=True if installment else False,
        )
        sale.set_abonement_values(abonement.name, abonement.available_visits_number, abonement.duration, abonement.number_of_freezing, abonement.cost)

        if group_count == 'ONE':
            self.strategy_context.set_strategy(OneGroup())
        elif group_count == 'TWO':
            self.strategy_context.set_strategy(TwoGroup())
        elif group_count == 'ALL':
            self.strategy_context.set_strategy(AllGroup())

        sale_m2m_adder = self.strategy_context.exec_strategy(
            sale=sale,
            group1=form.cleaned_data['group1'],
            group2=form.cleaned_data['group2'],
            tutor1=form.cleaned_data['tutor1'],
            tutor2=form.cleaned_data['tutor2'],
            departament1=form.cleaned_data['departament1'],
            departament2=form.cleaned_data['departament2']
        )

        sale.save()
        sale_m2m_adder.set_relation()


        if installment:
            client.block_by_installment()
            
            new_installment = Installment(
                client=client,
                sale=sale,
                debt=promotion.get_discounted(sale.abonement_price) if promotion else sale.abonement_price,
            )
            prev_payment = Installment.objects.filter(client=client, sale=sale).all()
            if len(prev_payment) == 0:
                new_installment.save()
                # FIRST PAYMENT
                try:
                    new_installment.new_payment(end_payment, first_payment=True)
                except IntegrityError:
                    raise Exception("ERROR")

            else:
                new_installment.save()
                try:
                    new_installment.new_payment(end_payment)
                except ValidationError:
                    print("VALIDATION ERROR")

        sale.save_cashbox()
        return JsonResponse(data={}, status=200)

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({"error": dict(form.errors.items())}, status=400)


class SaleEditView(TemplateView):
    template_name = 'sales/edit.html'
    strategy_context = SaleContextStrategy()


    def calculate_deactivation_date(self, days: int, activation_date: datetime.datetime) -> datetime.datetime:
        if not activation_date:
            return None
        return activation_date + datetime.timedelta(days=days)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        sale_id = kwargs['id']
        sale = Sale.objects.get(pk=sale_id)
        initial_data = self.get_initial_data(sale)
        form = SaleEditModelForm(initial=initial_data, instance=sale)
        context['edit_form'] = form
        return context

    def get_initial_data(self, sale):
        form_data = sale.saleformdata_set.last()
        data = {
            'tutor1': Tutor.objects.filter(id=form_data.order_tutor1).first(),
            'tutor2': Tutor.objects.filter(id=form_data.order_tutor2).first(),
            'departament1': Departament.objects.filter(id=form_data.order_departament1).first(),
            'departament2': Departament.objects.filter(id=form_data.order_departament2).first(),
            'group1': CrmGroup.objects.filter(id=form_data.order_group1).first(),
            'group2': CrmGroup.objects.filter(id=form_data.order_group2).first(),
            'sale_id': sale.id,
            'activation_date': sale.activation_date if sale.activation else datetime.datetime.now(),
            'saled_by_departament': self.request.user.profile.departament

        }
        if sale.promotion:
            promotion = Promotion.objects.filter(pk=sale.promotion.template_promotion_id).first()
            if promotion:
                data['promotion'] = promotion
        return data

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        instance = Sale.objects.get(pk=kwargs.get('id'))
        form = SaleEditModelForm(request.POST, instance=instance)
        if form.is_valid():
            client = form.cleaned_data['client']
            group_count = form.cleaned_data['abonement_group_count']
            abonement = form.cleaned_data['abonement']
            activation = form.cleaned_data['activation']
            promotion = form.cleaned_data['promotion']
            installment = form.cleaned_data['installment_plan']
            payment_type = form.cleaned_data['payment_type']
            end_payment = form.cleaned_data['end_payment']
            activation_date = form.cleaned_data['activation_date']
            if promotion: promotion.save()
            if not activation:
                activation_date = None


            instance.client = client
            instance.abonement_group_count = group_count
            instance.abonement = abonement
            instance.activation = activation
            instance.promotion = promotion
            instance.installment_plan = installment
            instance.payment_type = payment_type
            instance.end_payment = end_payment
            instance.activation_date = activation_date
            instance.deactivation_date = self.calculate_deactivation_date(abonement.duration, activation_date)


            # instance.set_abonement_values(abonement.name, instance.available_visits_number, abonement.duration, abonement.number_of_freezing, abonement.cost)

            if group_count == 'ONE':
                self.strategy_context.set_strategy(OneGroup())
            elif group_count == 'TWO':
                self.strategy_context.set_strategy(TwoGroup())
            elif group_count == 'ALL':
                self.strategy_context.set_strategy(AllGroup())

            sale_m2m_adder = self.strategy_context.exec_strategy(
                sale=instance,
                group1=form.cleaned_data['group1'],
                group2=form.cleaned_data['group2'],
                tutor1=form.cleaned_data['tutor1'],
                tutor2=form.cleaned_data['tutor2'],
                departament1=form.cleaned_data['departament1'],
                departament2=form.cleaned_data['departament2']
            )

            if not instance.saled_by_installment and installment:
                client.block_by_installment()

                new_installment = Installment(
                    client=client,
                    sale=instance,
                    debt=promotion.get_discounted(instance.abonement_price) if promotion else instance.abonement_price,
                )
                prev_payment = Installment.objects.filter(client=client, sale=instance).all()
                if len(prev_payment) == 0:
                    new_installment.save()
                    # FIRST PAYMENT
                    try:
                        new_installment.new_payment(end_payment, first_payment=True)
                        instance.saled_by_installment = True
                    except IntegrityError:
                        raise Exception("ERROR")

                else:
                    new_installment.save()
                    try:
                        new_installment.new_payment(end_payment)
                        instance.saled_by_installment = True
                    except ValidationError:
                        print("VALIDATION ERROR")

            instance.save()
            sale_m2m_adder.set_relation()
            instance.update_cashbox()

            # RUN TASKS
            # instance.run_deactivate_task(update=True)
            return redirect('sale_list')

        return render(request, self.template_name, {'edit_form': form})


class SaleDelete(APIView):
    def post(self, request):
        if not request.user.is_authenticated:
            return Response(data=dict(), status=status.HTTP_403_FORBIDDEN)

        sales_ids = request.data
        ids = map(int, dict(sales_ids)['elemIds[]'])
        for _id in ids:
            sale = Sale.objects.filter(id=_id).first()
            sale.delete_sale()
        return Response(data={}, status=status.HTTP_200_OK)


def sale_delete(request, sale_id):
    if request.method == "POST":
        sale = Sale.objects.get(pk=sale_id)
        sale.delete_sale()
        return redirect('sale_list')


# SEARCH CASHBOX
class CashboxSearchView(SaleViewList):
    context = None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['search_done'] = True
        self.context = context

    def post(self, request, *args, **kwargs):
        form = CashboxSearchForm(request.POST)
        qset = CashBox.objects.filter(archived=False)
        final_qset = None
        if form.is_valid():
            surname = form.cleaned_data['surname']
            cost = form.cleaned_data['cost']
            if surname and cost:
                final_qset = qset.filter(
                    abonement__client__surname__icontains=surname,
                    price=cost
                )
            elif surname and cost == '':
                final_qset = qset.filter(
                    abonement__client__surname__icontains=surname
                )
            elif cost and len(surname) == 0:
                final_qset = qset.filter(
                    price=cost
                )

            self.object_list = final_qset
            self.get_context_data(**kwargs)
            return render(request, self.template_name, context=self.context)
        return redirect('sale_list')

# AJAX DATA
class AjaxGroupsByTutorView(APIView):
    def get(self, request):
        if not request.user.is_authenticated:
            return Response(data=dict(), status=status.HTTP_403_FORBIDDEN)

        departament = request.query_params.get('departament', None)
        tutor_id = request.query_params.get('tutor_id', None)



        if tutor_id and departament:
            groups = CrmGroup.objects.filter(tutor_id=tutor_id, departament_id=departament)
        elif tutor_id and len(departament) == 0:
            groups = CrmGroup.objects.filter(tutor_id=tutor_id)
        elif len(tutor_id) == 0 and departament:
            groups = CrmGroup.objects.filter(departament_id=departament)
        else:
            return Response(data={}, status=status.HTTP_400_BAD_REQUEST)
        serialized = GroupSerializer(groups, many=True)
        return Response(data=serialized.data, status=status.HTTP_200_OK)



class AjaxGetLastSoldAbon(APIView):
    def get(self, request, client_id):
        if not request.user.is_authenticated:
            return Response(data=dict(), status=status.HTTP_403_FORBIDDEN)

        last_sale = Sale.objects.select_related('abonement').filter(client_id=client_id).order_by('-id')[:1]
        if not last_sale:
            return Response(data={})
        serialized = AbonementSerializer(last_sale[0].abonement)
        return Response(data=serialized.data, status=status.HTTP_200_OK)


class AjaxGetDiscount(APIView):
    def get(self, request):
        if not request.user.is_authenticated:
            return Response(data=dict(), status=status.HTTP_403_FORBIDDEN)

        promo_id = request.query_params.get('promo_id', None)
        money = request.query_params.get('money', None)

        if len(promo_id) == 0 or len(money) == 0:
            return Response(data={}, status=status.HTTP_400_BAD_REQUEST)


        promotion = Promotion.objects.get(pk=int(promo_id))
        calculating_money = promotion.get_discounted(int(money))
        return Response(data={'money': calculating_money})


class AjaxGetLastSoldPromotion(APIView):
    def get(self, request, client_id):
        if not request.user.is_authenticated:
            return Response(data=dict(), status=status.HTTP_403_FORBIDDEN)

        last_sale = Sale.objects.select_related('promotion').filter(client_id=client_id).order_by('-id')[:1]
        if not last_sale:
            return Response(data={})
        serialized = PromotionSerializer(last_sale[0].promotion)
        return Response(data=serialized.data)


class AjaxGetAbonementIdBySale(APIView):
    def get(self, request):
        if not request.user.is_authenticated:
            return Response(data=dict(), status=status.HTTP_403_FORBIDDEN)

        sale_id = request.query_params.get('sale_id', None)
        if sale_id is None:
            return Response(data=dict(), status=status.HTTP_400_BAD_REQUEST)

        abonement_id = Sale.objects.prefetch_related('abonement').filter(id=sale_id).values('abonement').first()
        return Response(data=abonement_id)



class AjaxDeleteRevoke(APIView):
    def post(self, request):
        if not request.user.is_authenticated:
            return Response(data=dict(), status=status.HTTP_403_FORBIDDEN)


        cashbox_id = request.data.get('cashbox_id', None)
        if cashbox_id:
            cashbox = CashBox.objects.get(pk=cashbox_id)
            cashbox.hide_revoke()
            return Response(data=dict(), status=status.HTTP_200_OK)
        return Response(data=dict(), status=status.HTTP_400_BAD_REQUEST)


class AjaxGetGroupsBySale(APIView):
    def get(self, request):
        if not request.user.is_authenticated:
            return Response(data=dict(), status=status.HTTP_403_FORBIDDEN)

        data = {}
        sale_id = request.query_params.get('sale_id', None)
        if not sale_id:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        sale = Sale.objects.get(pk=sale_id)
        form_data = sale.saleformdata_set.last()
        data['group1'] = form_data.order_group1
        data['group2'] = form_data.order_group2

        return Response(data=data)
