from django.shortcuts import get_object_or_404
from django.forms import HiddenInput
from django.views.generic import TemplateView
from crm.mixins import LoginMixin
from crm.models import CrmGroup, Client
from django.http import HttpResponseNotFound
from crm.forms.sale import SaleModelForm, SaleOneTimeVisitForm
import datetime

class CalendarView(LoginMixin, TemplateView):
    template_name = 'calendar.html'


class CalendarAddVisitsView(LoginMixin, TemplateView):
    template_name = 'addVisit.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        group_id, day = kwargs.get('group_id'), kwargs.get('day_number')
        group = get_object_or_404(CrmGroup, pk=group_id)
        context['group'] = group
        context['training_days'] = group.get_day(day)
        default_client = Client.objects.filter(blocked_by_installment=False).all()[0]
        context['sale_form'] = SaleOneTimeVisitForm(initial={
            'client': default_client,
            'departament1': self.request.user.profile.departament,
            'departament2': self.request.user.profile.departament,
            'activation_date': datetime.datetime.now(),
            'saled_by_departament': self.request.user.profile.departament
        })  # Форма одноразового занятия

        context['sale_form'].fields['saled_by_departament'].widget = HiddenInput()
        return context
