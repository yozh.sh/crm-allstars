from rest_framework.viewsets import ModelViewSet
from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.response import Response
from rest_framework import status
from crm.serializers.calendar import ClientSerializer, Client
from crm.serializers.client import ClientCheckSubscriptionInGroup
from crm.serializers.sale import SaleSerializer
from crm.models import Sale
from crm.utils.dt import datetime_now


class ClientApiView(ModelViewSet):
    serializer_class = ClientSerializer
    queryset = Client.objects.filter(blocked_by_installment=False)


class CheckClientSubscriptionInGroup(RetrieveAPIView):
    def retrieve(self, request, client_id, group_id, *args, **kwargs):
        sales = Sale.objects.filter(client_id=client_id).all()
        for sale in sales:
            if sale.is_active():
                if sale.group.filter(pk=group_id).first():
                    serializer = ClientCheckSubscriptionInGroup(dict(subscription=True))
                    return Response(data=serializer.data)
        return Response(data=ClientCheckSubscriptionInGroup(dict(subscription=False)).data)


class GetAbonementsByClient(ListAPIView):
    serializer_class = SaleSerializer

    def get_queryset(self):
        sale_qs = Sale.objects.filter(
            client_id=self.kwargs.get('id'),
        )
        if self.request.query_params.get('active', None):
            now = datetime_now()
            return sale_qs.filter(activation_date__gte=now, activation_date__lt=now)
        return sale_qs

