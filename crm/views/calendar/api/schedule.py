from django.urls import reverse
from rest_framework.generics import CreateAPIView
from rest_framework.mixins import DestroyModelMixin
from crm.serializers.calendar import CalendarScheduleCancelGroupSerializer, CalendarScheduleByDate
from crm.auth.csrf_exempt import CsrfExemptSessionAuthentication
from crm.models import CrmGroup
from rest_framework.response import Response


class ScheduleByDayView(CreateAPIView):
    serializer_class = CalendarScheduleByDate
    authentication_classes = (CsrfExemptSessionAuthentication, )

    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        response_data = []
        if serializer.is_valid():
            date = serializer.validated_data.get('date')
            day_of_week = date.isoweekday()
            # groups_by_date = CrmGroup.objects.raw('''
            # SELECT * FROM crm_crmgroup
            # where training_days @> '[{"day": %s}]';
            #
            # ''', [day_of_week])
            # print(list(groups_by_date))

            query_result = []

            for group in CrmGroup.objects.all():
                for data in group.training_days:
                    if data['day'] == date.isoweekday() and data['end_time'] is not None:
                        query_result.append(group)

            # Тут нужно отсечь все группы у которых занятия отменены

            for group in query_result:
                training_data = group.get_day(date.isoweekday())
                if len(training_data):
                    training_data['tutor_fullname'] = group.tutor.full_name
                    training_data['name'] = group.name
                    training_data['add_visits_url'] = reverse('calendar_addvisits', kwargs={'group_id': group.id, 'day_number': training_data.get('day')})
                    response_data.append(training_data)
            return Response(response_data)
        return Response(serializer.errors)


class CalendarScheduleCancelGroupView(CreateAPIView, DestroyModelMixin):
    serializer_class = CalendarScheduleCancelGroupSerializer
