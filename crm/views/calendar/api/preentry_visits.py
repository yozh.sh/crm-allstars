from rest_framework.generics import CreateAPIView, DestroyAPIView, ListAPIView
from crm.auth.csrf_exempt import CsrfExemptSessionAuthentication
from crm.serializers.calendar import PreEntryVisitSerializer, ClientSerializer, PreEntryVisitsGroupSerializer
from crm.models import PreEntryClients, Client
from django.shortcuts import get_object_or_404
from crm.utils.dt import parse_date, parse_date_from_input
from rest_framework.response import Response
from rest_framework import status


class CreatePreEntryVisitView(CreateAPIView):
    authentication_classes = (CsrfExemptSessionAuthentication, )
    serializer_class = PreEntryVisitSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        client = serializer.data.get('client')
        group = serializer.data.get('group')
        date = parse_date_from_input(serializer.data.get('date'))
        preentry = PreEntryClients.objects.filter(client_id=client, group_id=group, date=date).first()
        if not preentry:
            return super(CreatePreEntryVisitView, self).create(request, *args, **kwargs)
        return Response(status=status.HTTP_204_NO_CONTENT)


class DeletePreEntryVisitView(DestroyAPIView):
    authentication_classes = (CsrfExemptSessionAuthentication, )
    serializer_class = PreEntryVisitSerializer
    queryset = PreEntryClients.objects.all()

    def get_object(self):
        obj = get_object_or_404(PreEntryClients, pk=self.kwargs.get('id'))
        self.check_object_permissions(self.request, obj)
        return obj


class GetPreEntryGroupByUser(ListAPIView):
    authentication_classes = (CsrfExemptSessionAuthentication,)
    serializer_class = PreEntryVisitsGroupSerializer

    def get_queryset(self):
        return get_object_or_404(PreEntryClients, client_id=self.kwargs.get('pk')),


class GetPreEntryByGroupAndDate(ListAPIView):
    authentication_classes = (CsrfExemptSessionAuthentication, )
    serializer_class = PreEntryVisitsGroupSerializer

    def get_queryset(self):
        date = parse_date(self.request.query_params.get('date'))
        return PreEntryClients.objects.select_related('client', 'group').filter(
            group_id=self.kwargs.get('id'),
            date=date
        )
        # client_ids = PreEntryClients.objects.filter(
        #     group_id=self.kwargs.get('id'),
        #     date=date
        # ).values_list('client_id', flat=True)
        #
        # return Client.objects.filter(id__in=client_ids)
