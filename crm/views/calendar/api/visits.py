from rest_framework import status
from rest_framework.generics import CreateAPIView, DestroyAPIView, ListAPIView
from rest_framework.generics import GenericAPIView
from crm.serializers.calendar import VisitSerializer, ClientSerializer, VisitDetailSerializer
from crm.auth.csrf_exempt import CsrfExemptSessionAuthentication
from crm.models import VisitsToGroup, Client
from rest_framework.response import Response
from crm.utils.dt import parse_date, parse_date_from_input
from rest_framework import status



class CreateVisitsView(CreateAPIView):
    authentication_classes = (CsrfExemptSessionAuthentication, )
    serializer_class = VisitSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        group = serializer.validated_data.get('group')
        client = serializer.validated_data.get('client')
        sale = client.check_abonement_visit(group)
        if not sale:
            return Response(data={}, status=status.HTTP_400_BAD_REQUEST)

        # проверка на предотвращение дублей
        exist_visit = VisitsToGroup.objects.filter(
            group=group,
            client=client,
            visit_date=serializer.validated_data.get('visit_date')
        )
        if exist_visit:
            return Response(status=status.HTTP_204_NO_CONTENT)

        new_visit = VisitsToGroup.objects.create(
            **serializer.validated_data,
            abonement=sale
        )
        new_visit_serializer = self.get_serializer(instance=new_visit)
        return Response(data=new_visit_serializer.data)


class DestroyVisitsView(DestroyAPIView):
    authentication_classes = (CsrfExemptSessionAuthentication,)
    serializer_class = VisitSerializer
    queryset = VisitsToGroup.objects.all()

    def get_object(self):
        queryset = self.filter_queryset(self.get_queryset())
        obj = queryset.get(pk=self.kwargs.get('id'))
        self.check_object_permissions(self.request, obj)
        return obj


class GetVisitsByGroupAndDate(ListAPIView):
    authentication_classes = (CsrfExemptSessionAuthentication,)
    serializer_class = VisitDetailSerializer

    def get_queryset(self):
        date = parse_date(self.request.query_params.get('date'))
        return VisitsToGroup.objects.select_related(
            'client',
            'group',
            'abonement',
        ).filter(
            visit_date=date,
            group_id=self.kwargs.get('id')
        )
        # client_ids = VisitsToGroup.objects.filter(
        #     group_id=self.kwargs.get('id'),
        #     visit_date=date
        # ).values_list('client_id', flat=True)
        #
        # return Client.objects.filter(id__in=client_ids)




