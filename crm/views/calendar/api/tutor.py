from django.urls import reverse
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from crm.models import Tutor
from crm.utils.dt import datetime_now


class CalendarDayView(ListAPIView):
    # queryset = Tutor.objects.all()

    def list(self, request, *args, **kwargs):
        resp_data = list()
        for tutor in Tutor.objects.all():
            group_set = tutor.crmgroup_set.all
            for group in group_set():
                training_data = group.get_day(datetime_now().isoweekday())
                if len(training_data):
                    training_data['tutor_fullname'] = tutor.full_name
                    training_data['name'] = group.name
                    training_data['add_visits_url'] = reverse('calendar_addvisits', kwargs={'group_id': group.id, 'day_number': training_data.get('day')})
                    resp_data.append(training_data)
        return Response(resp_data)