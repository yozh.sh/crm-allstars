from crm.views import BaseListView
from django.http import JsonResponse
from crm.models import CrmGroup
from crm.forms.group import CrmGroupModelForm, MondayDayForm, TuesdayDayForm, WednesdayDayForm, ThursdayDayForm, FridayDayForm, SaturdayDayForm, SundayDayForm
from django.views.generic.edit import FormView
from django.views.generic import TemplateView
from django.contrib import messages
from django.shortcuts import redirect
from crm.utils.dt import time_delta
from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from crm.utils.constants.pagination import SHOW_COUNT
from django.forms import formset_factory
from crm.utils.models.dayofweek import DAY_OF_THE_WEEK
from django.views.generic import View
from crm.mixins import LoginMixin


class GroupViewList(BaseListView):
    title = 'Groups'
    create_name = 'Группу'
    heading = 'Группы'
    template_name = 'group.html' # TODO: need change template for bisnes login. Now template used as a mock
    create_form = CrmGroupModelForm()
    create_url = 'group_create'
    paginate_by = SHOW_COUNT
    queryset = CrmGroup.objects.order_by('name')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['utils'] = {
            'checkbox_to_dropdown': {
                'checkbox_id': 'id_is_podushka',
                'hiden_field_id': 'id_podushka_cost'
            },
            'input_mask': {
                'field_id': 'schedule_input',
                'mask_format': '00:00'
            }
        }
        context['training_days'] = {
            'monday': MondayDayForm(),
            'tuesday': TuesdayDayForm(),
            'wednesday': WednesdayDayForm(),
            'thursday': ThursdayDayForm(),
            'friday': FridayDayForm(),
            'saturday': SaturdayDayForm(),
            'sunday': SundayDayForm()
        }
        return context


class GroupCreateView(View):
    def post(self, request, *args, **kwargs):
        data = [
            {"day": 1, "begin_time": None, "end_time": None},
            {"day": 2, "begin_time": None, "end_time": None},
            {"day": 3, "begin_time": None, "end_time": None},
            {"day": 4, "begin_time": None, "end_time": None},
            {"day": 5, "begin_time": None, "end_time": None},
            {"day": 6, "begin_time": None, "end_time": None},
            {"day": 7, "begin_time": None, "end_time": None},
        ]
        main_form = CrmGroupModelForm(request.POST)
        if main_form.is_valid():
            pass
        else:
            return JsonResponse({"error": dict(main_form.errors.items())}, status=400)

        # DAY OF WEEK
        monday = MondayDayForm(request.POST)
        tuesday = TuesdayDayForm(request.POST)
        wednesday = WednesdayDayForm(request.POST)
        thursday = ThursdayDayForm(request.POST)
        friday = FridayDayForm(request.POST)
        saturday = SaturdayDayForm(request.POST)
        sunday = SundayDayForm(request.POST)

        if monday.is_valid() and monday.cleaned_data['monday_day'] and monday.cleaned_data['monday_time']:
            for x in data:
                if x['day'] == 1:
                    x['begin_time'] = monday.cleaned_data['monday_time'].isoformat()
                    x['end_time'] = time_delta(monday.cleaned_data['monday_time'], main_form.cleaned_data['training_duration']).isoformat()

        else:
            pass


        if tuesday.is_valid() and tuesday.cleaned_data['tuesday_day'] and tuesday.cleaned_data['tuesday_time']:
            for x in data:
                if x['day'] == 2:
                    x['begin_time'] = tuesday.cleaned_data['tuesday_time'].isoformat()
                    x['end_time'] = time_delta(tuesday.cleaned_data['tuesday_time'],
                                               main_form.cleaned_data['training_duration']).isoformat()
        else:
            pass

        if wednesday.is_valid() and wednesday.cleaned_data['wednesday_day'] and wednesday.cleaned_data['wednesday_time']:
            for x in data:
                if x['day'] == 3:
                    x['begin_time'] = wednesday.cleaned_data['wednesday_time'].isoformat()
                    x['end_time'] = time_delta(wednesday.cleaned_data['wednesday_time'], main_form.cleaned_data['training_duration']).isoformat()
        else:
            pass

        if thursday.is_valid() and thursday.cleaned_data['thursday_day'] and thursday.cleaned_data['thursday_time']:
            for x in data:
                if x['day'] == 4:
                    x['begin_time'] = thursday.cleaned_data['thursday_time'].isoformat()
                    x['end_time'] = time_delta(thursday.cleaned_data['thursday_time'],
                                               main_form.cleaned_data['training_duration']).isoformat()
        else:
            pass

        if friday.is_valid() and friday.cleaned_data['friday_day'] and friday.cleaned_data['friday_time']:
            for x in data:
                if x['day'] == 5:
                    x['begin_time'] = friday.cleaned_data['friday_time'].isoformat()
                    x['end_time'] = time_delta(friday.cleaned_data['friday_time'],
                                               main_form.cleaned_data['training_duration']).isoformat()
        else:
            pass

        if saturday.is_valid() and saturday.cleaned_data['saturday_day'] and saturday.cleaned_data['saturday_time']:
            for x in data:
                if x['day'] == 6:
                    x['begin_time'] = saturday.cleaned_data['saturday_time'].isoformat()
                    x['end_time'] = time_delta(saturday.cleaned_data['saturday_time'],
                                               main_form.cleaned_data['training_duration']).isoformat()
        else:
            pass

        if sunday.is_valid() and sunday.cleaned_data['sunday_day'] and sunday.cleaned_data['sunday_time']:
            for x in data:
                if x['day'] == 7:
                    x['begin_time'] = sunday.cleaned_data['sunday_time'].isoformat()
                    x['end_time'] = time_delta(sunday.cleaned_data['sunday_time'],
                                               main_form.cleaned_data['training_duration']).isoformat()
        else:
            pass


        name = main_form.cleaned_data['name']
        rate = main_form.cleaned_data['rate']
        is_podushka = main_form.cleaned_data['is_podushka']
        departament = main_form.cleaned_data['departament']
        one_visit_price = main_form.cleaned_data['one_visit_price']
        training_duration = main_form.cleaned_data['training_duration']
        podushka_cost = main_form.cleaned_data['podushka_cost']



        new_group = CrmGroup(
            name=name,
            rate=rate,
            is_podushka=is_podushka,
            departament=departament,
            one_visit_price=one_visit_price,
            tutor=main_form.cleaned_data['tutor'],
            podushka_cost=podushka_cost if podushka_cost is not None else 0,
            training_duration=training_duration,
            training_days=data
        )
        new_group.save()
        return JsonResponse(data={}, status=200)


class GroupEditView(LoginMixin, TemplateView):
    template_name = 'group/edit.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        group = CrmGroup.objects.get(pk=kwargs.get('id'))
        context['utils'] = {
            'checkbox_to_dropdown': {
                'checkbox_id': 'id_is_podushka',
                'hiden_field_id': 'id_podushka_cost'
            }
        }
        training_days = group.training_days

        monday = MondayDayForm(initial={
            'monday_day': True if training_days[0]['end_time'] != None else False,
            'monday_time': training_days[0]['begin_time'] if training_days[0]['begin_time'] != None else ''

        })
        tuesday = TuesdayDayForm(initial={
            'tuesday_day': True if training_days[1]['end_time'] != None else False,
            'tuesday_time': training_days[1]['begin_time'] if training_days[1]['begin_time'] != None else ''
        })
        wednesday = WednesdayDayForm(initial={
            'wednesday_day': True if training_days[2]['end_time'] != None else False,
            'wednesday_time': training_days[2]['begin_time'] if training_days[2]['begin_time'] != None else ''
        })
        thursday = ThursdayDayForm(initial={
            'thursday_day': True if training_days[3]['end_time'] != None else False,
            'thursday_time': training_days[3]['begin_time'] if training_days[3]['begin_time'] != None else ''
        })
        friday = FridayDayForm(initial={
            'friday_day': True if training_days[4]['end_time'] != None else False,
            'friday_time': training_days[4]['begin_time'] if training_days[4]['begin_time'] != None else ''
        })
        saturday = SaturdayDayForm(initial={
            'saturday_day': True if training_days[5]['end_time'] != None else False,
            'saturday_time': training_days[5]['begin_time'] if training_days[5]['begin_time'] != None else ''
        })
        sunday = SundayDayForm(initial={
            'sunday_day': True if training_days[6]['end_time'] != None else False,
            'sunday_time': training_days[6]['begin_time'] if training_days[6]['begin_time'] != None else ''
        })

        form = CrmGroupModelForm(instance=group)
        context['training_days'] = {
            'monday': monday,
            'tuesday': tuesday,
            'wednesday': wednesday,
            'thursday': thursday,
            'friday': friday,
            'saturday': saturday,
            'sunday': sunday
        }
        context['edit_form'] = form
        return context

    def post(self, request, *args, **kwargs):
        main_form = CrmGroupModelForm(request.POST, instance=CrmGroup.objects.get(pk=kwargs.get('id')))
        if main_form.is_valid():
            pass
        else:
            return render(request, self.template_name, {'edit_form': main_form})

        data = main_form.instance.training_days
        monday = MondayDayForm(request.POST)
        tuesday = TuesdayDayForm(request.POST)
        wednesday = WednesdayDayForm(request.POST)
        thursday = ThursdayDayForm(request.POST)
        friday = FridayDayForm(request.POST)
        saturday = SaturdayDayForm(request.POST)
        sunday = SundayDayForm(request.POST)

        if monday.is_valid() and monday.cleaned_data['monday_day'] and monday.cleaned_data['monday_time']:
            for x in data:
                if x['day'] == 1:
                        x['begin_time'] = monday.cleaned_data['monday_time'].isoformat()
                        x['end_time'] = time_delta(monday.cleaned_data['monday_time'], main_form.cleaned_data['training_duration']).isoformat()

        else:
            if monday.cleaned_data['monday_day'] != True:
                data[0]['begin_time'] = None
                data[0]['end_time'] = None


        if tuesday.is_valid() and tuesday.cleaned_data['tuesday_day'] and tuesday.cleaned_data['tuesday_time']:
            for x in data:
                if x['day'] == 2:
                    x['begin_time'] = tuesday.cleaned_data['tuesday_time'].isoformat()
                    x['end_time'] = time_delta(tuesday.cleaned_data['tuesday_time'],
                                               main_form.cleaned_data['training_duration']).isoformat()
        else:
            if tuesday.cleaned_data['tuesday_day'] != True:
                data[1]['begin_time'] = None
                data[1]['end_time'] = None


        if wednesday.is_valid() and wednesday.cleaned_data['wednesday_day'] and wednesday.cleaned_data['wednesday_time']:
            for x in data:
                if x['day'] == 3:
                    x['begin_time'] = wednesday.cleaned_data['wednesday_time'].isoformat()
                    x['end_time'] = time_delta(wednesday.cleaned_data['wednesday_time'], main_form.cleaned_data['training_duration']).isoformat()
        else:
            if wednesday.cleaned_data['wednesday_day'] != True:
                data[2]['begin_time'] = None
                data[2]['end_time'] = None



        if thursday.is_valid() and thursday.cleaned_data['thursday_day'] and thursday.cleaned_data['thursday_time']:
            for x in data:
                if x['day'] == 4:
                    x['begin_time'] = thursday.cleaned_data['thursday_time'].isoformat()
                    x['end_time'] = time_delta(thursday.cleaned_data['thursday_time'],
                                               main_form.cleaned_data['training_duration']).isoformat()
        else:
            if thursday.cleaned_data['thursday_day'] != True:
                data[3]['begin_time'] = None
                data[3]['end_time'] = None


        if friday.is_valid() and friday.cleaned_data['friday_day'] and friday.cleaned_data['friday_time']:
            for x in data:
                if x['day'] == 5:
                    x['begin_time'] = friday.cleaned_data['friday_time'].isoformat()
                    x['end_time'] = time_delta(friday.cleaned_data['friday_time'],
                                               main_form.cleaned_data['training_duration']).isoformat()
        else:
            if friday.cleaned_data['friday_day'] != True:
                data[4]['begin_time'] = None
                data[4]['end_time'] = None

        if saturday.is_valid() and saturday.cleaned_data['saturday_day'] and saturday.cleaned_data['saturday_time']:
            for x in data:
                if x['day'] == 6:
                    x['begin_time'] = saturday.cleaned_data['saturday_time'].isoformat()
                    x['end_time'] = time_delta(saturday.cleaned_data['saturday_time'],
                                               main_form.cleaned_data['training_duration']).isoformat()
        else:
            if saturday.cleaned_data['saturday_day'] != True:
                data[5]['begin_time'] = None
                data[5]['end_time'] = None

        if sunday.is_valid() and sunday.cleaned_data['sunday_day'] and sunday.cleaned_data['sunday_time']:
            for x in data:
                if x['day'] == 7:
                    x['begin_time'] = sunday.cleaned_data['sunday_time'].isoformat()
                    x['end_time'] = time_delta(sunday.cleaned_data['sunday_time'],
                                               main_form.cleaned_data['training_duration']).isoformat()
        else:
            if sunday.cleaned_data['sunday_day'] != True:
                data[6]['begin_time'] = None
                data[6]['end_time'] = None
        # print(main_form.instance.training_days)
        if main_form.instance.podushka_cost is None:
            main_form.instance.podushka_cost = 0
        main_form.save()
        return redirect('group_list')





def delete_group(request, id):
    if request.method == "POST":
        CrmGroup.objects.get(pk=id).delete()
        messages.add_message(request, messages.SUCCESS, 'Группа успешно удалена')
        return redirect('group_list')