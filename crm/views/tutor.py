from crm.views import BaseListView
from django.http import JsonResponse
from crm.models import Tutor
from crm.forms.tutor import TutorModelForm
from django.views.generic.edit import FormView
from django.views.generic import TemplateView
from django.contrib import messages
from django.shortcuts import redirect
from crm.mixins import LoginMixin


class TutorViewList(BaseListView):
    title = 'Tutors'
    create_name = 'Педагога'
    heading = 'Педагоги'
    template_name = 'tutor.html'
    create_form = TutorModelForm()
    create_url = 'tutor_create'

    def get_queryset(self):
        return Tutor.objects.all


class TutorCreateView(FormView):
    form_class = TutorModelForm

    def form_valid(self, form):
        form.save()
        return JsonResponse(data={}, status=200)

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({"error": dict(form.errors.items())}, status=400)


class TutorEditView(LoginMixin, TemplateView):
    template_name = 'crud/edit.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        tutor_id = kwargs['id']
        tutor = Tutor.objects.get(pk=tutor_id)
        form = TutorModelForm(instance=tutor)
        context['edit_form'] = form
        return context

    def post(self, request, *args, **kwargs):
        tutor = Tutor.objects.filter(id=kwargs.get('id')).prefetch_related('crmgroup_set').first()
        form = TutorModelForm(request.POST, instance=tutor)
        if form.is_valid():
            groups = request.POST.getlist('group')
            for group in groups:
                tutor.group.add(group)

            tutor.save()

            return redirect('tutor_list')


def delete_tutor(request, id):
    if request.method == "POST":
        Tutor.objects.get(pk=id).delete()
        messages.add_message(request, messages.SUCCESS, 'Преподователь успешно удален')
        return redirect('tutor_list')