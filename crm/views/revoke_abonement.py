from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import  status
from crm.models import Sale


class RevokeAbonementAjaxView(APIView):
    def post(self, request, abon_id):
        if not request.user.is_authenticated:
            return Response(data=dict(), status=status.HTTP_403_FORBIDDEN)

        sale = Sale.objects.get(pk=abon_id)
        sale.revoke_sale()
        return Response(data=dict(), status=status.HTTP_200_OK)
