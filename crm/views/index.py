from django.http import HttpResponse
from django.views.generic import TemplateView
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from crm.mixins import LoginMixin
from crm.models import Sale
import datetime
from django.db.models import Sum


class IndexView(LoginMixin, TemplateView):
    template_name = 'index.html'


class AjaxSalesToday(APIView):
    def get(self, request):
        self.is_auth(request)
        return self.response(self.process())

    def is_auth(self, request):
        if not request.user.is_authenticated:
            return Response(data=dict(), status=status.HTTP_403_FORBIDDEN)

    def get_dt(self):
        today = datetime.datetime.today()
        return today.year, today.month, today.day

    def process(self):
        dt = self.get_dt()
        sales = self.get_sale_by_date(dt[0], dt[1], dt[2])
        if sales['end_payment__sum'] is None:
            return 0
        return sales['end_payment__sum']

    def get_sale_by_date(self, year, month, day):
        sales = Sale.objects.filter(
            created__year=year,
            created__month=month,
            created__day=day,
        ).all().aggregate(Sum('end_payment'))
        return sales

    def response(self, count):
        return Response(data={'count': count})


class AjaxSaleYersterday(AjaxSalesToday):
    def get_dt(self):
        today = datetime.datetime.today()
        return today.year, today.month, today.day - 1


class AjaxSaleMonth(AjaxSalesToday):
    def get_today_dt(self):
        today = datetime.datetime.today()
        return "{}-{}-{}".format(today.year, today.month, today.day)

    def get_month_dt(self):
        today = datetime.datetime.today()
        month = today.month - 1
        if month == 0:
            month = 12

        return "{}-{}-{}".format(today.year, month, today.day)

    def process(self):
        today = self.get_today_dt()
        prev_month = self.get_month_dt()
        sales = self.get_sale_by_date(today, prev_month)
        if sales['end_payment__sum'] is None:
            return 0
        return sales['end_payment__sum']

    def get_sale_by_date(self, today, prev_month):
        sales = Sale.objects.filter(
            created__range=[prev_month, today]
        ).all().aggregate(Sum('end_payment'))
        return sales


