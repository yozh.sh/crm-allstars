from django.views.generic import TemplateView
from crm.mixins import LoginMixin
from crm.models import Client


class InformationView(LoginMixin, TemplateView):
    template_name = 'client/information/info/index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        client = Client.objects.get(pk=self.kwargs.get('client_id'))
        context['client'] = client
        return context
