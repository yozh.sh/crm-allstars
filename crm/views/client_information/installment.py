from django.views.generic import TemplateView, FormView
from crm.mixins import LoginMixin
from crm.models import Installment, InstallmentPayments, Client, CashBox
from crm.forms.client_info.installment import InstallmentPaymentsForm
from django.http import JsonResponse
from crm.views import BaseListView
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
import datetime
from crm.utils.dt import datetime_now


class InstallmentView(LoginMixin, TemplateView):
    template_name = 'client/information/installment/index.html'
    create_url = 'client_info_create_payment_by_installment'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object_list'] = Installment.objects.prefetch_related('sale').\
            filter(
            client_id=self.kwargs['client_id'],
            sale__revoke=False
        ).all()
        context['client_name'] = Client.objects.filter(id=self.kwargs['client_id']).\
            values('name').first()['name']
        context['create_form'] = InstallmentPaymentsForm(initial={
            'create_date': datetime_now()
        })
        context['create_url'] = self.create_url
        return context


class InstallmentPaymentsView(BaseListView):
    title = 'Payments'
    create_name = 'Платеж'
    heading = 'Платежи по рассрочке'
    # create_form = InstallmentPaymentsForm()
    create_url = 'client_info_create_payment_by_installment'
    # paginate_by = SHOW_COUNT
    template_name = 'client/information/installment/installment_list.html'

    def get_create_form(self):
        installment_id = self.kwargs.get('installment_id')
        form = InstallmentPaymentsForm(
            initial={
                'installment_id': installment_id,
                'create_date': datetime.datetime.now()
                }
            )
        return form


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        client_id = self.kwargs.get('client_id')
        installment_id = self.kwargs.get('installment_id')
        context['payments'] = self.get_queryset()
        context['client_id'] = client_id
        context['installment_id'] = installment_id
        return context
    
    def get_queryset(self):
        client_id = self.kwargs.get('client_id')
        installment_id = self.kwargs.get('installment_id')
        payments = InstallmentPayments.objects.filter(
            client_id=client_id,
            installment_id=installment_id
        ).all()
        return payments
        

class InstallmentPaymentsCreate(LoginMixin, FormView):
    form_class = InstallmentPaymentsForm
    
    def form_valid(self, form):
        instance: InstallmentPayments = form.save(commit=False)
        client = Client.objects.get(pk=self.kwargs['client_id'])
        installment = Installment.objects.get(
            pk=self.kwargs['installment_id']
            )
        instance.create_payment(client, installment)
        return JsonResponse(data={}, status=200)

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({"error": dict(form.errors.items())}, status=400)


class AjaxValidatePaymentMoney(APIView):
    def get(self, request, installment_id):
        if not request.user.is_authenticated:
            return Response(data=dict(), status=status.HTTP_403_FORBIDDEN)
        
        money = int(request.query_params.get('money', None))
        if money is None:
            return Response(data=dict(), status=status.HTTP_400_BAD_REQUEST)
        installment_instance: Installment = Installment.objects.get(pk=installment_id)
        if installment_instance.validate_payment_money(money):
            return Response(data=dict(), status=status.HTTP_200_OK)
        return Response(data=dict(), status=status.HTTP_406_NOT_ACCEPTABLE)


class AjaxForceClosePayment(APIView):
    def get(self, request, installment_id):
        if not request.user.is_authenticated:
            return Response(data=dict(), status=status.HTTP_403_FORBIDDEN)
        instance: Installment = Installment.objects.get(pk=installment_id)
        instance.force_close_installment()
        return Response(data=dict(), status=status.HTTP_200_OK)


class DeletePaymentAjax(APIView):
    def post(self, request):
        if not request.user.is_authenticated:
            return Response(data=dict(), status=status.HTTP_403_FORBIDDEN)

        installment_id = request.data.get('installment_id')
        payment_id = request.data.get('payment_id')
        cashbox = CashBox.objects.filter(installment_id=installment_id, operation_type='INSTALLMENT').first()
        cashbox.delete()
        InstallmentPayments.objects.filter(
            id=payment_id,
            installment_id=installment_id
        ).delete()
        return Response(data=dict(), status=status.HTTP_200_OK)
