from django.views.generic import ListView
from crm.mixins import LoginMixin
from crm.models import Sale, CashBox

class HistoryView(LoginMixin, ListView):
    template_name = 'client/information/history/list.html'

    def get_queryset(self):
        return CashBox.objects.prefetch_related('client').filter(client_id=self.kwargs.get('client_id'))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['client_id'] = self.kwargs.get('client_id')
        context['history'] = self.get_queryset()
        return context