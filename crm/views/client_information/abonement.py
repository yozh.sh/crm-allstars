from crm.forms.client_info.abonement import AbonementSaleClientInfoEdit, Sale
from crm.models import Sale, Abonement
from django.views.generic import ListView, TemplateView
from django.contrib import messages
from django.shortcuts import redirect, render
from crm.utils.constants.pagination import SHOW_COUNT
from crm.mixins import LoginMixin
import json
from django.core.serializers.json import DjangoJSONEncoder
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from crm.utils.dt import datetime_to_string, parse_date_from_input
from django.forms import ValidationError
from crm.forms.client_info.abonement import AbonementFreezeForm
from django.shortcuts import reverse
from crm.utils.dt import datetime_now
import datetime


class AbonementView(LoginMixin, ListView):
    template_name = 'client/information/abonement/list.html'
    paginate_by = SHOW_COUNT

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Client Abonements info'
        context['client_id'] = self.kwargs.get('client_id')
        context['freeze_form'] = AbonementFreezeForm(initial={
            'begin_date': datetime_now()
        })
        context['freeze_url'] = reverse('freeze_sale')
        return context

    def get_queryset(self, *args, **kwargs):
        return Sale.get_sales_prefetch(self.kwargs.get('client_id'))


class AbonementEditView(LoginMixin, TemplateView):
    template_name = 'client/information/abonement/edit.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        sale_id = kwargs.get('abon_id')
        sale = Sale.objects.filter(id=sale_id).first()
        form = AbonementSaleClientInfoEdit(instance=sale)
        context['edit_form'] = form
        vue_context = {
            'activation': sale.activation,
            'activation_date': datetime_to_string(sale.activation_date),
            'deactivation_date': datetime_to_string(sale.deactivation_date),
            'sale_id': sale.id,
        }
        context['vue_context'] = json.dumps(vue_context, cls=DjangoJSONEncoder)
        context['client_id'] = kwargs.get('client_id')
        context['abon_id'] = sale_id
        return context

    def post(self, request, *args, **kwargs):
        sale = Sale.objects.get(pk=kwargs.get('abon_id'))
        form = AbonementSaleClientInfoEdit(request.POST, instance=sale)
        if form.is_valid():
            form.save()
            next = request.POST.get('next', '/')
            return redirect(next)



class AjaxGetDeactivationDate(APIView):
    def get(self, request):
        if not request.user.is_authenticated:
            return Response(data=dict(), status=status.HTTP_403_FORBIDDEN)

        date = request.query_params.get('activation_date')
        sale_id = request.query_params.get('sale_id')
        sale = Sale.objects.get(pk=int(sale_id))

        try:
            parsed_date = self._parse_date(date)
        except ValueError:
            return Response(data=dict(error="Неправильный формат даты"),
                            status=status.HTTP_400_BAD_REQUEST)
        try:
            deactivation_date = sale.get_deactivation_date(parsed_date)
        except ValidationError:
            return Response(data=dict(error="Неправильный формат даты"), status=status.HTTP_400_BAD_REQUEST)
        return Response(data=deactivation_date)

    def _parse_date(self, date: str):
        splitted = date.split(".")
        day, month, year = splitted
        return datetime.datetime(
            year=int(year),
            month=int(month),
            day=int(day)
        )