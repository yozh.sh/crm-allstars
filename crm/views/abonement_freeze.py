from rest_framework.views import APIView
from crm.mixins import LoginMixin
from crm.models import Sale
from crm.forms.client_info.abonement import AbonementFreezeForm
from django.views.generic import FormView
from django.http.response import JsonResponse


class AbonementFreeze(LoginMixin, FormView):
    form_class = AbonementFreezeForm

    def form_valid(self, form):
        sale_id = form.cleaned_data['abonement_id']
        freeze_day = int(form.cleaned_data['freeze_day_count'])
        begin_date = form.cleaned_data['begin_date']
        sale = Sale.objects.get(pk=sale_id)
        sale.run_freeze(days=freeze_day, begin_date=begin_date)
        sale.run_unfreeze(days=freeze_day, freeze_date=begin_date)
        return JsonResponse(data={}, status=200)


    def form_invalid(self, form):
        pass

    # def post(self, request, *args, **kwargs):
    #     sale = Sale.objects.get(pk=request.data.get('abonement_id'))
    #     form = AbonementFreezeForm(request.data['form_data'])
    #     if form.is_valid():
    #         print("VALID!!!")
    #     print(form.errors)
