from django.test import TestCase
from crm.models import Sale
from crm.models import Client, Abonement, Promotion, Tutor, PromotionToSale, Departament, Profile
from django.contrib.auth.models import User
import datetime
import random
import string
from _fixtures.group import GroupFixture
from _fixtures.client import ClientFixture
from _fixtures.abonement import AbonementFixture
from _fixtures.promo import PromotionFixture
from _fixtures.tutor import TutorFixture
from _fixtures.user_profile import ProfileFixture, ProfileNameMock
from _fixtures.departament import DepartamentFixture, DepartamentNameMock

class SaleFixture(TestCase):
    def setUpSale(self, client, tutor,
                  group, user, departament, abonement, promo,
                  payment, abonement_price):
        sale = Sale(
            saled_by=user.profile,
            saled_by_departament=departament,
            client=client,
            abonement_name='Test Abonement',
            abonement_available_visits_number=40,
            start_visits_number=40,
            current_visits_number=40,
            abonement_duration=168,
            abonement_group_count='ONE',
            abonement=abonement,
            activation=True,
            activation_date=datetime.datetime(year=2020, month=1, day=3),
            deactivation_date=datetime.datetime(year=2020, month=6,
                                                day=19),
            promotion=promo if promo else None,
            installment_plan=False,
            saled_by_installment=False,
            abonement_price=abonement_price,
            revoke=False,
            payment_type='CASH',
            end_payment=payment

        )

        sale.save()
        sale.tutor.add(tutor)
        sale.group.add(group)
        sale.save()
        return sale


class SaleCase(GroupFixture, TestCase):
    DEPARTAMENT_1 = 'Test Departament 1'
    ABON_PRICE = 3000
    PAYMENT = 1000

    def setUpTutor(self):
        return Tutor.objects.create(
            first_name='Тестовый',
            last_name='Преподователь'
        )

    def setUpPromo(self, discount_type=None):
        if discount_type == 'money':
            d_type = 'MONEY'
            discount = 100  # UAH
        elif discount_type == 'percent':
            d_type = 'PROCENT'
            discount = 10  # %
        else:
            raise Exception(
                'You need provide one of two values, money or percent')

        promo = Promotion(
            name='Test Promotion',
            type_of_discount=d_type,
            discount=discount,
            temporary=False,
            active=True
        )
        promo.save()
        sale_promo = PromotionToSale(
            name=promo.name,
            type_of_discount=promo.type_of_discount,
            discount=promo.discount,
            temporary=promo.temporary,
            active=promo.active

        )
        sale_promo.save()
        sale_promo.template_promotion = promo
        sale_promo.save()
        return sale_promo

    def setUpClient(self):
        client = Client(
            name='Тест',
            surname='Тестович',
            middle_name='Джангович',
            sex=1,
            birthday=datetime.datetime.now(),
            phone_number='+380988962515'
        )
        client.save()
        return client

    def setUpAbonement(self):
        return Abonement.objects.create(
            name='Тестовый Абонемент',
            available_visits_number=48,
            duration=168,
            group_count='ONE',
            number_of_freezing=14,
            cost=3100
        )

    def setUpUserDepartament(self, name: str):
        exist_departament = Departament.objects.filter(name=name).first()
        if not exist_departament:
            self.departament = Departament.objects.create(name=name)
        else:
            self.departament = exist_departament

    def setUpUserProfile(self, departament_obj):
        self.user = User.objects.create(username='test_user', password='1234')
        self.user.profile.departament = departament_obj
        self.user.save()

    def setUpSale(self, activation=True, promotion=True,
                  group_name='Test Group', installment_plan=False):
        client = self.setUpClient()
        self.client = client
        abonement = self.setUpAbonement()
        if promotion:
            promo = self.setUpPromo(discount_type='money')
        else:
            promo = None
        group = self.setUpGroup(group_name)
        tutor = self.setUpTutor()
        sale = Sale(
            saled_by=self.setUpUserProfile(departament_obj=self.setUpUserDepartament("testDepartament")),
            saled_by_departament=self.user.profile.departament.id,
            client=client,
            abonement_name=abonement.name,
            abonement_available_visits_number=40,
            start_visits_number=40,
            current_visits_number=40,
            abonement_duration=168,
            abonement_group_count='ONE',
            abonement=abonement,
            activation=activation,
            activation_date=datetime.date(year=2020, month=1, day=3),
            deactivation_date=datetime.date(year=2020, month=6,
                                            day=19),
            promotion=promo if promo else None,
            installment_plan=installment_plan,
            saled_by_installment=installment_plan,
            abonement_price=self.ABON_PRICE,
            revoke=False,
            payment_type='CASH',
            end_payment=self.PAYMENT

        )

        sale.save()
        sale.tutor.add(tutor)
        sale.group.add(group)
        sale.save()
        return sale


class SaleFixtureCase(GroupFixture, ClientFixture, AbonementFixture, PromotionFixture, TutorFixture, ProfileFixture, DepartamentFixture, TestCase):
    activation_date=datetime.datetime.today()
    deactivation_date=datetime.datetime.today() + datetime.timedelta(days=1)
    payment_type = 'CASH'
    group_name = 'TEST GROUP NAME'

    def setUpSale(self, activation=True, promotion=True,
                  group_name=group_name, installment_plan=True,
                  discount_type=None, promo_temporary=False, promo_active=False):

        self.departament = self.setUpDepartament()
        self.user = self.setUpUser(self.departament)
        self.client = self.setUpClient()
        self.abonement = self.setUpAbonement()
        self.promotion = None
        if promotion:
            self.promotion = self.setUpPromo(
                discount_type=discount_type,
                temporary=promo_temporary,
                active=promo_active
            )
        self.group = self.setUpGroup(self.group_name)
        self.tutor = self.setUpTutor()

        self.sale = Sale(
            saled_by=self.user.profile,
            saled_by_departament=self.departament,
            client=self.client,
            abonement_name=self.abonement.name,
            abonement_available_visits_number=self.abonement.available_visits_number,
            start_visits_number=self.abonement.available_visits_number,
            current_visits_number=self.abonement.available_visits_number,
            abonement_duration=self.abonement.duration,
            abonement_group_count=self.abonement.group_count,
            abonement=self.abonement,
            activation_date=self.activation_date,
            deactivation_date=self.deactivation_date,
            promotion=self.promotion,
            installment_plan=installment_plan,
            saled_by_installment=installment_plan,
            abonement_price=self.abonement.cost,
            revoke=False,
            payment_type=self.payment_type,
            end_payment=self.abonement.cost
        )
        self.sale.save()
        self.sale.tutor.add(self.tutor)
        self.sale.group.add(self.group)
        self.sale.save()
        return self.sale


    def setUpSaleAnother(self, group_name, activation=True, promotion=True,
                         installment_plan=True,
                         discount_type=None, promo_temporary=False, promo_active=False):

        self.departament = self.setUpDepartament()
        self.user = self.setUpUser(self.departament)
        self.client = self.setUpClient()
        self.abonement = self.setUpAbonement()
        self.promotion = None
        if promotion:
            self.promotion = self.setUpPromo(
                discount_type=discount_type,
                temporary=promo_temporary,
                active=promo_active
            )
        self.group = self.setUpGroup(group_name)
        self.tutor = self.setUpTutor()

        self.sale = Sale(
            saled_by=self.user.profile,
            saled_by_departament=self.departament,
            client=self.client,
            abonement_name=self.abonement.name,
            abonement_available_visits_number=self.abonement.available_visits_number,
            start_visits_number=self.abonement.available_visits_number,
            current_visits_number=self.abonement.available_visits_number,
            abonement_duration=self.abonement.duration,
            abonement_group_count=self.abonement.group_count,
            abonement=self.abonement,
            activation_date=self.activation_date,
            deactivation_date=self.deactivation_date,
            promotion=self.promotion,
            installment_plan=installment_plan,
            saled_by_installment=installment_plan,
            abonement_price=self.abonement.cost,
            revoke=False,
            payment_type=self.payment_type,
            end_payment=self.abonement.cost
        )
        self.sale.save()
        self.sale.tutor.add(self.tutor)
        self.sale.group.add(self.group)
        self.sale.save()
        return self.sale





class SaleWithDepartamentCase(SaleCase):
    DEPARTAMENT_1 = 'Test Departament 1'

    def randomString(self, stringLength=10):
        """Generate a random string of fixed length """
        letters = string.ascii_lowercase
        return ''.join(random.choice(letters) for i in range(stringLength))

    def setUpAbonement(self, cost):
        return Abonement.objects.create(
            name=self.randomString(),
            available_visits_number=48,
            duration=168,
            group_count='ONE',
            number_of_freezing=14,
            cost=cost
        )



    def setUpSale(self, activation=True, promotion=True,
                  group_name='Test Group', installment_plan=False, departament_name=None, abon_cost: int = 0):

        client = self.setUpClient()
        self.setUpUserDepartament(departament_name)
        self.setUpUserProfile(self.departament)

        self.client = client
        abonement = self.setUpAbonement(abon_cost)
        if promotion:
            promo = self.setUpPromo(discount_type='money')
        else:
            promo = None

        group = self.setUpGroup(group_name)
        tutor = self.setUpTutor()
        sale = Sale(
            saled_by=self.user.profile,
            saled_by_departament=self.user.profile.departament,
            client=client,
            abonement_name=abonement.name,
            abonement_available_visits_number=40,
            start_visits_number=40,
            current_visits_number=40,
            abonement_duration=168,
            abonement_group_count='ONE',
            abonement=abonement,
            activation=activation,
            activation_date=datetime.date(year=2020, month=1, day=3),
            deactivation_date=datetime.date(year=2020, month=6,
                                            day=19),
            promotion=promo if promo else None,
            installment_plan=installment_plan,
            saled_by_installment=installment_plan,
            abonement_price=abonement.cost,
            revoke=False,
            payment_type='CASH',
            end_payment=self.PAYMENT

        )

        sale.save()
        sale.tutor.add(tutor)
        sale.group.add(group)
        sale.save()
        sale.save_cashbox()
        return sale

    def createNewSaleWithSameDepartament(self, abon_cost: int):

        abonement = self.setUpAbonement(abon_cost)

        sale = Sale(
            saled_by=self.user.profile,
            saled_by_departament=self.user.profile.departament,
            client=self.client,
            abonement_name=abonement.name,
            abonement_available_visits_number=40,
            start_visits_number=40,
            current_visits_number=40,
            abonement_duration=168,
            abonement_group_count='ONE',
            abonement=abonement,
            activation=True,
            activation_date=datetime.date(year=2020, month=1, day=3),
            deactivation_date=datetime.date(year=2020, month=6,
                                            day=19),
            promotion=None,
            installment_plan=False,
            saled_by_installment=False,
            abonement_price=abonement.cost,
            revoke=False,
            payment_type='CASH',
            end_payment=self.PAYMENT

        )
        sale.save()
        sale.tutor.add(self.setUpTutor())
        sale.group.add(self.setUpGroup('Test Group 1'))
        sale.save()
        sale.save_cashbox()
        return sale

class SaleFixtureCaseWithRandomDepartament(SaleFixtureCase, DepartamentNameMock, ProfileNameMock):
    activation_date=datetime.datetime.today()
    deactivation_date=datetime.datetime.today() + datetime.timedelta(days=1)
    payment_type = 'CASH'
    group_name = 'TEST GROUP NAME'

    def setUpSale(self, activation=True, promotion=True,
                  group_name=group_name, installment_plan=True,
                  discount_type=None, promo_temporary=False, promo_active=False):

        self.departament = self.setUpDepartamentMock()
        self.user = self.setUpUserMock(self.departament)
        self.client = self.setUpClient()
        self.abonement = self.setUpAbonement()
        self.promotion = None
        if promotion:
            self.promotion = self.setUpPromo(
                discount_type=discount_type,
                temporary=promo_temporary,
                active=promo_active
            )
        self.group = self.setUpGroup(self.group_name)
        self.tutor = self.setUpTutor()

        self.sale = Sale(
            saled_by=self.user.profile,
            saled_by_departament=self.departament,
            client=self.client,
            abonement_name=self.abonement.name,
            abonement_available_visits_number=self.abonement.available_visits_number,
            start_visits_number=self.abonement.available_visits_number,
            current_visits_number=self.abonement.available_visits_number,
            abonement_duration=self.abonement.duration,
            abonement_group_count=self.abonement.group_count,
            abonement=self.abonement,
            activation=activation,
            activation_date=self.activation_date,
            deactivation_date=self.deactivation_date,
            promotion=self.promotion,
            installment_plan=installment_plan,
            saled_by_installment=installment_plan,
            abonement_price=self.abonement.cost,
            revoke=False,
            payment_type=self.payment_type,
            end_payment=self.abonement.cost
        )
        self.sale.save()
        self.sale.tutor.add(self.tutor)
        self.sale.group.add(self.group)
        self.sale.save()
        return self.sale