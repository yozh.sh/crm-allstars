from django.test import TestCase
from crm.models import Client
import datetime


class ClientFixture(TestCase):
    def setUpClient(self):
        return Client.objects.create(
            name='Test',
            surname='Tester',
            middle_name='Testerovich',
            sex=1,
            birthday=datetime.date(
                year=1995,
                month=11,
                day=12
            ),
            phone_number='+380990503443'
        )