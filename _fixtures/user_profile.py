from django.test import TestCase
from django.contrib.auth.models import User
import uuid


class ProfileFixture(TestCase):
    USERNAME, PASSWORD = 'TESTING_USER', 'qwerty'

    def setUpUser(self, departament_obj):
        user = User.objects.create(
            username=self.USERNAME,
            password=self.PASSWORD
        )
        user.profile.departament = departament_obj
        user.save()
        return user


class ProfileNameMock(TestCase):

    def setUpUserMock(self, departament_obj):
        user = User.objects.create(
            username=str(uuid.uuid4()),
            password=str(uuid.uuid4())
        )
        user.profile.departament = departament_obj
        user.save()
        return user