from django.test import TestCase
from crm.models import Promotion, PromotionToSale


class PromotionFixture(TestCase):
    PROMO_NAME = 'TEST PROMOTION'
    MONEY = {
        'type': 'MONEY',
        'discount': 100
    }
    PERCENT = {
        'type': 'PROCENT',
        'discount': 10,
    }

    def setUpPromo(self, discount_type=None, temporary=False, active=True):
        if not discount_type:
            raise Exception("Please provide one of discount_type - <MONEY>, <PERCENT>")

        promotion = Promotion(
            name=self.PROMO_NAME
        )

        if discount_type == self.MONEY['type']:
            promotion.type_of_discount=self.MONEY['type']
            promotion.discount = self.MONEY['discount']

        elif discount_type == self.PERCENT['type']:
            promotion.type_of_discount = self.PERCENT['type']
            promotion.discount = self.PERCENT['discount']

        promotion.temporary = temporary
        promotion.active = active
        promotion.save()

        # Копирование акции в таблицу PromotionToSale
        sale_promotion = PromotionToSale.objects.create(
            name=promotion.name,
            type_of_discount=promotion.type_of_discount,
            discount=promotion.discount,
            temporary=promotion.temporary,
            active=promotion.active
        )
        sale_promotion.template_promotion = promotion
        sale_promotion.save(update_fields=['template_promotion'])
        return sale_promotion


