from django.test import TestCase
from crm.models import Tutor

class TutorFixture(TestCase):
    def setUpTutor(self):
        return Tutor.objects.create(
            first_name='Тестовый',
            last_name='Преподователь'
        )