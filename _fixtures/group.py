from django.test import TestCase
from crm.models import CrmGroup
import datetime


class GroupFixture(TestCase):
    def setUpGroup(self, group_name='Test Group'):
        # MONDAY
        begin_time_m = datetime.time(
            hour=17,
            minute=0
        ).isoformat()
        end_time_m = datetime.time(
            hour=17,
            minute=30
        ).isoformat()

        # FRIDAY
        begin_time_f = datetime.time(
            hour=16,
            minute=0
        ).isoformat()
        end_time_f = datetime.time(
            hour=16,
            minute=30
        ).isoformat()
        data = [
            {"day": 1, "begin_time": begin_time_m, "end_time": end_time_m},
            {"day": 2, "begin_time": None, "end_time": None},
            {"day": 3, "begin_time": None, "end_time": None},
            {"day": 4, "begin_time": None, "end_time": None},
            {"day": 5, "begin_time": begin_time_f, "end_time": end_time_f},
            {"day": 6, "begin_time": None, "end_time": None},
            {"day": 7, "begin_time": None, "end_time": None},
        ]
        return CrmGroup.objects.create(
            name=group_name,
            rate=10,
            is_podushka=False,
            training_days=data,
            training_duration=30,
            one_visit_price=100,
        )
