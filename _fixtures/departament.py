from django.test import TestCase
from crm.models import Departament
from psycopg2 import IntegrityError
import uuid


class DepartamentFixture(TestCase):
    NAME = 'TEST DEPARTAMENT'

    def setUpDepartament(self):
        return Departament.objects.create(
            name=self.NAME
        )


class DepartamentNameMock(TestCase):
    # Необходимо когда имя департамента не важно но необходимо иметь уникальные обьекты департамента
    def setUpDepartamentMock(self):
        return Departament.objects.create(
            name=str(uuid.uuid4())
        )