from django.test import TestCase
from crm.models import Installment
import datetime


class InstallmentFixture(TestCase):
    def setUpInstallment(self, client, sale, promo):
        installment = Installment.objects.create(
            client=client,
            sale=sale,
            debt=promo.get_discounted(sale.abonement_price) if promo else sale.abonement_price
        )
        installment.save()
        return installment

    def installment_create_payment(self, money: int):
        self.installment.new_payment(money)

