from django.test import TestCase
from crm.models import Abonement


class AbonementFixture(TestCase):
    def setUpAbonement(self):
        return Abonement.objects.create(
            name='TEST_ABON',
            available_visits_number=40,
            duration=20,
            group_count='ONE',
            number_of_freezing=5,
            cost=2000
        )