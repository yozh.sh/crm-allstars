---
name: Bug report
about: Create a report to help us improve
title: ''
labels: bug
assignees: ''

---

**Описание бага**
A clear and concise description of what the bug is.

**Шаги для воспроизведения бага**
Steps to reproduce the behavior:
1. Go to '...'
2. Click on '....'
3. Scroll down to '....'
4. See error

**Ожидаемое поведение/значение**
A clear and concise description of what you expected to happen.

**Фактическое поведение/значение**
A clear and concise description of what you expected to happen.

**Screenshots**
If applicable, add screenshots to help explain your problem.

**Дополнительная информация**
Add any other context about the problem here.
