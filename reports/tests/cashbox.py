from _fixtures.sale import SaleWithDepartamentCase
from reports.models import CashboxReport
from reports.tasks import cashbox_make_report
from tests.utils.strategies.test_period_strategy import TestDayPeriod
from reports.models import CashboxReport
from crm.models import CashBox


class TestCashboxReport(SaleWithDepartamentCase):
    TEST_DEPARTAMENT_1 = 'Test departament1'
    TEST_DEPARTAMENT_2 = 'Test departament2'

    TEST_DEPARTAMENT_1_TOTAL_COST = 4000
    TEST_DEPARTAMENT_2_TOTAL_COST = 6000


    def setUp(self) -> None:
        # SET UP SALE FOR 1 DEPARTAMENT
        self.sale = self.setUpSale(departament_name=self.TEST_DEPARTAMENT_1, abon_cost=2000)
        self.new_sale = self.createNewSaleWithSameDepartament(abon_cost=2000)

    def test_data_in_cashbox_exist(self):
        cashbox_data = CashBox.objects.all()
        self.assertEqual(len(cashbox_data), 2)

    def test_data_in_same_departament_exist(self):
        cashbox_data = CashBox.objects.filter(abonement__saled_by__departament_id=self.sale.saled_by_departament_id).all()
        self.assertEqual(len(cashbox_data), 2)

    def test_total_price_one_departament_sale(self):
        today_period = TestDayPeriod()
        cashbox_make_report(
            date_to=today_period._date_to(),
            date_from=today_period._date_from(),
            user_id=self.user.id,
            period_info=today_period.get_strategy().get_period_info(),
            departament_id=self.user.profile.departament.id
        )
        report_data = CashboxReport.objects.first()  # because was created one report

        self.assertEqual(report_data.total, self.TEST_DEPARTAMENT_1_TOTAL_COST)
