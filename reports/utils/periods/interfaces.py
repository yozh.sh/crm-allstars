import abc
import typing
import datetime


class IDateStrategy(metaclass=abc.ABCMeta):
    def get_period_info(self) -> str:
        pass

    def get_dates(self) -> typing.Tuple[datetime.datetime, datetime.datetime]:
        pass
