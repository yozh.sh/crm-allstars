from reports.utils.periods.interfaces import IDateStrategy


class PeriodContext(object):
    strategy = None

    def set_strategy(self, strategy: IDateStrategy):
        self.strategy = strategy

    def exec_strategy(self):
        return self.strategy.get_dates()

    def get_period(self):
        if self.strategy:
            return self.strategy.get_period_info()
        raise NotImplementedError('Before call get_period you must set strategy')
