from reports.utils.periods.interfaces import IDateStrategy
import datetime
import typing

# This a just proxy for custom period, it need for right strategy implementation

class CustomPeriod(IDateStrategy):
    def get_period_info(self) -> str:
        return 'CUSTOM'

    def __init__(self, date_to: datetime.datetime, date_from: datetime.datetime):
        self.date_to = date_to
        self.date_from = date_from

    def get_dates(self) -> typing.Tuple[datetime.datetime, datetime.datetime]:
        return self.date_to, self.date_from
