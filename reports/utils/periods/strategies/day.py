from reports.utils.periods.interfaces import IDateStrategy
import typing
import datetime


class DayDate(IDateStrategy):
    def __init__(self, context=None):
        self.context = context

    def get_period_info(self) -> str:
        return 'DAY'

    def get_dates(self) -> typing.Tuple[datetime.datetime, datetime.datetime]:
        if self.context:
            date = self.context
        else:
            date = datetime.datetime.now()

        date_to = datetime.datetime(
            year=date.year,
            month=date.month,
            day=date.day,
            hour=0,
            minute=0,
            second=1
        )

        date_from = datetime.datetime(
            year=date.year,
            month=date.month,
            day=date.day,
            hour=23,
            minute=59,
        )
        return date_to, date_from
