from reports.utils.periods.interfaces import IDateStrategy
import typing
import datetime
from calendar import monthrange
from reports.utils.prev_month import get_prev_month


class PreviousMonth(IDateStrategy):
    def get_period_info(self) -> str:
        return 'PREV_MONTH'

    def get_dates(self) -> typing.Tuple[datetime.datetime, datetime.datetime]:
        current_date = datetime.datetime.now()
        prev_month = get_prev_month(current_date)
        _, last_month_day = monthrange(prev_month.year, prev_month.month)

        date_to = datetime.datetime(
            year=prev_month.year,
            month=prev_month.month,
            day=1,
            hour=0,
            minute=0,
            second=1
        )
        date_from = datetime.datetime(
            year=prev_month.year,
            month=prev_month.month,
            day=last_month_day,
            hour=23,
            minute=59,
        )
        return date_to, date_from


class CurrentMonth(IDateStrategy):
    def get_period_info(self) -> str:
        return 'CURRENT_MONTH'

    def get_dates(self) -> typing.Tuple[datetime.datetime, datetime.datetime]:
        now = datetime.datetime.now()
        first_month_day, last_month_day = monthrange(now.year,
                                                     now.month)

        date_to = datetime.datetime(
            year=now.year,
            month=now.month,
            day=1,
            hour=0,
            minute=0,
            second=1
        )

        date_from = datetime.datetime(
            year=now.year,
            month=now.month,
            day=last_month_day,
            hour=23,
            minute=59,
        )
        return date_to, date_from


