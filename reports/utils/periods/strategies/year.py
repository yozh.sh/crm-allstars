from reports.utils.periods.interfaces import IDateStrategy
import typing
import datetime
from calendar import monthrange


class PreviousYear(IDateStrategy):
    def get_period_info(self) -> str:
        return 'PREV_YEAR'

    def get_dates(self) -> typing.Tuple[datetime.datetime, datetime.datetime]:
        now = datetime.datetime.now()
        prev_year_val = now.year - 1
        first_day_prev_year, _ = monthrange(prev_year_val, 1)
        _, last_day_prev_year = monthrange(prev_year_val, 12)

        date_to = datetime.datetime(
            year=prev_year_val,
            month=1,
            day=first_day_prev_year,
            hour=0,
            minute=0,
            second=1
        )

        date_from = datetime.datetime(
            year=prev_year_val,
            month=12,
            day=last_day_prev_year,
            hour=23,
            minute=59
        )
        return date_to, date_from


class CurrentYear(IDateStrategy):
    def get_period_info(self) -> str:
        return 'CURRENT_YEAR'

    def get_dates(self) -> typing.Tuple[datetime.datetime, datetime.datetime]:
        now = datetime.datetime.now()
        first_day_year, _ = monthrange(now.year, 1)

        date_to = datetime.datetime(
            year=now.year,
            month=1,
            day=1,
            hour=0,
            minute=0,
            second=1
        )

        date_from = datetime.datetime(
            year=now.year,
            month=now.month,
            day=now.day - 1,
            hour=0,
            minute=0,
            second=1
        )

        return date_to, date_from