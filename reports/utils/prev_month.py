import datetime


def get_prev_month(currentMonth: datetime.datetime) -> datetime.datetime:
    if isinstance(currentMonth, datetime.datetime):
        first = currentMonth.replace(day=1)
        prev_month = first - datetime.timedelta(days=1)
        return prev_month
    raise TypeError("function argument should be datetime instance not {}".format(currentMonth))
