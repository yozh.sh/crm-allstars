from django.contrib import admin
from reports.models import CashboxReport, PromoDiscountReport, ClientDebtReport, IncomingMoneyReport,\
    ActiveClientsReport, SalesByGroupsReport, SalesToGroupReport
# Register your models here.

admin.site.register(CashboxReport)
admin.site.register(IncomingMoneyReport)
admin.site.register(PromoDiscountReport)
admin.site.register(ActiveClientsReport)
admin.site.register(ClientDebtReport)
admin.site.register(SalesByGroupsReport)
admin.site.register(SalesToGroupReport)

