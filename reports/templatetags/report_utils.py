from django import template


register = template.Library()


@register.filter('display_departament')
def get_display_departament(departament):
    if not departament:
        return 'Все'
    return departament


@register.filter('display_period')
def get_display_period(period):
    if not period:
        return 'Произвольный'
    return period