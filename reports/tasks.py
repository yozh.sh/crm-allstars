from __future__ import absolute_import, unicode_literals
from celery import shared_task
from crm.models import Sale, CashBox, PromotionToSale, Installment, Abonement
from django.db.models import Sum
import pandas as pd
from notifications.utils.report import make_report_notification
from django.utils.timezone import datetime
from .models import CashboxReport
from .models import IncomingMoneyReport, IncomingMoneyData, PromoDiscountReportDataPromo, PromoDiscountReport, PromoDiscountReportDataSale
from .models import ActiveClientsReport, ActiveClientsReportData
from .models import ClientDebtReportData, ClientDebtReport
from .models import SalesByGroupsReport, SalesByGroupsReportData
from .models import SalesToGroupReport, SalesToGroupReportData
from django.contrib.auth.models import User
from datetime import time
from django.shortcuts import reverse



@shared_task
def cashbox_make_report(date_to, date_from, user_id, period_info, departament_id=None):
    user = User.objects.get(pk=user_id)
    total_sum = None
    if departament_id:
        cash = CashBox.objects.filter(
            abonement__saled_by__departament_id=departament_id,
            abonement__payment_type='CASH',
            create_date__range=(date_to, date_from),
        ).all().aggregate(
            sum=Sum('price')
        )

        terminal = CashBox.objects.filter(
            abonement__saled_by__departament_id=departament_id,
            abonement__payment_type='CARD',
            create_date__range=(date_to, date_from),
        ).all().aggregate(
            sum=Sum('price')
        )

        if cash['sum'] is None:
            cash['sum'] = 0

        if terminal['sum'] is None:
            terminal['sum'] = 0

        total_sum = cash['sum'] + terminal['sum']

        CashboxReport.objects.create(
            departament_id=departament_id,
            cash=cash['sum'],
            terminal=terminal['sum'],
            total=total_sum,
            user=user.profile,
            date_to=date_to,
            date_from=date_from,
            period=period_info
        )
        make_report_notification(user.id, 'Касса')
    else:
        cash = CashBox.objects.filter(
            abonement__payment_type='CASH',
            create_date__range=(date_to, date_from),
        ).all().aggregate(
            sum=Sum('price')
        )

        terminal = CashBox.objects.filter(
            abonement__payment_type='CARD',
            create_date__range=(date_to, date_from),
        ).all().aggregate(
            sum=Sum('price')
        )

        try:
            total_sum = cash['sum'] + terminal['sum']
        except TypeError:
            if cash['sum'] is not None and terminal['sum'] is None:
                total_sum = cash['sum']
            elif terminal['sum'] is not None and cash['sum'] is None:
                total_sum = terminal['sum']

        CashboxReport.objects.create(
            cash=cash['sum'],
            terminal=terminal['sum'],
            total=total_sum,
            user=user.profile,
            date_to=date_to,
            date_from=date_from,
            period=period_info
        )
        make_report_notification(user.id, "Касса")



@shared_task
def incoming_money_make_report(date_to, date_from, user_id, period_info, departament_id=None):
    user = User.objects.get(pk=user_id)
    dates_list = pd.date_range(start=date_to, end=date_from).tolist()
    report = IncomingMoneyReport(
        departament_id=departament_id,
        user=user.profile,
        date_to=date_to,
        date_from=date_from,
        period=period_info
    )
    report.save()

    if departament_id:
        for date in dates_list:
            day_min = datetime.combine(date, time.min)  # get min time of current date EXAMPLE: 10.12.2012T00:00:01
            day_max = datetime.combine(date, time.max)  # get max time of current date EXAMPLE: 10.12.2012T23:59:59
            money_per_date = CashBox.objects.filter(
                abonement__saled_by__departament_id=departament_id,
                create_date__range=(day_min, day_max)
            ).all().aggregate(
                sum=Sum('price')
            )

            IncomingMoneyData.objects.create(
                date=date,
                money=money_per_date['sum'],
                report=report
            )
    else:
        for date in dates_list:
            day_min = datetime.combine(date, time.min)  # get min time of current date EXAMPLE: 10.12.2012T00:00:01
            day_max = datetime.combine(date, time.max)  # get max time of current date EXAMPLE: 10.12.2012T23:59:59
            money_per_date = CashBox.objects.filter(
                create_date__range=(day_min, day_max)
            ).all().aggregate(
                sum=Sum('price')
            )
            IncomingMoneyData.objects.create(
                date=date,
                money=money_per_date['sum'],
                report=report
            )

    make_report_notification(user.id, "Приход Денег")



@shared_task
def promotion_discount_make_report(date_to, date_from, user_id, period_info, departament_id=None):
    user = User.objects.get(pk=user_id)
    promotions = PromotionToSale.objects.distinct('name')
    cashbox_q = CashBox.objects.select_related('client', 'abonement', 'promotion').filter(operation_type='SALE')
    report = PromoDiscountReport(   # создаем обьект отчета
        departament_id=departament_id,
        user=user.profile,
        date_to=date_to,
        date_from=date_from,
        period=period_info
    )
    report.save()


    if departament_id:
        cashbox_q = cashbox_q.filter(abonement__saled_by_departament_id=departament_id)

    promo_to_sale_cnt = {promo: cashbox_q.filter(promotion__name=promo.name) for promo in promotions}

    for promo in promo_to_sale_cnt:
        promo_report_obj = PromoDiscountReportDataPromo(
            report=report,
            promo_discount_name=promo.name
        )
        promo_report_obj.save()

        for sale in promo_to_sale_cnt[promo]:
            promo_sale_data = PromoDiscountReportDataSale(
                date_of_sale=sale.create_date,
                client_fullname=sale.client.fullname(),
                client_profile_url=reverse('client_info_information', args=[sale.client.id]),
                price_without_discount=sale.abonement.abonement_price,
                price_with_discount=sale.abonement.full_price(),
                type_of_discount=sale.promotion.type_of_discount,
                discount=sale.promotion.discount,
                promotion_data=promo_report_obj
            )

            promo_sale_data.save()
            promo_sale_data.groups.add(*list(sale.group.all()))

    make_report_notification(user.id, "Скидок и Акций")


@shared_task
def make_report_active_clients(date_to, date_from, user_id, period_info, departament_id=None):
    user = User.objects.get(pk=user_id)
    sales_q = Sale.objects.prefetch_related('client', 'group')
    if departament_id:
        sales_q = sales_q.filter(saled_by_departament_id=departament_id)

    active_sales = sales_q.filter(activation_date__gte=date_to.date()).all()
    report = ActiveClientsReport.objects.create(
        departament_id=departament_id,
        user=user.profile,
        date_to=date_to,
        date_from=date_from,
        period=period_info
    )

    for sale in active_sales:
        report_data = ActiveClientsReportData.objects.create(
                client_fullname=sale.client.get_fio(),
                source_of_attraction=sale.client.source_of_attraction,
                abonement_name=sale.abonement_name,
                date_of_sale=sale.created.date(),
                date_of_start_abonement=sale.activation_date,
                date_of_finish_abonement=sale.deactivation_date,
                abonement_price=sale.abonement_price,
                report=report
            )
        report_data.groups.add(*list(sale.group.all()))

    make_report_notification(user.id, "Клиенты с активными картами")


@shared_task
def make_report_clients_debt(date_to, date_from, user_id, period_info, departament_id=None):
    user = User.objects.get(pk=user_id)
    sales_q = Sale.objects.select_related('client')
    if departament_id:
        sales_q = sales_q.filter(saled_by_departament_id=departament_id)

    report = ClientDebtReport.objects.create(
        departament_id=departament_id,
        user=user.profile,
        date_to=date_to,
        date_from=date_from,
        period=period_info
    )

    for sale in sales_q.filter(installment_plan=True):
        installment = Installment.objects.filter(sale_id=sale.id).first()
        ClientDebtReportData.objects.create(
            client_fullname=sale.client.get_fio(),
            abonement_id=sale.id,
            activation=sale.activation,
            date_of_start_abonement=sale.activation_date,
            date_of_finish_abonement=sale.deactivation_date,
            debt_sum=installment.debt,
            report=report
        )

    make_report_notification(user.id, "Клиенты с долгами")



@shared_task
def make_report_saled_by_groups(date_to, date_from, user_id, period_info, departament_id=None):
    user = User.objects.get(pk=user_id)
    abones = Abonement.objects.all()

    report = SalesByGroupsReport.objects.create(
        departament_id=departament_id,
        user=user.profile,
        date_to=date_to,
        date_from=date_from,
        period=period_info
    )

    for abon in abones:
        if departament_id:
            sales_q = abon.sale_set.filter(
                saled_by_departament_id=departament_id,
                created__range=(date_to, date_from),
            )
        else:
            sales_q = abon.sale_set.filter(
                created__range=(date_to, date_from),
            )
        SalesByGroupsReportData.objects.create(
            abonement=abon,
            duration=abon.duration,
            sales_count=sales_q.count(),
            count_payment_by_installment=sum([sale.get_count_payments_by_installment(date_to, date_from) for sale in sales_q]),
            income_money=sum([sale.get_income_money(date_to, date_from) for sale in sales_q]),
            report=report
        )

    report.set_total_values()

    make_report_notification(user.id, "Продажи по группам")




@shared_task
def make_report_sales_to_group(date_to, date_from, user_id, period_info, departament_id=None):
    user = User.objects.get(pk=user_id)

    sales_q = Sale.objects.prefetch_related('group', 'client').filter(created__range=(date_to, date_from))

    if departament_id:
        sales_q = sales_q.filter(saled_by_departament_id=departament_id)

    report = SalesToGroupReport.objects.create(
        departament_id=departament_id,
        user=user.profile,
        date_to=date_to,
        date_from=date_from,
        period=period_info
    )

    for sale in sales_q:
        for group in sale.group.all():
            SalesToGroupReportData.objects.create(
                sales_date=sale.created,
                group=group,
                client=sale.client,
                abonement_name=sale.abonement_name,
                income_money=sale.end_payment,
                report=report
            )
    report.set_total_money()

    make_report_notification(user.id, "Продажи абонементов по группам")



