from .views.cashbox import CashboxView, StartReportView
from .views.incoming_money import IncomingMoneyView, StartIncomingMoneyReportView
from .views.incoming_money_detail import IncomingMoneyDetailView

# PROMO DISCOUNT
from .views.promo_discount import PromoDiscountView, StartPromoDiscountReportView, PromoDiscountDetailView

# ACTIVE CLIENTS
from .views.active_clients import ActiveClientReportView, StartActiveClientReportView
from .views.active_clients_detail import ActiveClientsDetailView

# CLIENTS DEBT
from .views.clients_debt import view as clientdebtview
from .views.clients_debt import detail as clientdebtdetail

# SALES BY GROUPS
from .views.sales_by_groups import view as salesbygroupview
from .views.sales_by_groups import detail as salesbygroupdetail

# SALES TO GROUP
from .views.sales_to_groups import view as salestogroupview
from .views.sales_to_groups import detail as salestogroupdetail


from django.urls import path

app_name = 'reports'

urlpatterns = [
    path('cashbox/', CashboxView.as_view(), name="cashbox_report"),
    path('cashbox/start_report/', StartReportView.as_view(), name="cashbox_make_report"),
    path('incoming_money/', IncomingMoneyView.as_view(), name="incoming_money_report"),
    path('incoming_money/start_report/', StartIncomingMoneyReportView.as_view(), name="make_incoming_report"),
    path('incoming_money/detail/<int:id>/', IncomingMoneyDetailView.as_view(), name="incoming_money_report_detail"),

    # PROMO AND DISCOUNT
    path('promo_discount/', PromoDiscountView.as_view(), name="promo_discount_report"),
    path('promo_discount/start_report/', StartPromoDiscountReportView.as_view(), name="make_promo_discount_report"),
    path('promo_discount/detail/<int:id>/', PromoDiscountDetailView.as_view(), name="promo_discount_report_detail"),

    # ACTIVE CLIENTS
    path('active_clients/', ActiveClientReportView.as_view(), name="active_client_report"),
    path('active_clients/start_report/', StartActiveClientReportView.as_view(), name="active_client_report_start"),
    path('active_clients/detail/<int:id>/', ActiveClientsDetailView.as_view(), name="active_clients_report_detail"),

    # CLIENTS DEBT
    path('clients_debt/', clientdebtview.ReportView.as_view(), name="clients_debt_report_view"),
    path('clients_debt/start_report/', clientdebtview.StartReport.as_view(), name="clients_debt_report_start"),
    path('clients_debt/detail/<int:id>/', clientdebtdetail.ReportDetailView.as_view(), name="clients_debt_report_detail"),

    # SALES BY GROUPS
    path('sales_by_groups/', salesbygroupview.ReportView.as_view(), name="sales_by_groups_report_view"),
    path('sales_by_groups/start/report', salesbygroupview.StartReport.as_view(), name="sales_by_groups_report_start"),
    path('sales_by_groups/detail/<int:id>/', salesbygroupdetail.ReportDetailView.as_view(), name="sales_by_groups_report_detail"),

    # SALES TO GROUP
    path('sales_to_groups/', salestogroupview.ReportView.as_view(), name="sales_to_groups_report_view"),
    path('sales_to_groups/start/report', salestogroupview.StartReport.as_view(), name="sales_to_groups_report_start"),
    path('sales_to_groups/detail/<int:id>/', salestogroupdetail.ReportDetailView.as_view(), name="sales_to_groups_report_detail"),

]