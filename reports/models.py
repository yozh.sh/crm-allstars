from django.db import models
from django.db.models import Sum, Count
from .report_manager import ReportManager


class ReportModel(models.Model):
    class Meta:
        abstract = True

    created = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey('crm.Profile', on_delete=models.PROTECT)
    departament = models.ForeignKey('crm.Departament', on_delete=models.PROTECT)
    date_from = models.DateTimeField()
    date_to = models.DateTimeField()


class CashboxReport(ReportModel):
    objects = models.Manager()
    report_objects = ReportManager()

    DAY = 'DAY'
    CURR_MONTH = 'CURRENT_MONTH'
    PREV_MONTH = 'PREV_MONTH'
    CURR_YEAR = 'CURRENT_YEAR'
    PREV_YEAR = 'PREV_YEAR'
    CUSTOM = 'CUSTOM'

    PERIOD_INFO = (
        (DAY, 'День'),
        (CURR_MONTH, 'Текущий месяц'),
        (PREV_MONTH, 'Прошлый месяц'),
        (CURR_YEAR, 'Текущий год'),
        (PREV_YEAR, 'Прошлый год')
    )
    departament = models.ForeignKey('crm.Departament', on_delete=models.PROTECT, null=True, blank=True)
    cash = models.PositiveIntegerField(null=True, blank=True)
    terminal = models.PositiveIntegerField(null=True, blank=True)
    total = models.PositiveIntegerField(null=True, blank=True)
    period = models.CharField(max_length=100,
                              choices=PERIOD_INFO)


class IncomingMoneyReport(ReportModel):
    objects = models.Manager()
    report_objects = ReportManager()

    DAY = 'DAY'
    CURR_MONTH = 'CURRENT_MONTH'
    PREV_MONTH = 'PREV_MONTH'
    CURR_YEAR = 'CURRENT_YEAR'
    PREV_YEAR = 'PREV_YEAR'
    CUSTOM = 'CUSTOM'

    PERIOD_INFO = (
        (DAY, 'День'),
        (CURR_MONTH, 'Текущий месяц'),
        (PREV_MONTH, 'Прошлый месяц'),
        (CURR_YEAR, 'Текущий год'),
        (PREV_YEAR, 'Прошлый год')
    )
    departament = models.ForeignKey('crm.Departament', on_delete=models.PROTECT, null=True, blank=True)
    period = models.CharField(max_length=100,
                              choices=PERIOD_INFO)

    def get_total_money_count(self):
        return self.incomingmoneydata_set.all().aggregate(
            sum=Sum('money')
        )


class IncomingMoneyData(models.Model):
    objects = models.Manager()
    report_objects = ReportManager()

    date = models.DateField()
    money = models.PositiveIntegerField(null=True, blank=True)
    report = models.ForeignKey('IncomingMoneyReport', on_delete=models.CASCADE)


class PromoDiscountReport(ReportModel):
    objects = models.Manager()
    report_objects = ReportManager()

    DAY = 'DAY'
    CURR_MONTH = 'CURRENT_MONTH'
    PREV_MONTH = 'PREV_MONTH'
    CURR_YEAR = 'CURRENT_YEAR'
    PREV_YEAR = 'PREV_YEAR'
    CUSTOM = 'CUSTOM'

    PERIOD_INFO = (
        (DAY, 'День'),
        (CURR_MONTH, 'Текущий месяц'),
        (PREV_MONTH, 'Прошлый месяц'),
        (CURR_YEAR, 'Текущий год'),
        (PREV_YEAR, 'Прошлый год')
    )
    departament = models.ForeignKey('crm.Departament', on_delete=models.PROTECT, null=True, blank=True)
    period = models.CharField(max_length=100,
                              choices=PERIOD_INFO)


class PromoDiscountReportDataPromo(models.Model):
    objects = models.Manager()
    report_objects = ReportManager()

    promo_discount_name = models.CharField(max_length=255, db_index=True)
    report = models.ForeignKey(PromoDiscountReport, on_delete=models.CASCADE)

    def __str__(self):
        return self.promo_discount_name


class PromoDiscountReportDataSale(models.Model):
    objects = models.Manager()
    report_objects = ReportManager()

    PROCENT = 'PROCENT'
    MONEY = 'MONEY'

    TYPE = (
        (PROCENT, 'Проценты'),
        (MONEY, 'Гривны')
    )
    date_of_sale = models.DateTimeField()
    groups = models.ManyToManyField('crm.CrmGroup')
    client_fullname = models.CharField(max_length=255)
    client_profile_url = models.CharField(max_length=255)
    price_without_discount = models.PositiveIntegerField()
    price_with_discount = models.PositiveIntegerField()
    type_of_discount = models.CharField(choices=TYPE, max_length=120, default=MONEY)
    discount = models.IntegerField()  # или в процентах или в гривнах
    promotion_data = models.ForeignKey(PromoDiscountReportDataPromo, on_delete=models.CASCADE)


class ActiveClientsReport(ReportModel):
    objects = models.Manager()
    report_objects = ReportManager()

    DAY = 'DAY'
    CURR_MONTH = 'CURRENT_MONTH'
    PREV_MONTH = 'PREV_MONTH'
    CURR_YEAR = 'CURRENT_YEAR'
    PREV_YEAR = 'PREV_YEAR'
    CUSTOM = 'CUSTOM'

    PERIOD_INFO = (
        (DAY, 'День'),
        (CURR_MONTH, 'Текущий месяц'),
        (PREV_MONTH, 'Прошлый месяц'),
        (CURR_YEAR, 'Текущий год'),
        (PREV_YEAR, 'Прошлый год')
    )
    period = models.CharField(max_length=100,
                              choices=PERIOD_INFO)
    departament = models.ForeignKey('crm.Departament', on_delete=models.PROTECT, null=True, blank=True)


class ActiveClientsReportData(models.Model):
    objects = models.Manager()
    report_objects = ReportManager()

    client_fullname = models.CharField(max_length=255, db_index=True)
    source_of_attraction = models.CharField(max_length=255, null=True, blank=True)
    abonement_name = models.CharField(max_length=255)
    groups = models.ManyToManyField('crm.CrmGroup')
    date_of_sale = models.DateField()
    date_of_start_abonement = models.DateField()
    date_of_finish_abonement = models.DateField()
    abonement_price = models.PositiveIntegerField()
    report = models.ForeignKey('ActiveClientsReport', on_delete=models.CASCADE)


class ClientDebtReport(ReportModel):
    objects = models.Manager()
    report_objects = ReportManager()

    DAY = 'DAY'
    CURR_MONTH = 'CURRENT_MONTH'
    PREV_MONTH = 'PREV_MONTH'
    CURR_YEAR = 'CURRENT_YEAR'
    PREV_YEAR = 'PREV_YEAR'
    CUSTOM = 'CUSTOM'

    PERIOD_INFO = (
        (DAY, 'День'),
        (CURR_MONTH, 'Текущий месяц'),
        (PREV_MONTH, 'Прошлый месяц'),
        (CURR_YEAR, 'Текущий год'),
        (PREV_YEAR, 'Прошлый год')
    )

    period = models.CharField(max_length=100,
                              choices=PERIOD_INFO)
    departament = models.ForeignKey('crm.Departament', on_delete=models.PROTECT, null=True, blank=True)


class ClientDebtReportData(models.Model):
    objects = models.Manager()
    report_objects = ReportManager()

    client_fullname = models.CharField(max_length=255, db_index=True)
    abonement = models.ForeignKey('crm.Sale', on_delete=models.PROTECT)
    activation = models.BooleanField(default=False)
    date_of_start_abonement = models.DateField(null=True, blank=True)
    date_of_finish_abonement = models.DateField(null=True, blank=True)
    debt_sum = models.PositiveIntegerField()
    report = models.ForeignKey('ClientDebtReport', on_delete=models.CASCADE)


class SalesByGroupsReport(ReportModel):
    objects = models.Manager()
    report_objects = ReportManager()

    DAY = 'DAY'
    CURR_MONTH = 'CURRENT_MONTH'
    PREV_MONTH = 'PREV_MONTH'
    CURR_YEAR = 'CURRENT_YEAR'
    PREV_YEAR = 'PREV_YEAR'
    CUSTOM = 'CUSTOM'

    PERIOD_INFO = (
        (DAY, 'День'),
        (CURR_MONTH, 'Текущий месяц'),
        (PREV_MONTH, 'Прошлый месяц'),
        (CURR_YEAR, 'Текущий год'),
        (PREV_YEAR, 'Прошлый год')
    )
    period = models.CharField(max_length=100,
                              choices=PERIOD_INFO)
    departament = models.ForeignKey('crm.Departament', on_delete=models.PROTECT, null=True, blank=True)
    total_sales_abon_count = models.PositiveIntegerField(default=0)  # общее количество проданных абонементов
    total_count_payments = models.PositiveIntegerField(default=0)   # общее количество доплат по рассрочкам
    total_sum = models.PositiveIntegerField(default=0)  # общее количество денег

    def set_total_values(self):
        self.total_sales_abon_count = self.get_total_sales_abon_count()
        self.total_count_payments = self.get_total_count_payments()
        self.total_sum = self.get_total_sum()
        self.save()

    def get_total_sales_abon_count(self):
        total = self.salesbygroupsreportdata_set.aggregate(sum=Sum('sales_count'))
        return total['sum']

    def get_total_count_payments(self):
        total = self.salesbygroupsreportdata_set.aggregate(sum=Sum('count_payment_by_installment'))
        return total['sum']

    def get_total_sum(self):
        total = self.salesbygroupsreportdata_set.aggregate(sum=Sum('income_money'))
        return total['sum']


class SalesByGroupsReportData(models.Model):
    objects = models.Manager()
    report_objects = ReportManager()

    abonement = models.ForeignKey('crm.Abonement', on_delete=models.PROTECT)
    duration = models.PositiveIntegerField()    # длительность в днях
    sales_count = models.PositiveIntegerField() # количество проданых
    count_payment_by_installment = models.PositiveIntegerField()    # количество доплат
    income_money = models.PositiveIntegerField()    # Доход
    report = models.ForeignKey('SalesByGroupsReport', on_delete=models.CASCADE)


class SalesToGroupReport(ReportModel):  # Продажа абонементов по группам
    objects = models.Manager()
    report_objects = ReportManager()

    DAY = 'DAY'
    CURR_MONTH = 'CURRENT_MONTH'
    PREV_MONTH = 'PREV_MONTH'
    CURR_YEAR = 'CURRENT_YEAR'
    PREV_YEAR = 'PREV_YEAR'
    CUSTOM = 'CUSTOM'

    PERIOD_INFO = (
        (DAY, 'День'),
        (CURR_MONTH, 'Текущий месяц'),
        (PREV_MONTH, 'Прошлый месяц'),
        (CURR_YEAR, 'Текущий год'),
        (PREV_YEAR, 'Прошлый год')
    )
    period = models.CharField(max_length=100,
                              choices=PERIOD_INFO)
    departament = models.ForeignKey('crm.Departament', on_delete=models.PROTECT, null=True, blank=True)
    total_money = models.PositiveIntegerField(default=0)

    def set_total_money(self):
        total_sum = self.salestogroupreportdata_set.aggregate(total=Sum('income_money'))
        self.total_money = total_sum['total']
        self.save()


class SalesToGroupReportData(models.Model):
    objects = models.Manager()
    report_objects = ReportManager()

    sales_date = models.DateField()
    group = models.ForeignKey('crm.CrmGroup', on_delete=models.PROTECT)
    client = models.ForeignKey('crm.Client', on_delete=models.PROTECT)
    abonement_name = models.CharField(max_length=255)
    income_money = models.PositiveIntegerField()
    report = models.ForeignKey('SalesToGroupReport', on_delete=models.CASCADE)


