from .base import BaseStartReport, BaseReportView
from reports.forms.incoming_money import IncomingMoneyForm
from reports.models import IncomingMoneyReport
from reports.tasks import incoming_money_make_report


class IncomingMoneyView(BaseReportView):
    template_name = 'reports/incoming_money.html'
    create_form = IncomingMoneyForm
    queryset = IncomingMoneyReport.report_objects.all()
    heading = 'Приход Денег'


class StartIncomingMoneyReportView(BaseStartReport):
    form_class = IncomingMoneyForm
    template_name = 'reports/incoming_money.html'

    def run_task_departament_all(self, date_to, date_from):
        incoming_money_make_report.delay(
            period_info=self.period_context.get_period(),
            date_from=date_from,
            date_to=date_to,
            user_id=self.request.user.id
        )

    def run_task_concrete_departament(self, date_to, date_from, departament_id):
        incoming_money_make_report.delay(
            period_info=self.period_context.get_period(),
            date_from=date_from,
            date_to=date_to,
            user_id=self.request.user.id,
            departament_id=departament_id
        )