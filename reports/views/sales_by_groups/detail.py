from crm.mixins import LoginMixin
from django.views.generic import ListView
from reports.models import SalesByGroupsReportData, SalesByGroupsReport


class ReportDetailView(LoginMixin, ListView):
    template_name = 'reports/sales_by_groups/detail.html'

    def get_queryset(self):
        report_id = self.kwargs.get('id')
        return SalesByGroupsReportData.objects.filter(report_id=report_id).all()

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['report_data'] = self.get_queryset()
        report = SalesByGroupsReport.objects.get(pk=self.kwargs.get('id'))
        context['total_data'] = {
            'total_sales_abon_count': report.total_sales_abon_count,
            'total_count_payments': report.total_count_payments,
            'total_sum': report.total_sum
        }
        return context