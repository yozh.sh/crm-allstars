from reports.views.base import BaseStartReport, BaseReportView

from reports.forms.sales_by_groups import SalesByGroupsForm
from reports.models import SalesByGroupsReport
from reports.tasks import make_report_saled_by_groups


class ReportView(BaseReportView):
    template_name = 'reports/sales_by_groups/index.html'
    queryset = SalesByGroupsReport.report_objects.all()
    create_form = SalesByGroupsForm
    heading = 'Отчет Продажи по группам'


class StartReport(BaseStartReport):
    form_class = SalesByGroupsForm
    template_name = 'reports/sales_by_groups/index.html'

    def run_task_departament_all(self, date_to, date_from):
        make_report_saled_by_groups.delay(
            period_info=self.period_context.get_period(),
            date_from=date_from,
            date_to=date_to,
            user_id=self.request.user.id
        )

    def run_task_concrete_departament(self, date_to, date_from, departament_id):
        make_report_saled_by_groups.delay(
            period_info=self.period_context.get_period(),
            date_from=date_from,
            date_to=date_to,
            user_id=self.request.user.id,
            departament_id=departament_id
        )
