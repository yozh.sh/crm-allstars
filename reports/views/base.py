# STRATEGIES
from reports.utils.periods.strategies.context import PeriodContext
from reports.utils.periods.strategies.day import DayDate
from reports.utils.periods.strategies.month import CurrentMonth, PreviousMonth
from reports.utils.periods.strategies.year import CurrentYear, PreviousYear
from reports.utils.periods.strategies.custom import CustomPeriod

from django.http import JsonResponse
from django.views.generic import FormView, ListView
from crm.mixins import LoginMixin


class BaseReportView(LoginMixin, ListView):
    heading = ''
    create_form = None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['create_form'] = self.create_form()
        context['report_data'] = self.queryset
        context['heading'] = self.heading
        return context


class BaseStartReport(LoginMixin, FormView):
    period_context = PeriodContext()

    def run_task_departament_all(self, date_to, date_from):
        pass

    def run_task_concrete_departament(self, date_to, date_from, departament_id):
        pass

    def form_valid(self, form):
        action = form.cleaned_data.get('choice_period')
        departament = form.cleaned_data.get('departament_choice')
        date_to, date_from = None, None
        if action == 'DAY':
            self.period_context.set_strategy(DayDate(form.cleaned_data['current_day']))
        elif action == 'MONTH':
            if form.cleaned_data['month_parameters'] == 'CURRENT':
                self.period_context.set_strategy(CurrentMonth())
            else:
                self.period_context.set_strategy(PreviousMonth())
        elif action == 'YEAR':
            if form.cleaned_data['year_parameters'] == 'CURRENT':
                self.period_context.set_strategy(CurrentYear())
            else:
                self.period_context.set_strategy(PreviousYear())
        elif action == 'CUSTOM':
            self.period_context.set_strategy(CustomPeriod(
                date_to=form.cleaned_data.get('date_to'),
                date_from=form.cleaned_data.get('date_from')
            ))

        date_to, date_from = self.period_context.exec_strategy()
        if departament.type == 'ALL':
            self.run_task_departament_all(date_to, date_from)
        else:
            self.run_task_concrete_departament(date_to, date_from, departament.id)
        return JsonResponse(data={}, status=200)
