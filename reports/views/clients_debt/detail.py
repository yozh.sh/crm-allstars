from crm.mixins import LoginMixin
from django.views.generic import ListView
from reports.models import ClientDebtReportData


class ReportDetailView(LoginMixin, ListView):
    template_name = 'reports/clients_debt/detail.html'

    def get_queryset(self):
        report_id = self.kwargs.get('id')
        return ClientDebtReportData.objects.filter(report_id=report_id).all()

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['report_data'] = self.get_queryset()
        return context