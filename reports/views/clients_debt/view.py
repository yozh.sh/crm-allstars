from reports.views.base import BaseStartReport, BaseReportView

from reports.forms.clients_debt import ClientsDebtForm
from reports.models import ClientDebtReport
from reports.tasks import make_report_clients_debt


class ReportView(BaseReportView):
    template_name = 'reports/clients_debt/index.html'
    create_form = ClientsDebtForm
    queryset = ClientDebtReport.report_objects.all()
    heading = 'Отчет Клиенты с задолженостью'


class StartReport(BaseStartReport):
    form_class = ClientsDebtForm
    template_name = 'reports/clients_debt/index.html'

    def run_task_departament_all(self, date_to, date_from):
        make_report_clients_debt.delay(
            period_info=self.period_context.get_period(),
            date_from=date_from,
            date_to=date_to,
            user_id=self.request.user.id
        )

    def run_task_concrete_departament(self, date_to, date_from, departament_id):
        make_report_clients_debt.delay(
            period_info=self.period_context.get_period(),
            date_from=date_from,
            date_to=date_to,
            user_id=self.request.user.id,
            departament_id=departament_id
        )