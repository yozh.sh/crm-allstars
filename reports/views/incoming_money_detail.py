from crm.mixins import LoginMixin
from django.views.generic import ListView
from reports.models import IncomingMoneyData


class IncomingMoneyDetailView(LoginMixin, ListView):
    template_name = 'reports/incoming_money_detail.html'

    def get_queryset(self):
        report_id = self.kwargs.get('id')
        return IncomingMoneyData.objects.filter(report_id=report_id).all()

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['report_data'] = self.get_queryset()
        return context
