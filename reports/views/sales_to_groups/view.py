from reports.views.base import BaseStartReport, BaseReportView
from reports.forms.sales_to_group import SalesToGroupForm
from reports.models import SalesToGroupReport
from reports.tasks import make_report_sales_to_group


class ReportView(BaseReportView):
    template_name = 'reports/sales_to_group/index.html'
    queryset = SalesToGroupReport.report_objects.all()
    create_form = SalesToGroupForm
    heading = 'Отчет Продажи Абонементов по группам'


class StartReport(BaseStartReport):
    form_class = SalesToGroupForm
    template_name = 'reports/sales_to_group/index.html'

    def run_task_departament_all(self, date_to, date_from):
        make_report_sales_to_group.delay(
            period_info=self.period_context.get_period(),
            date_from=date_from,
            date_to=date_to,
            user_id=self.request.user.id
        )

    def run_task_concrete_departament(self, date_to, date_from, departament_id):
        make_report_sales_to_group.delay(
            period_info=self.period_context.get_period(),
            date_from=date_from,
            date_to=date_to,
            user_id=self.request.user.id,
            departament_id=departament_id
        )