from crm.mixins import LoginMixin
from django.views.generic import ListView
from reports.models import SalesToGroupReportData, SalesToGroupReport


class ReportDetailView(LoginMixin, ListView):
    template_name = 'reports/sales_to_group/detail.html'

    def get_queryset(self):
        report_id = self.kwargs.get('id')
        return SalesToGroupReportData.objects.filter(report_id=report_id).all()

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['report_data'] = self.get_queryset()
        report = SalesToGroupReport.objects.get(pk=self.kwargs.get('id'))
        context['total_sum'] = report.total_money
        return context