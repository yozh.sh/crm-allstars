from .base import BaseReportView, BaseStartReport
from reports.forms.cashbox import CashboxForm
from reports.models import CashboxReport


from reports.tasks import cashbox_make_report


class CashboxView(BaseReportView):
    template_name = 'reports/cashbox.html'
    heading = 'Отчет Касса'
    create_form = CashboxForm
    queryset = CashboxReport.report_objects.all()


class StartReportView(BaseStartReport):
    form_class = CashboxForm
    template_name = 'reports/cashbox.html'

    def run_task_departament_all(self, date_to, date_from):
        cashbox_make_report.delay(
            period_info=self.period_context.get_period(),
            date_from=date_from,
            date_to=date_to,
            user_id=self.request.user.id,
        )

    def run_task_concrete_departament(self, date_to, date_from, departament_id):
        cashbox_make_report.delay(
            period_info=self.period_context.get_period(),
            date_from=date_from,
            date_to=date_to,
            user_id=self.request.user.id,
            departament_id=departament_id
        )




    def form_invalid(self, form):
        print("INVALIIIIIIIIDDDD!!!!")
        print(form.errors, "INVALID!")
