from crm.mixins import LoginMixin
from django.views.generic import ListView
from reports.models import PromoDiscountReport, PromoDiscountReportDataPromo
from reports.forms.promo_discount import PromoDiscountForm
from reports.tasks import promotion_discount_make_report
from .base import BaseStartReport, BaseReportView


class PromoDiscountView(BaseReportView):
    template_name = 'reports/promo_discount.html'
    queryset = PromoDiscountReport.report_objects.all()
    create_form = PromoDiscountForm
    heading = 'Отчет Скидки и Акции'


class StartPromoDiscountReportView(BaseStartReport):
    form_class = PromoDiscountForm
    template_name = 'reports/promo_discount.html'

    def run_task_concrete_departament(self, date_to, date_from, departament_id):
        promotion_discount_make_report.delay(
            period_info=self.period_context.get_period(),
            date_from=date_from,
            date_to=date_to,
            user_id=self.request.user.id,
            departament_id=departament_id
        )

    def run_task_departament_all(self, date_to, date_from):
        promotion_discount_make_report.delay(
            period_info=self.period_context.get_period(),
            date_from=date_from,
            date_to=date_to,
            user_id=self.request.user.id
        )


class PromoDiscountDetailView(LoginMixin, ListView):
    template_name = 'reports/promo_discount_detail.html'

    def get_queryset(self):
        report_id = self.kwargs.get('id')
        return PromoDiscountReportDataPromo.objects.filter(report_id=report_id).all()

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['report_data'] = self.get_queryset()
        return context

