from .base import BaseStartReport, BaseReportView
from reports.forms.active_clients import ActiveClientsForm
from reports.models import ActiveClientsReport
import datetime
from reports.tasks import make_report_active_clients


class ActiveClientReportView(BaseReportView):
    template_name = 'reports/active_clients.html'
    create_form = ActiveClientsForm
    heading = 'Отчет Активные клиенты'
    queryset = ActiveClientsReport.report_objects.all()


class StartActiveClientReportView(BaseStartReport):
    form_class = ActiveClientsForm
    template_name = 'reports/active_clients.html'


    def run_task_departament_all(self, date_to, date_from):
        make_report_active_clients(
            period_info=self.period_context.get_period(),
            date_from=date_from,
            date_to=date_to,
            user_id=self.request.user.id
        )  # DELETE DELAY ITS TEMPORARY FIX, NEED REVIEW CELERY TASK

    def run_task_concrete_departament(self, date_to, date_from, departament_id):
        make_report_active_clients(
            period_info=self.period_context.get_period(),
            date_from=datetime.datetime.combine(date_from, datetime.time.min),
            date_to=datetime.datetime.combine(date_to, datetime.time.max),
            user_id=self.request.user.id,
            departament_id=departament_id
        )


