from django.db import models


class ReportManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().order_by('-created')
