from django import forms
from reports.forms.cashbox import CashboxForm
from reports.models import IncomingMoneyReport

class IncomingMoneyForm(CashboxForm):
    class Meta:
        model = IncomingMoneyReport
        fields = ['departament_choice', 'choice_period', 'date_to', 'date_from']
        labels = {
            "departament_choice": "Филиал",
            "date_to": 'Дата c',
            "date_from": 'Дата по',
        }
        widgets = {
            'date_to': forms.DateInput(format='%d.%m.%Y', attrs={
                "data-date-format": "dd.mm.yyyy"
            }),

            'date_from': forms.DateInput(format='%d.%m.%Y', attrs={
                "data-date-format": "dd.mm.yyyy"
            })
        }