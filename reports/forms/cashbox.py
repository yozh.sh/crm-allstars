from django import forms
from reports.models import CashboxReport
from crm.models import Departament
import datetime
from crm.utils.forms import make_dynamic_choice_field



class CashboxForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        # first call parent's constructor
        super().__init__(*args, **kwargs)
        # there's a `fields` property now
        self.fields['date_to'].required = False
        self.fields['date_from'].required = False
        self.fields['current_day'].required = False

    REPORT_CHOICES = [
        ('DAY', 'День'),
        ('MONTH', 'Месяц'),
        ('YEAR', 'Год'),
        ('CUSTOM', 'Произвольный период')
    ]

    MONTH_PARAMETERS = [
        ('CURRENT', 'Текущий месяц'),
        ('PREVIOUS', 'Прошлый месяц')
    ]

    YEAR_PARAMETERS = [
        ('CURRENT', 'Текущий Год'),
        ('PREVIOUS', 'Прошлый Год')
    ]

    choice_period = forms.ChoiceField(label="Период",
                                      choices=REPORT_CHOICES,
                                      widget=forms.Select())


    departament_choice = forms.ModelChoiceField(label='Филиал',
                                                queryset=Departament.objects_default.all(),
                                                widget=forms.Select(),
                                                )


    current_day = forms.DateField(label='Выбор дня', widget=forms.DateInput(
        format='%d.%m.%Y',
        attrs={
            "data-date-format": "dd.mm.yyyy",
        }
    ))

    month_parameters = forms.ChoiceField(
        label="Параметры",
        choices=MONTH_PARAMETERS,
        widget=forms.Select()
    )

    year_parameters = forms.ChoiceField(
        label="Параметры",
        choices=YEAR_PARAMETERS,
        widget=forms.Select()
    )

    class Meta:
        model = CashboxReport
        fields = ['departament_choice', 'choice_period', 'date_to', 'date_from']
        labels = {
            "departament_choice": "Филиал",
            "date_to": 'Дата c',
            "date_from": 'Дата по',
        }
        widgets = {
            'date_to': forms.DateInput(format='%d.%m.%Y', attrs={
                "data-date-format": "dd.mm.yyyy"
            }),

            'date_from': forms.DateInput(format='%d.%m.%Y', attrs={
                "data-date-format": "dd.mm.yyyy"
            })
        }


