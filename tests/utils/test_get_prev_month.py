from django.test import TestCase
import datetime
from reports.utils.prev_month import get_prev_month


class TestGetPrevMonth(TestCase):
    def setUp(self) -> None:
        self.january = datetime.datetime(
            year=2020,
            month=1,
            day=1
        )
        self.february = datetime.datetime(
            year=2020,
            month=2,
            day=1
        )

        self.december = datetime.datetime(
            year=2020,
            month=12,
            day=1
        )

    def test_non_datetime_inject(self):
        self.assertRaises(TypeError, get_prev_month, 12)

    def test_right_returned_type(self):
        result = get_prev_month(self.january)
        self.assertIsInstance(result, datetime.datetime)

    def test_prev_year_month_value(self):
        result = get_prev_month(self.january)
        self.assertEqual(result.month, self.december.month)

    def test_prev_month_value(self):
        result = get_prev_month(self.february)
        self.assertEqual(result.month, self.january.month)


