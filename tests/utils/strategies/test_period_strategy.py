from django.test import TestCase
from reports.utils.periods.strategies.day import DayDate
from reports.utils.periods.strategies.month import CurrentMonth, PreviousMonth
from reports.utils.periods.strategies.year import CurrentYear, PreviousYear
from reports.utils.periods.strategies.custom import CustomPeriod
import datetime
from calendar import monthrange
import unittest

@unittest.skip("Abstract Test Model")
class TestPeriodStrategy(TestCase):
    # This 3 methods  need override
    def get_strategy(self):
        pass

    def _date_to(self) -> datetime.datetime:
        pass

    def _date_from(self) -> datetime.datetime:
        pass


    def get_excepted_result(self):
        return self._date_to(), self._date_from()

    def setUp(self) -> None:
        self.strategy = self.get_strategy()

    def test_type(self):
        self.assertIsInstance(self.strategy.get_dates(), tuple)

    def test_count_values(self):
        self.assertEqual(len(self.strategy.get_dates()), 2)

    def test_values_types(self):
        date_to, date_from = self.strategy.get_dates()
        self.assertIsInstance(date_to, datetime.datetime)
        self.assertIsInstance(date_from, datetime.datetime)

    def test_values_equal(self):
        date_to, date_from = self.strategy.get_dates()
        exc_date_to, exc_date_from = self.get_excepted_result()
        self.assertEqual(exc_date_to, date_to)
        self.assertEqual(exc_date_from, date_from)


class TestDayPeriod(TestPeriodStrategy):
    def get_strategy(self):
        return DayDate()

    def _date_to(self) -> datetime.datetime:
        now = datetime.datetime.now()
        return datetime.datetime(
            year=now.year,
            month=now.month,
            day=now.day,
            hour=0,
            minute=0,
            second=1
        )

    def _date_from(self) -> datetime.datetime:
        now = datetime.datetime.now()
        return datetime.datetime(
            year=now.year,
            month=now.month,
            day=now.day,
            hour=23,
            minute=59
        )


class TestPrevMonthPeriod(TestPeriodStrategy):
    def get_strategy(self):
        return PreviousMonth()

    def _date_to(self) -> datetime.datetime:
        now = datetime.datetime.now()
        return datetime.datetime(
            year=now.year - 1,
            month=12 if now.month - 1 == 0 else now.month - 1,
            day=1,
            hour=0,
            minute=0,
            second=1
        )

    def _date_from(self) -> datetime.datetime:
        now = datetime.datetime.now()
        _, last_day = monthrange(now.year - 1, 12 if now.month - 1 == 0 else now.month - 1)
        return datetime.datetime(
            year=now.year - 1,
            month=12 if now.month - 1 == 0 else now.month - 1,
            day=last_day,
            hour=23,
            minute=59,
        )


class TestCurrentMonth(TestPeriodStrategy):
    def get_strategy(self):
        return CurrentMonth()

    def _date_to(self) -> datetime.datetime:
        pass
    # TODO: нужно дописать сюда тесты

class TestYearPeriod(TestCase):
    def setUp(self) -> None:
        self.prev_year = PreviousYear()
        self.curr_year = CurrentYear()


class TestCustomPeriod(TestCase):
    pass