Swal.fire({
    position: 'top',
    icon: 'success',
    title: "{{ message }}",
    showConfirmButton: true,

}).then((result => {
    window.location.reload()
}))
