from django.db import models
from django.contrib.auth.models import User
# Create your models here.


class UserNotification(models.Model):
    # Nitification types
    REPORT = 'REPORT'
    TYPES = [
        (REPORT, 'Отчет'),
    ]

    created = models.DateTimeField(auto_now_add=True)
    type = models.CharField(max_length=50, choices=TYPES)
    message = models.CharField(max_length=255)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    new = models.BooleanField(default=True)

    def __str__(self):
        return "{} {}".format(self.user, self.__class__.__name__)


