from rest_framework import serializers
from notifications.models import UserNotification


class ReportNotificationSerializer(serializers.ModelSerializer):
    created = serializers.DateTimeField(format="%d-%m-%Y")
    class Meta:
        model = UserNotification
        fields = "__all__"
