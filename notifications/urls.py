from django.urls import path
from .views import ReportNotificationsView, ReportReadNotificationsView

urlpatterns = [
    path('report/', ReportNotificationsView.as_view(), name="user_notification"),
    path('report/set_read/', ReportReadNotificationsView.as_view(), name="user_set_read_notification")
]