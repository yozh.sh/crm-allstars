from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from notifications.models import UserNotification
from notifications.serializers.report_notification import ReportNotificationSerializer


class ReportNotificationsView(APIView):
    def get(self, request):
        if not request.user.is_authenticated:
            return Response(data=dict(), status=status.HTTP_403_FORBIDDEN)

        notifications = UserNotification.objects.filter(
            user_id=self.request.user.id,
            new=True
        )
        serializer = ReportNotificationSerializer(instance=notifications, many=True)
        return Response(data=serializer.data)


class ReportReadNotificationsView(APIView):
    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return Response(data=dict(), status=status.HTTP_403_FORBIDDEN)
        ids = map(lambda x: x['id'], request.data['data'])
        UserNotification.objects.filter(id__in=ids).update(new=False)
        return Response()




