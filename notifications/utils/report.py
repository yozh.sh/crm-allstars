from notifications.models import UserNotification


def make_report_notification(user_id, report_name: str):
    UserNotification.objects.create(
        type='REPORT',
        message='Отчет {} завершен'.format(report_name),
        user_id=user_id
    )


