from .base import *


DEBUG = True
ALLOWED_HOSTS = ['stage.allstars-dance.com', 'localhost']


INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'crm.apps.CrmConfig',
    'crispy_forms',
    'bootstrap_datepicker_plus',
    'bootstrap4',
    'django_select2',
    'rest_framework',
    'rolepermissions',
    'reports.apps.ReportsConfig',
    'rest_framework_swagger',
    'notifications.apps.NotificationsConfig',
    'simple_history',
    'logger.apps.LoggerConfig'
]

WSGI_APPLICATION = 'allstars.dev_wsgi.application'
