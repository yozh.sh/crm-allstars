from .base import *


DEBUG = True

ALLOWED_HOSTS = ['localhost', '127.0.0.1', '192.168.2.102', '192.168.1.254']

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'debug_toolbar',
    'crm.apps.CrmConfig',
    'crispy_forms',
    'bootstrap4',
    'django_select2',
    'rest_framework',
    'rolepermissions',
    'reports.apps.ReportsConfig',
    'rest_framework_swagger',
    'notifications.apps.NotificationsConfig',
    'simple_history',
    'logger.apps.LoggerConfig',
    'reset_migrations'
]



DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'dev_crm',
        'USER': 'dev_crm',
        'PASSWORD': 'dozykaa11',
        'HOST': '185.25.119.24',
        # 'HOST': 'localhost',
        'PORT': '5432'
    }
}

MIDDLEWARE += [
    'debug_toolbar.middleware.DebugToolbarMiddleware',
]


BOOTSTRAP4 = {
    'include_jquery': True,
}

SHOW_TOOLBAR_CALLBACK = True

# DEBUG_TOOLBAR_PANELS = [
#     'debug_toolbar.panels.profiling.ProfilingPanel'
# ]

INTERNAL_IPS = ('127.0.0.1')


# Need using gevent for work celery on Windows OS


STATIC_ROOT = None
