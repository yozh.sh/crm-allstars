from .base import *

ALLOWED_HOSTS = ['crm.allstars-dance.com']


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'crm.apps.CrmConfig',
    'crispy_forms',
    'bootstrap_datepicker_plus',
    'bootstrap4',
    'django_select2',
    'rest_framework',
    'rolepermissions',
]

WSGI_APPLICATION = 'allstars.prod_wsgi.application'
STATIC_ROOT = '/var/www/crm/static'


