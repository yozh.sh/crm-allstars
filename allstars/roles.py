from rolepermissions.roles import AbstractUserRole


class SystemAdmin(AbstractUserRole):
    # regular user - обычный пользователь
    available_permissions = {
        'create_admin_user': True,
        'delete_admin_user': True,
        'create_regular_user': True,
        'change_admin_user_password': True,
        'change_regular_user_password': True
    }

    def __str__(self):
        return "Системный администратор(root)"

    @staticmethod
    def get_role_name():
        return "SYSTEMADMIN"


class Admin(AbstractUserRole):
    available_permissions = {
        'create_admin_user': False,
        'delete_admin_user': False,
        'create_regular_user': True,
        'change_admin_user_password': False,
        'change_regular_user_password': True
    }

    @staticmethod
    def get_role_name():
        return "ADMIN"

    def __str__(self):
        return "Администратор"


class User(AbstractUserRole):
    available_permissions = {
        'create_admin_user': False,
        'delete_admin_user': False,
        'create_regular_user': True,
        'change_admin_user_password': False,
        'change_regular_user_password': False
    }

    @staticmethod
    def get_role_name():
        return "USER"

    def __str__(self):
        return "Пользователь"