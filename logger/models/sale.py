from django.db import models
from django.contrib.postgres.fields import JSONField
import datetime
import json


class SaleLog(models.Model):
    user = models.ForeignKey('crm.Profile', on_delete=models.Su)
    action = models.CharField(max_length=120)
    operation_time = models.DateTimeField(default=datetime.datetime.now())
    edited_fields = JSONField(encoder=json.JSONEncoder, null=True, blank=True)



