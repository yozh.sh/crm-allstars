from django.db import models


class LogModel(models.Model):
    class Meta:
        abstract = True

    CREATE = 'CREATE'
    EDIT = 'EDIT'
    DELETE = 'DELETE'

    ACTIONS = (
        (CREATE, 'Создал'),
        (EDIT, 'Отредактировал'),
        (DELETE, 'Удалил')
    )

    created = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey('crm.Profile')
    action = models.CharField(max_length=100, choices=ACTIONS, default='')
