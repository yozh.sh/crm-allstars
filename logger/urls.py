from django.urls import path
from .views import MainLogPageView
# models views
from .views import TutorLog, SaleAbonementLog, ClientLog, GroupLog

urlpatterns = [
    path('', MainLogPageView.as_view(), name="log_index_view"),
    path('tutor/', TutorLog.as_view(), name="log_tutor_view"),
    path('sale_abon/', SaleAbonementLog.as_view(), name="log_sale_abon_view"),
    path('clients/', ClientLog.as_view(), name="log_client_view"),
    path('groups/', GroupLog.as_view(), name="log_group_view"),

]