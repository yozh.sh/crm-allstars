from django import template
from logger.utils.fields import human_readable

register = template.Library()

@register.filter(name='human_readable')
def readable_fields(changes: dict):
        return map(lambda field: human_readable(field, changes['instance'].get_mapped_labels()), changes['fields'])


@register.filter(name='hr_values')
def old_values(values: dict):
    hr_values = {}
    for field in values['values']:
        hr_values[human_readable(field, values['instance'].get_mapped_labels())] = values['values'][field]

    return hr_values


