from django import template
from crm.forms.group import CrmGroupModelForm
from logger.utils.fields import human_readable

register = template.Library()

MAPPED = CrmGroupModelForm.Meta.labels

@register.filter(name='group_human_readable')
def readable_fields(fields: list):
    return map(lambda field: human_readable(field, MAPPED), fields)

@register.filter(name='group_hr_values')
def old_values(values: dict):
    hr_values = {}
    for field in values:
        hr_values[human_readable(field, MAPPED)] = values[field]

    return hr_values


