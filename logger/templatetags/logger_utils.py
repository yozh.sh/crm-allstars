from django import template
from logger.utils.history_changes import changes as utils_changes
from django.utils.safestring import mark_safe
from django.contrib.auth.models import User
register = template.Library()


def get_prev_values(changed_fields: list, history):
    values = {}
    for field in changed_fields:
        if field in history.prev_record.__dict__:
            values[field] = history.prev_record.__dict__[field]

    return values


def get_next_values(changed_fields: list, history):
    values = {}
    for field in changed_fields:
        if field in history.next_record.__dict__:
            values[field] = history.next_record.__dict__[field]

    return values

def get_values(changed_fields: list, history):
    values = {}
    for field in changed_fields:
        if field in history.__dict__:
            values[field] = history.__dict__[field]
    return values


@register.filter(name='changes')
def changes(history_obj):
    changed_fields = utils_changes(history_obj)

    return {'fields': changed_fields, 'instance': history_obj.instance}


@register.filter(name='old_values')
def old_values(history):
    changed_fields = utils_changes(history)
    values = get_prev_values(changed_fields, history)
    return {'values': values, 'instance': history.instance}


@register.filter(name='new_values')
def new_values(history):
    changed_fields = utils_changes(history)
    if hasattr(history, 'next_record'):
        if history.next_record:
            values = get_next_values(changed_fields, history)
        else:
            values = get_values(changed_fields, history)
        return {'values': values, 'instance': history.instance}


@register.filter(name="htmlify_fields")
def fields_to_html(fields: list):
    html = "<div><p>{}</p></div>"
    html_fields = map(lambda field: html.format(field), fields)
    return mark_safe(" ".join(html_fields))


@register.filter(name="user_prettyfy")
def get_user_name(user: User):
    if user.get_full_name():
        return user.get_full_name()
    else:
        return user.email