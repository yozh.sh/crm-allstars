from django import template
from crm.models import Sale
from logger.utils.fields import human_readable

register = template.Library()

MAPPED = Sale.MAPPED_LABELS


@register.filter(name='sale_human_readable')
def readable_fields(changes: dict):
    return map(lambda field: human_readable(field, MAPPED), changes['fields'])


@register.filter(name='sale_hr_values')
def old_values(values: dict):
    hr_values = {}
    for field in values['values']:
        hr_values[human_readable(field, MAPPED)] = values['values'][field]
    return hr_values
