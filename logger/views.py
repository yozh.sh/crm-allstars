from django.views.generic import ListView
from crm.models import Tutor, Sale, Client, CrmGroup, Promotion, SourceOfAttraction, Abonement
from crm.utils.constants.pagination import SHOW_COUNT
from itertools import chain


class BaseLogView(ListView):
    paginate_by = SHOW_COUNT


class MainLogPageView(BaseLogView):
    template_name = 'logs/main.html'

    def get_queryset(self):
        clients_qs = Client.history.all()
        tutors_qs = Tutor.history.all()
        groups_qs = CrmGroup.history.all()
        promotions_qs = Promotion.history.all()
        sofa_qs = SourceOfAttraction.history.all()
        abonements_qs = Abonement.history.all()
        return sorted(list(chain(clients_qs, tutors_qs, groups_qs, promotions_qs, sofa_qs, abonements_qs)), key=lambda instance: instance.history_date, reverse=True)


# TODO: нужно зарефакторить
# Models Logs


class TutorLog(ListView):
    template_name = 'logs/models/tutors.html'
    paginate_by = SHOW_COUNT

    def get_queryset(self):
        return Tutor.history.all()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tutors_history'] = self.get_queryset()
        return context


class SaleAbonementLog(BaseLogView):
    template_name = 'logs/models/sale_abonement.html'
    queryset = Sale.history.all()


class ClientLog(ListView):
    template_name = 'logs/models/clients.html'
    paginate_by = SHOW_COUNT

    def get_queryset(self):
        return Client.history.all()

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['client_history'] = self.get_queryset()
        return context


class GroupLog(ListView):
    template_name = 'logs/models/groups.html'
    paginate_by = SHOW_COUNT

    def get_queryset(self):
        return CrmGroup.history.all()

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['group_history'] = self.get_queryset()
        return context
