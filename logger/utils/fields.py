

def human_readable(field: str, mapped_fields: dict):
    """
    в mapped_fields надо передать словарь где {"field_name": "читаемое_имя_поля"}
    """
    _field = None
    try:
        _field = mapped_fields[field]
    except KeyError:
        return field
    return _field