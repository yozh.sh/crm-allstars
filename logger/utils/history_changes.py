from typing import List

def changes(history_obj) -> List[str]:
    changed_fields = []
    if hasattr(history_obj, 'prev_record'):
        prev = history_obj.prev_record
        if prev:
            delta = history_obj.diff_against(prev)
            for change in delta.changes:
                changed_fields.append(change.field)
        return changed_fields
